/* This is for new family_db db
   Curate gene families for Ath db
   https://www.arabidopsis.org/browse/genefamily/index.jsp
   how to run
   psql -f
*/


/*  %%%%%%%%%%%%%%% */

/* old family_db database renamed to family_db_old and new family_db created
alter database family_db rename to family_db_old;
create database family_db;
*/

/* copy ath_gene_fam_1 from old db to family_db
   pg_dump -U renesh -t ath_gene_fam plant_db | psql -U renesh -d family_db */

/* curate gene family for ath */
DO language plpgsql $$
BEGIN
  RAISE NOTICE 'Creating ath_gene_fam_1 table';
END
$$;
drop table if exists atha_gene_fam_1;
create table atha_gene_fam_1 as select  gene_fam, sub_fam, upper(loc_tag) from ath_gene_fam;
alter table atha_gene_fam_1 rename upper to loc;
alter table atha_gene_fam_1 add gene_fam_2 varchar(80), add anot varchar(300);
ALTER TABLE atha_gene_fam_1 ADD COLUMN id SERIAL PRIMARY KEY;
update atha_gene_fam_1 set loc=trim(loc);
/*update ath_gene_fam_1 as a set anot=b.at_des from ath_phyt12 as b where a.loc=b.loc;*/

/* import annotation from tair 10
   New annotation; this file is imported becuase phytozome ath annotation was old and not up to date*/
DO language plpgsql $$
BEGIN
  RAISE NOTICE 'Adding TAIR 10 annotation atha_gene_fam_1 table';
END
$$;
drop table if exists atha_tair10_gene_anot;
create table atha_tair10_gene_anot (gene varchar(30), anot varchar(300));
\copy atha_tair10_gene_anot from '/Users/renesh/Renesh_Docs/Research/tamu/db/ath_tair10_gene_anot.txt';
alter table atha_tair10_gene_anot add loc varchar(20);
update atha_tair10_gene_anot set loc=(regexp_split_to_array(gene, '\.'))[1];
/*alter table ath_gene_fam_1 add anot2 varchar(300);*/
update atha_gene_fam_1 as a set anot=b.anot from atha_tair10_gene_anot as b where a.loc=b.loc;


/* annotation for ath from pfam */
DO language plpgsql $$
BEGIN
  RAISE NOTICE 'Adding PFAM annotation atha_gene_fam_1 table';
END
$$;
drop table if exists atha_pfam;
create table atha_pfam
(
dom varchar(200),
acc varchar(20),
query varchar(20),
acc2 varchar(20),
ev double precision,
scr double precision,
bias double precision,
ev2 double precision,
scr2 double precision,
bias2 double precision,
exp double precision,
reg int,
clu int,
ov int,
env int,
dom2 int,
rep int,
inc int,
des varchar(300)
);
\copy atha_pfam from '/Users/renesh/Renesh_Docs/Research/tamu/db/pfam_anot/atha_pfam_tab.csv' DELIMITER ',';
delete from atha_pfam where ev > 1e-05;
drop table if exists atha_pfam_group;
create table atha_pfam_group as select query, array_agg(dom) from atha_pfam group by query;
alter table atha_pfam_group add loc varchar(20), add dom_ct int;
update atha_pfam_group set loc=(regexp_split_to_array(query, '\.'))[1] where query like '%.1%';
update atha_pfam_group set dom_ct=array_length(array_agg, 1);
/* pfam_dom_auto are the domains which are joined using update command and not manually did */
alter table atha_gene_fam_1 drop if exists pfam_dom_auto, drop if exists pfam_len, drop if exists dom_ct, drop if exists tot_dom;
alter table atha_gene_fam_1 add  pfam_dom_auto varchar(200), add dom_ct int, add tot_dom varchar(10);
update atha_gene_fam_1 as a set pfam_dom_auto=b.array_agg from atha_pfam_group as b where upper(a.loc)=upper(b.loc);
update atha_gene_fam_1 as a set dom_ct=b.dom_ct from atha_pfam_group as b where upper(a.loc)=upper(b.loc);
drop table if exists temp;
create table temp as select distinct gene_fam from atha_gene_fam_1 where dom_ct > 1;
update atha_gene_fam_1 as a set tot_dom='multiple' from temp as b where a.gene_fam=b.gene_fam;
update atha_gene_fam_1 set tot_dom='single' where tot_dom is null;

/* curate gene families from tair10 */

/*

/* refer https://www.arabidopsis.org/browse/genefamily/index.jsp */
drop table if exists atha_aaap;
create table atha_aaap (id varchar(20));
\copy atha_aaap from '/Users/renesh/Renesh_Docs/Research/tamu/codes/python_scripts/ath_aaap.txt';

/* refer https://www.arabidopsis.org/browse/genefamily/index.jsp */
drop table if exists atha_ap2_tf;
create table atha_ap2_tf (id varchar(20));
\copy atha_ap2_tf from '/Users/renesh/Renesh_Docs/Research/tamu/codes/python_scripts/ath_ap2_tf.txt';

update atha_gene_fam_1 set gene_fam_2='AP2-EREBP Transcription Factor Family'
where
loc in (select id from atha_ap2_tf) and gene_fam like '%AP2-EREBP%' and gene_fam_2 is null;
insert into atha_gene_fam_1 (loc)
select id
from atha_ap2_tf
where id not in (select loc from atha_gene_fam_1);

update atha_gene_fam_1 set gene_fam_2='AP2-EREBP Transcription Factor Family' where gene_fam is null and gene_fam_2 is null;

update atha_gene_fam_1 set gene_fam_2='AS2 family'
where
gene_fam like '%AS2 family%' and gene_fam_2 is null;

/* refer https://www.arabidopsis.org/browse/genefamily/index.jsp */

/* refer https://www.arabidopsis.org/browse/genefamily/index.jsp */
drop table if exists atha_b3_bind;
create table atha_b3_bind (id varchar(20));
\copy atha_b3_bind from '/Users/renesh/Renesh_Docs/Research/tamu/codes/python_scripts/ath_b3_bind.txt';

update atha_gene_fam_1 set gene_fam_2='B3 DNA Binding Superfamily'
where
loc in (select id from atha_b3_bind) and gene_fam_2 is null ;

insert into atha_gene_fam_1 (loc)
select id
from atha_b3_bind
where id not in (select loc from atha_gene_fam_1);

update atha_gene_fam_1 set gene_fam_2='B3 DNA Binding Superfamily' where gene_fam is null and gene_fam_2 is null;
delete from atha_gene_fam_1 where gene_fam='ARF Transcription Factor Family' and
loc in (select loc from atha_gene_fam_1 where gene_fam_2='B3 DNA Binding Superfamily' );

delete from atha_gene_fam_1 where gene_fam_2='B3 DNA Binding Family' and
loc in (select loc from atha_gene_fam_1 where gene_fam_2='B3 DNA Binding Superfamily' );
update atha_gene_fam_1 set gene_fam_2='B3 DNA Binding Superfamily' where gene_fam_2='B3 DNA Binding Family';


update atha_gene_fam_1 set gene_fam_2='C2C2-Dof Transcription Factor Family'
where
gene_fam like '%C2C2-Dof Transcription Factor Family%' and gene_fam_2 is null;



update atha_gene_fam_1 set gene_fam_2='DOF Gene Family'
where
gene_fam like '%Dof family%' and gene_fam_2 is null;

update atha_gene_fam_1 set gene_fam_2='E2F-DP Transcription Factor Family'
where
gene_fam like '%E2F-DP Transcription Factor Family%' and gene_fam_2 is null;

/* refer https://www.arabidopsis.org/browse/genefamily/index.jsp */
drop table if exists atha_efhand;
create table atha_efhand (id varchar(20));
\copy atha_efhand from '/Users/renesh/Renesh_Docs/Research/tamu/codes/python_scripts/ath_efhand.txt';

update atha_gene_fam_1 set gene_fam_2='EF-hand Containing Proteins'
where
loc in (select id from atha_efhand) and gene_fam_2 is null;
insert into atha_gene_fam_1 (loc)
select id
from atha_efhand
where id not in (select loc from atha_gene_fam_1);
update atha_gene_fam_1 set gene_fam_2='EF-hand Containing Proteins' where gene_fam is null and gene_fam_2 is null;


update atha_gene_fam_1 set gene_fam_2='Lipid Metabolism Gene Family'
where
gene_fam like '%Lipid Metabolism Gene Families%' and gene_fam_2 is null;

update atha_gene_fam_1 set gene_fam_2='Lipid Metabolism Gene Family'
where
gene_fam like '%Lipid Metabolism Gene Families%' and gene_fam_2 is null;

/* refer https://www.arabidopsis.org/browse/genefamily/index.jsp */
drop table if exists atha_mads;
create table atha_mads (id varchar(20));
\copy atha_mads from '/Users/renesh/Renesh_Docs/Research/tamu/codes/python_scripts/ath_mads.txt';

update atha_gene_fam_1 set gene_fam_2='MADS Transcription Factor Family'
where
loc in (select id from atha_mads) and gene_fam_2 is null and gene_fam like '%MADS Transcription Factor Family%';
insert into atha_gene_fam_1 (loc)
select id
from atha_mads
where id not in (select loc from atha_gene_fam_1);
update atha_gene_fam_1 set gene_fam_2='MADS Transcription Factor Family' where gene_fam is null and gene_fam_2 is null;
delete from atha_gene_fam_1 where gene_fam like '%MADS like gene family%';
delete from atha_gene_fam_1 where gene_fam like '%MADS-box Transcription Factor Family%';
delete from atha_gene_fam_1 where gene_fam like '%Type I MADS box%';

update atha_gene_fam_1 set gene_fam_2='Major Intrinsic Protein MIP'
where
gene_fam like '%MIP family%' and gene_fam_2 is null;

drop table if exists atha_gene_fam_1_count;
create table atha_gene_fam_1_count as
select gene_fam,  count(distinct loc) from atha_gene_fam_1 group by gene_fam;

update atha_gene_fam_1 set gene_fam_2=gene_fam where gene_fam_2 is null;

update atha_gene_fam_1 set gene_fam_2='Nucleobase Ascorbate Transporter Gene Family'
where
gene_fam_2 like '%Nucleobase ascorbate transporters%' ;

delete from atha_gene_fam_1 where gene_fam_2='Phospholipase D';
update atha_gene_fam_1 set gene_fam_2='Phospholipase D Gene Family'
where
gene_fam_2 like '%Phospholipase D%' ;

update atha_gene_fam_1 set loc=(regexp_split_to_array(loc, ';'))[1] where loc like '%;%';

/* refer https://www.arabidopsis.org/browse/genefamily/index.jsp */
drop table if exists atha_phytocyanin;
create table atha_phytocyanin (id varchar(20));
\copy atha_phytocyanin from '/Users/renesh/Renesh_Docs/Research/tamu/codes/python_scripts/ath_phytocyanin.txt';

insert into atha_gene_fam_1 (loc)
select id
from atha_phytocyanin;
update atha_gene_fam_1 set gene_fam_2='Phytocyanin Superfamily' where gene_fam is null and gene_fam_2 is null;

delete from atha_gene_fam_1 where gene_fam='Primary Pumps (ATPases) Gene Family (2)' and
loc in (select loc from atha_gene_fam_1 where gene_fam='Primary Pumps (ATPases) Gene Families' );

update atha_gene_fam_1 set gene_fam_2='Primary Pumps (ATPases) Gene Family'
where
gene_fam_2 like '%Primary Pumps (ATPases) Gene Famil%' ;

update atha_gene_fam_1 set gene_fam_2='Sucrose Transporter Gene Family'
where
gene_fam_2 like '%Sucrose%' ;

delete from atha_gene_fam_1 where gene_fam='WRKY Transcription Factor Family' and
loc in (select loc from atha_gene_fam_1 where gene_fam='WRKY Transcription Factor Superfamily' );
delete from atha_gene_fam_1 where gene_fam='WRKY Transcription Factor; Unclassified' and
loc in (select loc from atha_gene_fam_1 where gene_fam='WRKY Transcription Factor Superfamily' );

update atha_gene_fam_1 set gene_fam_2='WRKY Transcription Factor Superfamily'
where
gene_fam_2 like '%WRKY%' ;

update atha_gene_fam_1 set gene_fam_2='Xyloglucan Fucosyltransferase1'
where
gene_fam_2 like '%Xyloglucan%' ;

update atha_gene_fam_1 set gene_fam_2='B3 DNA Binding Superfamily' where gene_fam_2='B3 DNA Binding Family';
delete from atha_gene_fam_1 where gene_fam='Lipid Metabolism Gene Families' and
loc in (select loc from atha_gene_fam_1 where gene_fam='Acyl Lipid Metabolism Family');

delete from atha_gene_fam_1 where gene_fam='Phospholipase D (Zarsky group)' and
loc in (select loc from atha_gene_fam_1 where gene_fam='Acyl Lipid Metabolism Family');

update atha_gene_fam_1 set gene_fam_2='Acyl Lipid Metabolism Family'
where
gene_fam_2 like '%Phospholipase D%' ;

update atha_gene_fam_1 set gene_fam_2='Response Regulator Gene Family'
where
gene_fam_2 like '%Response Regulator%' ;

delete from atha_gene_fam_1 where gene_fam='Caleosins' and
loc in (select loc from atha_gene_fam_1 where gene_fam='Acyl Lipid Metabolism Family');

delete from atha_gene_fam_1 where gene_fam='`';

update atha_gene_fam_1 set gene_fam_2='EF-hand Containing Proteins'
where
gene_fam_2 like '%EF-hand containing proteins%' ;



delete from atha_gene_fam_1 where gene_fam='REM Transcription Factor Family';





update atha_gene_fam_1 set gene_fam_2='Cell Wall Biosynthesis'
where
gene_fam like '%Plant Cell Wall Biosynthesis Families%' and gene_fam_2 is null;

update atha_gene_fam_1 set gene_fam_2='Glycosyltransferase Gene Family'
where
gene_fam like '%Cell Wall Biosynthesis%' ;

delete from atha_gene_fam_1 where gene_fam='Trehalose Biosynthesis Gene Families' and
    loc in (select loc from atha_gene_fam_1 where gene_fam='Glycosyltransferase Gene Families');

update atha_gene_fam_1 set gene_fam_2='Glycosyltransferase Gene Family'
where
gene_fam_2 like '%Trehalose Biosynthesis Gene Families%' ;

delete from atha_gene_fam_1 where gene_fam='Sucrose-H+ symporters' and
    loc in (select loc from atha_gene_fam_1 where gene_fam='Organic Solute Cotransporters');



delete from atha_gene_fam_1 where gene_fam='Monosaccharide transporter-like gene family' and
    loc in (select loc from atha_gene_fam_1 where gene_fam='Organic Solute Cotransporters');

delete from atha_gene_fam_1 where gene_fam='Nucleobase ascorbate transporters, NAT family' or
    gene_fam='AAAP family'  and
    loc in (select loc from atha_gene_fam_1 where gene_fam='Organic Solute Cotransporters');

update atha_gene_fam_1 set gene_fam_2='Organic Solute Cotransporters'
where
gene_fam_2 like  '%Monosaccharide transporter-like gene family%' or
gene_fam_2 like  '%Amino Acid/Auxin Permease AAAP Family%' ;

delete from atha_gene_fam_1 where gene_fam='MLO proteins' and
    loc in (select loc from atha_gene_fam_1 where gene_fam='Miscellaneous Membrane Protein Families');

update atha_gene_fam_1 set gene_fam_2='Miscellaneous Membrane Protein Family'
where
gene_fam_2 like '%Miscellaneous Membrane Protein Families%' ;

delete from atha_gene_fam_1 where gene_fam='Family of Arabidopsis genes related to Xyloglucan Fucosyltransferase1' and
    loc in (select loc from atha_gene_fam_1 where gene_fam='Glycosyltransferase Gene Families');

delete from atha_gene_fam_1 where gene_fam_2='Phytocyanin Superfamily' and
    loc in (select loc from atha_gene_fam_1 where gene_fam_2='Miscellaneous Membrane Protein Family');

update atha_gene_fam_1 set gene_fam_2='Miscellaneous Membrane Protein Family'
where
gene_fam_2 like '%Phytocyanin Superfamily%' ;

/* change gene families based on domains */
update atha_gene_fam_1 set gene_fam_2='E2F-DP Transcription Factor Family'
where
anot similar to '%(E2F family transcription factor|transcription factor Dp)%' and pfam_dom_auto similar to
'%({E2F_CC-MB,E2F_TDP}|{E2F_TDP}|{DP,E2F_TDP})%' and gene_fam_2 not like '%E2F-DP Transcription Factor Family%';

delete from atha_gene_fam_1 where loc ~ '(AT4G05110|AT4G05120|AT4G05140)' and gene_fam like '%EF-hand containing proteins%';

update atha_gene_fam_1 set gene_fam_2='Amino Acid/Auxin Permease AAAP Family'
where
anot similar to '%(transmembrane amino acid transporter protein)%' and pfam_dom_auto similar to
'%(Aa_trans)%' and gene_fam_2 not like '%Amino Acid/Auxin Permease AAAP Family%';

/* no genes associated with it */
delete from atha_gene_fam_1 where gene_fam_2~'Calcineurin B-like Calcium Sensor Proteins|Pirin-like proteins';
*/

/* Aquaporins; signature domain: MIP */
update atha_gene_fam_1 set gene_fam_2='Aquaporins' where gene_fam like '%Aquaporin%' and gene_fam_2 is null;
update atha_gene_fam_1 set gene_fam_2='Aquaporins' where gene_fam_2 like '%Major Intrinsic Protein MIP%' ;
delete from atha_gene_fam_1 where gene_fam='MIP family' and loc in (select loc from atha_gene_fam_1 where
    gene_fam='Aquaporin Families');
/* AT1G31880 this is not aquaporin but classified as aquaporin on TAIR Website */
update atha_gene_fam_1 set gene_fam_2=NULL where loc~'AT1G31880';

/* 14-3-3; signature domain: 14-3-3 */
update atha_gene_fam_1 set gene_fam_2='14-3-3 family' where gene_fam like '%14-3-3%' and gene_fam_2 is null;
delete from atha_gene_fam_1 where loc like '%AT5G16040%'; /* not 14-3-3 gene */

/* MYB; signature domain: Myb_DNA-binding; Myb_DNA-bind_6 and no Linker_histone */
update atha_gene_fam_1 set gene_fam_2='MYB Transcription Factor Family' where gene_fam='MYB' and
    lower(pfam_dom_auto)~lower('Myb_DNA-binding|Myb_DNA-bind_6') ;
delete from atha_gene_fam_1 where gene_fam like '%MYB Transcription Factor Family%';
delete from atha_gene_fam_1 where gene_fam like '%MYB-Related Transcription Factor Family%';
update atha_gene_fam_1 set gene_fam_2='MYB Transcription Factor Family' where lower(anot)~lower('MYB') and
    lower(pfam_dom_auto)~lower('Myb_DNA-binding|Myb_DNA-bind_6') and
    gene_fam_2 not like '%MYB Transcription Factor Family%';

/* IDZ; as defined in TAIR DB */
update atha_gene_fam_1 set gene_fam_2='IDZ Gene Family' where gene_fam like '%IDZ Gene Family%' and gene_fam_2 is null;

/*F-Box; FBA_1 2 and 3, FBD, F-box, F-box-like  and no TUB */
update atha_gene_fam_1 set gene_fam_2='F-Box Gene Family' where gene_fam like '%F-Box Proteins%' and gene_fam_2 is null;

/* ARF; Auxin_resp */
/* refer https://www.arabidopsis.org/browse/genefamily/index.jsp */
drop table if exists atha_arf_tf;
create table atha_arf_tf (id varchar(20));
\copy atha_arf_tf from '/Users/renesh/Renesh_Docs/Research/tamu/codes/python_scripts/ath_arf_tf.txt';
update atha_gene_fam_1 set gene_fam_2='ARF Transcription Factor Family' where loc in (select id from atha_arf_tf)
    and gene_fam like '%ARF Transcription Factor Family%' and pfam_dom_auto~'Auxin_resp' and gene_fam_2 is null;
insert into atha_gene_fam_1 (loc) select id from atha_arf_tf where id not in (select loc from atha_gene_fam_1);
update atha_gene_fam_1 set gene_fam_2='ARF Transcription Factor Family' where gene_fam is null and gene_fam_2 is null;

/* ABC Superfamily; ABC_tran, UPF0051, MlaE */
/* refer https://www.arabidopsis.org/browse/genefamily/index.jsp */
drop table if exists atha_abc_superfam;
create table atha_abc_superfam (id varchar(20));
\copy atha_abc_superfam from '/Users/renesh/Renesh_Docs/Research/tamu/codes/python_scripts/ath_abc_superfam.txt';
update atha_gene_fam_1 set gene_fam_2='ABC Superfamily' where loc in (select upper((regexp_split_to_array(id, '\.'))[1])
    from atha_abc_superfam);
insert into atha_gene_fam_1 (loc) select upper((regexp_split_to_array(id, '\.'))[1]) from atha_abc_superfam
    where upper((regexp_split_to_array(id, '\.'))[1]) not in (select loc from atha_gene_fam_1);
update atha_gene_fam_1 set gene_fam_2='ABC Superfamily' where gene_fam is null;
delete from atha_gene_fam_1 where gene_fam='ABC transporters' and loc in (select loc from atha_gene_fam_1
    where gene_fam='ABC Superfamily' );
delete from atha_gene_fam_1 where gene_fam='ABC transporters (Smart Lab)' and loc in (select loc from atha_gene_fam_1
    where gene_fam='ABC Superfamily' );

update atha_gene_fam_1 set gene_fam_2='ABC Superfamily' where gene_fam_2 like '%ABC transporters%' ;

/* aldehyde dehydrogenase; Aldedh and annotation*/
update atha_gene_fam_1 set gene_fam_2='Aldehyde Dehydrogenase Gene Family' where gene_fam like
    '%Aldehyde Dehydrogenase%' and gene_fam_2 is null;

/* Glycoside Hydrolase; Glyco_hydro_n, Cellulase, Alpha-amylase_n, Melibiase_2, Alpha_L_fucos, BNR_2, Raffinose_syn, Trehalase, Alpha-L-AF_C and annotation*/
update atha_gene_fam_1 set gene_fam_2='Glycoside Hydrolase Gene Family' where gene_fam like
    '%Glycoside Hydrolase Gene Families%' and gene_fam_2 is null;

/* BZR BRASSINAZOLE-RESISTANT; BES1_N and no Glyco_hydro_14*/
update atha_gene_fam_1 set gene_fam_2='BZR Transcription Factor Family' where gene_fam like
    '%BZR Transcription Factor Family%' and gene_fam_2 is null;

/* U-box; U-box */
update atha_gene_fam_1 set gene_fam_2='U-box Gene Family' where gene_fam_2 like '%Plant U-box protein%' ;

/* arid AT-Rich Interaction Domain; ARID use this example for more genes found not listed in TAIR gene families */
update atha_gene_fam_1 set gene_fam_2='ARID Transcription Factor Family' where gene_fam like
    '%ARID Transcription Factor Family%' and gene_fam_2 is null;

/* Cytoskeleton; Actin, Profilin, no PAE  */
update atha_gene_fam_1 set gene_fam_2='Cytoskeleton Gene Family' where gene_fam like '%Cytoskeleton%' and
    gene_fam_2 is null;

/* Carbohydrate Esterase; PAE|Esterase|Pectinesterase|Abhydrolase_3|LpxC */
update atha_gene_fam_1 set gene_fam_2='Carbohydrate Esterase Gene Family' where gene_fam like
    '%Carbohydrate Esterase Gene Families%' and gene_fam_2 is null;

/* Cytoplasmic Ribosomal Protein Gene Family; Ribosomal_S|Ribosomal_L|Ribosomal_60|S10_plectin|Ribosom_S */
update atha_gene_fam_1 set gene_fam_2=null where gene_fam like '%Cytoplasmic ribosomal protein gene family%' and
    gene_fam_2 like '%Calmodulin-binding Proteins%';
update atha_gene_fam_1 set gene_fam_2='Cytoplasmic Ribosomal Protein Gene Family' where gene_fam like
    '%Cytoplasmic ribosomal protein gene family%' and gene_fam_2 is null;

/* Receptor kinase-like protein family; Pkinase and WAK|LRR|Lectin_legB|GDPD|LysM|Usp|Stress-antifung|Malectin|RCC1_2|Thaumatin|B_lectin|Lectin_C|Pkinase_Tyr|B_lectin */
delete from atha_gene_fam_1 where gene_fam='Strubbelig Receptor Family' and
    loc in (select loc from atha_gene_fam_1 where gene_fam='Receptor kinase-like protein family');
update atha_gene_fam_1 set gene_fam_2='Receptor kinase-like protein family' where gene_fam_2 like
    '%Strubbelig Receptor Family%' ;

/* bZIP Transcription Factor Family; bZIP_* */
update atha_gene_fam_1 set gene_fam_2='bZIP Transcription Factor Family' where gene_fam like
    '%bZIP Transcription Factor Family%' and gene_fam_2 is null;
update atha_gene_fam_1 set gene_fam_2='bZIP Transcription Factor Family' where gene_fam ~ 'TGA3|`' and gene_fam_2 is null;
drop table if exists atha_bzip;
create table atha_bzip as select distinct loc from atha_gene_fam_1 where gene_fam like '%basic region leucine zipper (bZIP)%';
alter table atha_bzip rename loc to id;
delete from atha_gene_fam_1 where gene_fam like '%basic region leucine zipper (bZIP)%';
insert into atha_gene_fam_1 (loc) select id from atha_bzip where id not in (select loc from atha_gene_fam_1);
update atha_gene_fam_1 set gene_fam_2='bZIP Transcription Factor Family' where gene_fam is null and gene_fam_2 is null;

/* Core Cell Cycle Gene Family; complex domain */
update atha_gene_fam_1 set gene_fam_2='Core Cell Cycle Gene Family' where gene_fam like
    '%Core Cell Cycle Genes%' and gene_fam_2 is null;
delete from atha_gene_fam_1 where loc like '%AT5G22220%' and gene_fam_2 like '%Core Cell Cycle Gene Family%';

/* Equilibrative Nucleoside Transporter (ENT) Gene Family; Nucleoside_tran */
/*delete from atha_gene_fam_1 where gene_fam='Equilibrative Nucleoside Transporter Family (ENT)' and
    loc in (select loc from atha_gene_fam_1 where gene_fam='Organic Solute Cotransporters');*/
update atha_gene_fam_1 set gene_fam_2='Equilibrative Nucleoside Transporter (ENT) Gene Family ' where
    pfam_dom_auto~'Nucleoside_tran' and gene_fam_2 is null;

/* GRAS Transcription Factor Family; GRAS */
update atha_gene_fam_1 set gene_fam_2='GRAS Transcription Factor Family' where gene_fam like '%GRAS Gene Family%' and
    gene_fam_2 is null;
delete from atha_gene_fam_1 where gene_fam='GRAS Transcription Factor Family';

/* Tify Gene Family; tify */
update atha_gene_fam_1 set gene_fam_2='Tify Gene Family' where pfam_dom_auto ~ 'tify' ;

/* VOZ-9 Transcription Factor Family; no domain just based on locus */
update atha_gene_fam_1 set gene_fam_2='VOZ-9 Transcription Factor Family' where loc ~ 'AT1G28520|AT2G42400' ;

/* zinc finger-homeobox gene family */
delete from atha_gene_fam_1 where gene_fam='ZF-HD Transcription Factor Family' and loc in
    (select loc from atha_gene_fam_1 where gene_fam='zinc finger-homeobox gene family' );
update atha_gene_fam_1 set gene_fam_2='ZF-HD Transcription Factor Family' where pfam_dom_auto ~ 'ZF-HD_dimer' ;

/* ARIADNE Gene Family; IBR */
update atha_gene_fam_1 set gene_fam_2='ARIADNE Gene Family' where gene_fam like '%ARIADNE gene family%' and
    gene_fam_2 is null;

/* C2C2-Gata Transcription Factor Family; GATA and not tify */
update atha_gene_fam_1 set gene_fam_2='C2C2-Gata Transcription Factor Family' where gene_fam like
    '%C2C2-Gata Transcription Factor Family%' and gene_fam_2 is null;

/* SNARE and Associated Proteins; Syntaxin|SNARE|Longin|V-SNARE_C|SNAP|Synaptobrevin|Sec1|Sec20|CDC48 */
update atha_gene_fam_1 set gene_fam_2='SNARE and Associated Proteins' where gene_fam_2 like '%SNARE%' ;

/* COBRA Gene Family; COBRA  */
update atha_gene_fam_1 set gene_fam_2='COBRA Gene Family' where gene_fam like '%COBRA Gene Family%' and
    gene_fam_2 is null;

/* MAP Kinase (MAPK) Family; ath db */
update atha_gene_fam_1 set gene_fam_2='MAP Kinase (MAPK) Gene Family' where gene_fam~'MAPK\)' and gene_fam_2 is null;

/* MAP Kinase kinase (MAPKK) Family; ath db */
update atha_gene_fam_1 set gene_fam_2='MAP Kinase Kinase (MAPKK) Gene Family' where gene_fam~'MAPKK\)' and
    gene_fam_2 is null;

/* MAP Kinase Kinase Kinase (MAPKKK) Family; ath db */
update atha_gene_fam_1 set gene_fam_2='MAP Kinase Kinase Kinase (MAPKKK) Gene Family' where gene_fam~'MAPKKK\)' and
    gene_fam_2 is null;

/* MAP Kinase Kinase Kinase Kinase (MAPKKKK) Family */
update atha_gene_fam_1 set gene_fam_2='MAP Kinase Kinase Kinase Kinase (MAPKKKK) Family' where gene_fam~'MAPKKKK\)' and
    gene_fam_2 is null;

/* Histidine Kinase Gene Family; HATPase_c|GAF|CHASE|PHY */
update atha_gene_fam_1 set gene_fam_2='Histidine Kinase Gene Family' where gene_fam like '%Histidine Kinase%'
    and gene_fam_2 is null;

/* Expansin Gene Family; Pollen_allerg_1|DPBB_1 */
update atha_gene_fam_1 set gene_fam_2='Expansin Gene Family' where gene_fam like '%Expansins%' and gene_fam_2 is null;

/* SBP Transcription Factor Family; SBP */
update atha_gene_fam_1 set gene_fam_2='SBP Transcription Factor Family' where pfam_dom_auto~'SBP' and
    pfam_dom_auto!~'SBP_bac' and gene_fam_2 is null;

/* Non-SMC Element family; Nse4_C */


/* AT-hook Motif Nuclear Localized (AHL) Family; DUF296 */
/* it was not listed gene family db downloaded file and therfore externally added */
drop table if exists atha_ahl;
create table atha_ahl (id varchar(20));
\copy atha_ahl from '/Users/renesh/Renesh_Docs/Research/tamu/codes/python_scripts/ath_ahl.txt';
insert into atha_gene_fam_1 (loc) select id from atha_ahl where id not in (select loc from atha_gene_fam_1);
update atha_gene_fam_1 set gene_fam_2='AT-hook Motif Nuclear Localized (AHL) Family' where gene_fam is null and
    gene_fam_2 is null;

/* PRT Gene Family; Pribosyltran|UPRTase|GATase_7 */
update atha_gene_fam_1 set gene_fam_2='PRT Gene Family' where gene_fam like '%Phosphoribosyltransferases (PRT)%' ;
update atha_gene_fam_1 set gene_fam_2='PRT Gene Family' where gene_fam like
    '%Miscellaneous Membrane Protein Family%' and sub_fam like '%Adenine phosphoribosyltransferase%' ;

/* Structural Maintenance of Chromosomes family; SMC_N */
update atha_gene_fam_1 set gene_fam_2='Structural Maintenance of Chromosomes family' where  gene_fam~'Structural';

/* RRE (Rapid Response to Elicitors) */
update atha_gene_fam_1 set gene_fam_2='RRE (Rapid Response to Elicitors)' where lower(loc) ~ lower('At2g35000|At3g16720|At3g05200|At4g15975') ;

/* C2C2-YABBY Transcription Factor Family; YABBY */
update atha_gene_fam_1 set gene_fam_2='C2C2-YABBY Transcription Factor Family' where gene_fam like
    '%C2C2-YABBY Transcription Factor Family%' and gene_fam_2 is null;

/*
/* FtsH Gene Family */
update atha_gene_fam_1 set gene_fam_2='FtsH Gene Family' where gene_fam like
    '%FtsH: AAA ATP-dependent zinc metallopeptidase%' and gene_fam_2 is null;
update atha_gene_fam_1 set gene_fam_2='FtsH Gene Family ' where anot similar to '%(FtsH protease)%' and
    pfam_dom_auto similar to '%(Peptidase_M41)%' and gene_fam_2 not like '%FtsH Gene Family%';
*/

/* Nicotianamine Synthase Gene Family; NAS */
update atha_gene_fam_1 set gene_fam_2='Nicotianamine Synthase Gene Family' where
    gene_fam_2 like '%Nicotianamine Synthase%' ;

/* Chloroplast and Mitochondria gene Family; Chloroa_b-bind|60KD_IMP|Cyt_b-c1_8|Cytochrom_C1|DsbD|Mito_carr|Pyr_redox_2|Tim17|Cytochrom_B561|FA_hydroxylase|TIC20|SecY */
update atha_gene_fam_1 set gene_fam_2='Chloroplast and Mitochondria gene Family' where gene_fam like
    '%Chloroplast and Mitochondria gene families%' and gene_fam_2 is null;

/* PHD Gene Family; PHD_Oberon */

/* PHD Transcription Factor Family; PHD domain */

/* Histidine Phosphotransfer Proteins; Hpt */
update atha_gene_fam_1 set gene_fam_2='Histidine Phosphotransfer Proteins' where gene_fam like
    '%Histidine Phosphotransfer Proteins%' and gene_fam_2 is null;

/* Cytochrome b5; FAD_binding|Cyt-b5 and annotation */
update atha_gene_fam_1 set gene_fam_2='Cytochrome b5' where gene_fam like '%Cytochrome b5%' and gene_fam_2 is null;

/* Eukaryotic Initiation Factor Gene Family; complex and defined from ath db  */
update atha_gene_fam_1 set gene_fam_2=null where gene_fam like '%Eukaryotic Initiation Factor Gene Family%'
    and gene_fam_2 like '%Calmodulin-binding Proteins%';
update atha_gene_fam_1 set gene_fam_2='Eukaryotic Initiation Factor Gene Family' where gene_fam like
    '%Eukaryotic Initiation Factor Gene Family%' and gene_fam_2 is null;

/* CAMTA Transcription Factor Family; CG-1,IQ */
drop table if exists atha_camta;
create table atha_camta (id varchar(50));
copy atha_camta from program 'tail -n +2 /Users/renesh/Renesh_Docs/Research/tamu/codes/psql_scripts_b/atah_camta_family_genes.txt';
update atha_gene_fam_1 set gene_fam_2='CAMTA Transcription Factor Family' where loc in (select id from atha_camta );

/* IQD Protein Family; IQ, DUF4005 and annotation */
update atha_gene_fam_1 set gene_fam_2='IQD Protein Family' where gene_fam like '%IQD Protein Family%' and
    gene_fam_2 is null;

/* Subtilisin-like Serine Gene Family; Peptidase_S8, ComA */
update atha_gene_fam_1 set gene_fam_2='Subtilisin-like Serine Gene Family' where gene_fam_2 like '%Subtilisin%' ;

/* Trihelix Transcription Factor Family; Myb_DNA-bind_4 */

/* Kinesins Gene Family; Kinesin,Microtub_bd */
update atha_gene_fam_1 set gene_fam_2=null where gene_fam like '%Kinesins%' and gene_fam_2 like
    '%Calmodulin-binding Proteins%';
update atha_gene_fam_1 set gene_fam_2='Kinesins Gene Family' where gene_fam like '%Kinesins%' and gene_fam_2 is null;

/* NAC Transcription Factor Family; NAM */

/* CPP Transcription Factor Family; TCR */
update atha_gene_fam_1 set gene_fam_2='CPP Transcription Factor Family' where gene_fam like
    '%CPP Transcription Factor Family%' and gene_fam_2 is null;

/* HSF Transcription Factor Family; HSF_DNA-bind */
update atha_gene_fam_1 set gene_fam_2='HSF Transcription Factor Family' where gene_fam like
    '%HSF Transcription Factor Family%' and gene_fam_2 is null;
delete from atha_gene_fam_1 where gene_fam='Heat Shock Transcription Factor Family';

/* Pollen Coat Proteome; Oleosin|Lipase_GDSL|Stress-antifung|Caleosin and annotation */

/* TCP transcription factor family; TCP */
update atha_gene_fam_1 set gene_fam_2='TCP transcription factor family' where
    gene_fam_2 like '%TCP transcription factor family%' ;

/* ARR-B Transcription Factor Family; Response_reg */
update atha_gene_fam_1 set gene_fam_2='ARR-B Transcription Factor Family' where gene_fam like
    '%ARR-B Transcription Factor Family%' and gene_fam_2 is null;
delete from atha_gene_fam_1 where gene_fam='Response Regulator' and
    loc in (select loc from atha_gene_fam_1 where gene_fam='ARR-B Transcription Factor Family');
update atha_gene_fam_1 set gene_fam_2='ARR-B Transcription Factor Family' where gene_fam_2 like '%Response Regulator%' ;

/* Nodulin-like protein family ; EamA|Nodulin-like|VIT1|GDA1_CD39*/

/* FH2 protein (formin) Gene Family; FH2 */
update atha_gene_fam_1 set gene_fam_2='FH2 protein (formin) Gene Family' where gene_fam like '%FH2 proteins%'
    and gene_fam_2 is null;

/* Core DNA replication machinery Family; as per defined in ath db */
update atha_gene_fam_1 set gene_fam_2='Core DNA replication machinery Family' where gene_fam like
    '%Core DNA replication machinery%' and gene_fam_2 is null;

/* Myosin; Myosin_head */

/* GeBP Transcription Factor Family; DUF573 */
update atha_gene_fam_1 set gene_fam_2='GeBP Transcription Factor Family' where gene_fam like
    '%GeBP Transcription Factor Family%' and gene_fam_2 is null;

/* BBR/BPC Transcription Factor Family; GAGA_bind */
update atha_gene_fam_1 set gene_fam_2='BBR/BPC Transcription Factor Family' where gene_fam like
    '%BBR/BPC Transcription Factor Family%' and gene_fam_2 is null;
delete from atha_gene_fam_1 where gene_fam='BBR/BPC-family of GAGA-motif binding transcription factors' and loc in
    (select loc from atha_gene_fam_1 where gene_fam='BBR/BPC Transcription Factor Family' );

/* TRM (TON1 Recruiting Motif) Superfamily; ath db */
/* refer https://www.arabidopsis.org/browse/genefamily/index.jsp */
drop table if exists atha_trm;
create table atha_trm (id varchar(20));
\copy atha_trm from '/Users/renesh/Renesh_Docs/Research/tamu/codes/python_scripts/ath_trm.txt';
insert into atha_gene_fam_1 (loc) select id from atha_trm;
update atha_gene_fam_1 set gene_fam_2='TRM (TON1 Recruiting Motif) Superfamily' where gene_fam is null and
    gene_fam_2 is null;

/* TUB Transcription Factor Family; Tub */

/* HRT Transcription Factor Family; ath db */
update atha_gene_fam_1 set gene_fam_2='HRT Transcription Factor Family' where gene_fam like
    '%HRT Transcription Factor Family%' and gene_fam_2 is null;

/* Glutamine dumper (GDU) family; ath db */
update atha_gene_fam_1 set gene_fam_2='Glutamine dumper (GDU) family' where gene_fam like
    '%Glutamine dumper (GDU) family%' and gene_fam_2 is null;

/* Short Under Blue Light (SUBs); again check this one */


/* EXO70 exocyst subunit family; Exo70 */
update atha_gene_fam_1 set gene_fam_2='EXO70 exocyst subunit family' where gene_fam like
    '%EXO70 exocyst subunit family%' and gene_fam_2 is null;

/* Lateral Organ Boundaries Gene Family; LOB */
update atha_gene_fam_1 set gene_fam_2='Lateral Organ Boundaries Gene Family' where gene_fam like
    '%Lateral Organ Boundaries Gene Family%' and gene_fam_2 is null;
delete from atha_gene_fam_1 where gene_fam='AS2 family' and loc in (select loc from atha_gene_fam_1 where
    gene_fam='Lateral Organ Boundaries Gene Family' );

/* Ion Channel Family; Ion_trans|ANF_receptor|Voltage_CLC */
/* update atha_gene_fam_1 set gene_fam_2=null where gene_fam like '%Ion Channel Families%' and gene_fam_2 like
    '%Calmodulin-binding Proteins%';*/
update atha_gene_fam_1 set gene_fam_2='Ion Channel Family' where gene_fam like '%Ion Channel Families%' and
    gene_fam_2 is null;

/* NADPH P450 reductases; FAD_binding_1 */

/* Single Myb Histone (SMH) Gene Family;  Myb_DNA-bind_6|Linker_histone and annotation*/
update atha_gene_fam_1 set gene_fam_2='Single Myb Histone (SMH) Gene Family' where gene_fam_2 like '%Single Myb Histone%' ;

/* Plant defensins superfamily; Gamma-thionin */
drop table if exists atha_pdf;
create table atha_pdf (id varchar(50));
copy atha_pdf from program 'tail -n +2 /Users/renesh/Renesh_Docs/Research/tamu/codes/psql_scripts_b/atha_pdf_family_genes.txt';
update atha_gene_fam_1 set gene_fam_2='Plant defensins superfamily' where loc in (select id from atha_pdf );

/* JUMONJI Transcription Factor Family; JmjC */

/* AtRKD Transcription Factor Family; RWP-RK */
update atha_gene_fam_1 set gene_fam_2='AtRKD Transcription Factor Family' where gene_fam like
    '%AtRKD Transcription Factor Family%' and gene_fam_2 is null;

/* RCI2 gene Family; Pmp3 */

/* EIL Transcription Factor Family; EIN3 */
update atha_gene_fam_1 set gene_fam_2='EIL Transcription Factor Family' where gene_fam like
    '%EIL Transcription Factor Family%' and gene_fam_2 is null;

/* RAD5/RAD16-like Gene Family; SNF2_N */
update atha_gene_fam_1 set gene_fam_2='RAD5/RAD16-like Gene Family' where
    gene_fam_2 like '%Rad5/16-like gene family%' ;

/* AGC Kinase Gene Family; ath db */
update atha_gene_fam_1 set gene_fam_2='AGC Kinase Gene Family' where gene_fam like '%AGC Family%' and gene_fam_2 is null;

/* Monolignol Biosynthesis; ath db */
drop table if exists atha_mling;
create table atha_mling (id varchar(50));
copy atha_mling from program 'tail -n +2 /Users/renesh/Renesh_Docs/Research/tamu/codes/psql_scripts_b/atha_mling_family_genes.txt';
update atha_gene_fam_1 set gene_fam_2='Monolignol Biosynthesis' where loc in (select id from atha_mling );

/* NLP Transcription Factor Family; NIN-LIKE PROTEIN; RWP-RK and ath db */
drop table if exists atha_nlp;
create table atha_nlp (id varchar(50));
copy atha_nlp from program 'tail -n +2 /Users/renesh/Renesh_Docs/Research/tamu/codes/psql_scripts_b/atha_nlp_family_genes.txt';
update atha_gene_fam_1 set gene_fam_2='NLP Transcription Factor Family' where loc in (select id from atha_nlp )
    and gene_fam_2 is null;


/* Magnesium Transporter (MRS2) Gene Family; CorA and ath db */
update atha_gene_fam_1 set gene_fam_2='Magnesium Transporter (MRS2) Gene Family' where gene_fam like
    '%Magnesium Transporter Gene Family%' ;

/* Amino Acid/Auxin Permease AAAP Family; Aa_trans and ath db */
drop table if exists atha_auxin;
create table atha_auxin (id varchar(50));
copy atha_auxin from program 'tail -n +2 /Users/renesh/Renesh_Docs/Research/tamu/codes/psql_scripts_b/atha_auxin_family_genes.txt';
update atha_gene_fam_1 set gene_fam_2='Amino Acid/Auxin Permease AAAP Family' where loc in (select id from atha_auxin )
    and gene_fam_2 is null;

/* Glycosyltransferase Gene Family; ath db and see domain criteria below */
drop table if exists atha_glycotr;
create table atha_glycotr (id varchar(50));
copy atha_glycotr from program 'tail -n +2 /Users/renesh/Renesh_Docs/Research/tamu/codes/psql_scripts_b/atha_glycotr_family_genes.txt';
update atha_gene_fam_1 set gene_fam_2='Glycosyltransferase Gene Family' where loc in (select id from atha_glycotr )
    and gene_fam_2 is null;

/* WRKY Transcription Factor Superfamily; WRKY */
drop table if exists atha_wrky;
create table atha_wrky (id varchar(50));
copy atha_wrky from program 'tail -n +2 /Users/renesh/Renesh_Docs/Research/tamu/codes/psql_scripts_b/atha_wrky_family_genes.txt';
update atha_gene_fam_1 set gene_fam_2='WRKY Transcription Factor Superfamily' where loc in (select id from atha_wrky )
    and gene_fam_2 is null;

/* Class III peroxidase; peroxidase */

/* G2-like Transcription Factor Family; ath db */
update atha_gene_fam_1 set gene_fam_2='G2-like Transcription Factor Family' where gene_fam like
    '%G2-like Transcription Factor Family%' and gene_fam_2 is null;

/* bHLH Transcription Factor Family; HLH|bHLH-MYC_N */
update atha_gene_fam_1 set gene_fam_2='bHLH Transcription Factor Family' where gene_fam like
    '%bHLH Transcription Factor Family%' and gene_fam_2 is null;
drop table if exists atha_bhlh;
create table atha_bhlh as select distinct loc from atha_gene_fam_1 where gene_fam like '%basic Helix-Loop-Helix (bHLH)%';
alter table atha_bhlh rename loc to id;
delete from atha_gene_fam_1 where gene_fam like '%basic Helix-Loop-Helix (bHLH)%';
insert into atha_gene_fam_1 (loc) select id from atha_bhlh where id not in (select loc from atha_gene_fam_1);
update atha_gene_fam_1 set gene_fam_2='bHLH Transcription Factor Family' where gene_fam is null and gene_fam_2 is null;

/* Calcium Dependent Protein Kinase; Pkinase and annotation */
update atha_gene_fam_1 set gene_fam_2='Calcium Dependent Protein Kinase' where gene_fam like
    '%Calcium Dependent Protein Kinase%' and gene_fam_2 is null;
delete from atha_gene_fam_1 where gene_fam like '%CDPKs%';

/* Primary Pumps (ATPases) Gene Family; ath db */
drop table if exists atha_patp;
create table atha_patp (id varchar(50));
copy atha_patp from program 'tail -n +2 /Users/renesh/Renesh_Docs/Research/tamu/codes/psql_scripts_b/atha_patp_family_genes.txt';
update atha_gene_fam_1 set gene_fam_2='Primary Pumps (ATPases) Gene Family' where loc in (select id from atha_patp )
    and gene_fam_2 is null;

/* C2C2-Dof Transcription Factor Family; zf-Dof */
drop table if exists atha_dof;
create table atha_dof (id varchar(50));
copy atha_dof from program 'tail -n +2 /Users/renesh/Renesh_Docs/Research/tamu/codes/psql_scripts_b/atha_dof_family_genes.txt';
update atha_gene_fam_1 set gene_fam_2='C2C2-Dof Transcription Factor Family' where loc in (select id from atha_dof )
    and gene_fam_2 is null;

/* Mechanosensitive Ion Channel Family; MS_channel */

/* Homeobox Transcription Factor Family; Homeobox|RRM_1 */
update atha_gene_fam_1 set gene_fam_2='Homeobox Transcription Factor Family' where gene_fam like
    '%Homeobox Transcription Factor Family%' and gene_fam_2 is null;
delete from atha_gene_fam_1 where gene_fam='WOX gene family' and loc in (select loc from atha_gene_fam_1 where
    gene_fam='Homeobox Transcription Factor Family');

/* CBL-interacting serione-threonine Protein Kinases; Pkinase and annotaion */
update atha_gene_fam_1 set gene_fam_2='CBL-interacting serione-threonine Protein Kinases' where
    gene_fam like '%CBL-interacting serione-threonine Protein Kinases%' and gene_fam_2 is null;

/* C3H Transcription Factor Family; ath db */
update atha_gene_fam_1 set gene_fam_2=null where gene_fam like '%C3H Transcription Factor Family%' and gene_fam_2
    like '%Calmodulin-binding Proteins%';
update atha_gene_fam_1 set gene_fam_2='C3H Transcription Factor Family' where gene_fam like
    '%C3H Transcription Factor Family%' and gene_fam_2 is null;

/* CCAAT-HAP2 Transcription Factor Family; CBFB_NFYA */
update atha_gene_fam_1 set gene_fam_2='CCAAT-HAP2 Transcription Factor Family' where gene_fam like
    '%CCAAT-HAP2 Transcription Factor Family%' and gene_fam_2 is null;

/* CCAAT-HAP5 Transcription Factor Family; CBFD_NFYB_HMF */
update atha_gene_fam_1 set gene_fam_2='CCAAT-HAP5 Transcription Factor Family' where gene_fam like
    '%CCAAT-HAP5 Transcription Factor Family%' and gene_fam_2 is null;

/* Leucine-rich repeat (LRR) family protein; LRR_n domain */
update atha_gene_fam_1 set gene_fam_2='Leucine-rich repeat extensin' where gene_fam like
    '%Leucine-rich repeat extensin%' and gene_fam_2 is null;
update atha_gene_fam_1 set gene_fam_2='Leucine-rich repeat (LRR) family protein' where gene_fam_2 like
    '%Leucine-rich repeat extensin%' ;
update atha_gene_fam_1 set anot='Leucine-rich repeat (LRR) family protein'
    where loc ~ '(AT2G15880|AT3G22800|AT4G13340|AT3G24480|AT1G49490|AT4G18670|AT4G33970|AT3G19020|AT5G25550)';
update atha_gene_fam_1 set anot='leucine-rich repeat/extensin 2' where loc ~ '(AT1G62440|AT1G12040)';

/* C2C2-CO-like Transcription Factor Family; zf-B_box|CCT and annotation */
update atha_gene_fam_1 set gene_fam_2='C2C2-CO-like Transcription Factor Family' where gene_fam like
    '%C2C2-CO-like Transcription Factor Family%' and gene_fam_2 is null;

/* AP2-EREBP Transcription Factor Family; AP2 */
drop table if exists atha_ap2;
create table atha_ap2 (id varchar(50));
copy atha_ap2 from program 'tail -n +2 /Users/renesh/Renesh_Docs/Research/tamu/codes/psql_scripts_b/atha_ap2_family_genes.txt';
update atha_gene_fam_1 set gene_fam_2='AP2-EREBP Transcription Factor Family' where loc in (select id from atha_ap2 )
    and gene_fam_2 is null;

/* FtsH Gene Family; Peptidase_M41 */
drop table if exists atha_ftsh;
create table atha_ftsh (id varchar(50));
copy atha_ftsh from program 'tail -n +2 /Users/renesh/Renesh_Docs/Research/tamu/codes/psql_scripts_b/atha_ftsh_family_genes.txt';
update atha_gene_fam_1 set gene_fam_2='FtsH Gene Family' where loc in (select id from atha_ftsh )
    and gene_fam_2 is null;

/* Glutathione S-transferase Family; GST */

/* Orphan Transcription Factor Family */
update atha_gene_fam_1 set gene_fam_2='Orphan Transcription Factor Family' where loc ~ 'AT5G61850|AT1G06040' ;

/* PP2C-type phosphatases; PP2C */

/* EF-hand Containing Proteins; ath db */


/* C2H2 Transcription Factor Family; ath db */
update atha_gene_fam_1 set gene_fam_2='C2H2 Transcription Factor Family' where gene_fam like
    '%C2H2 Transcription Factor Family%' and gene_fam_2 is null;

/* E2F-DP Transcription Factor Family; ath db some genes are shared with core cell cycle */
drop table if exists atha_efdp;
create table atha_efdp (id varchar(50));
copy atha_efdp from program 'tail -n +2 /Users/renesh/Renesh_Docs/Research/tamu/codes/psql_scripts_b/atha_efdp_family_genes.txt';
update atha_gene_fam_1 set gene_fam_2='E2F-DP Transcription Factor Family' where loc in (select id from atha_efdp )
    and gene_fam_2 is null;

/* MADS Transcription Factor Family; SRF-TF and annotation */
drop table if exists atha_mads;
create table atha_mads (id varchar(50));
copy atha_mads from program 'tail -n +2 /Users/renesh/Renesh_Docs/Research/tamu/codes/psql_scripts_b/atha_mads_family_genes.txt';
update atha_gene_fam_1 set gene_fam_2='MADS Transcription Factor Family' where loc in (select id from atha_mads )
    and gene_fam_2 is null;

/* Protein tyrosine phosphatase (PTP) family; ath db and see domain info below complex  */


/* ABI3VP1 Transcription Factor Family; ath db */
update atha_gene_fam_1 set gene_fam_2='ABI3VP1 Transcription Factor Family' where gene_fam like
    '%ABI3VP1 Transcription Factor Family%' and gene_fam_2 is null;

/* B3 DNA Binding Superfamily; mixed with ARF and ABI3VP1 and others check table atha_verify_b3  */
drop table if exists atha_b3;
create table atha_b3 (id varchar(50));
copy atha_b3 from program 'tail -n +2 /Users/renesh/Renesh_Docs/Research/tamu/codes/psql_scripts_b/atha_b3_family_genes.txt';
update atha_gene_fam_1 set gene_fam_2='B3 DNA Binding Superfamily' where loc in (select id from atha_b3 )
    and gene_fam_2 is null;

/* CCAAT-DR1 Transcription Factor Family; ath db */
update atha_gene_fam_1 set gene_fam_2='CCAAT-DR1 Transcription Factor Family' where loc ~ 'AT5G08190|AT5G23090' ;

/* Lipid Metabolism Gene Family; ath db and complex */
drop table if exists atha_lipmeta;
create table atha_lipmeta (id varchar(50));
copy atha_lipmeta from program 'tail -n +2 /Users/renesh/Renesh_Docs/Research/tamu/codes/psql_scripts_b/atha_lipmeta_family_genes.txt';
update atha_gene_fam_1 set gene_fam_2='Lipid Metabolism Gene Family' where loc in (select id from atha_lipmeta )
    and gene_fam_2 is null;

/* Inorganic Solute Cotransporters; ath db and complex domain */
update atha_gene_fam_1 set gene_fam_2='Inorganic Solute Cotransporters' where gene_fam like
    '%Inorganic Solute Cotransporters%' and gene_fam_2 is null;

/* Organic Solute Cotransporters */
drop table if exists atha_orgt;
create table atha_orgt (id varchar(50));
copy atha_orgt from program 'tail -n +2 /Users/renesh/Renesh_Docs/Research/tamu/codes/psql_scripts_b/atha_orgt_family_genes.txt';
update atha_gene_fam_1 set gene_fam_2='Organic Solute Cotransporters' where loc in (select id from atha_orgt )
    and gene_fam_2 is null;

/* Polysaccharide Lyase Gene Family; Rhamnogal_lyase|Pec_lyase */
update atha_gene_fam_1 set gene_fam_2='Polysaccharide Lyase Gene Family' where gene_fam like
    '%Polysaccharide Lyase Gene Families%' ;

/* GRF Transcription Factor Family; WRC */
update atha_gene_fam_1 set gene_fam_2='GRF Transcription Factor Family' where gene_fam like
    '%GRF Transcription Factor Family%' and gene_fam_2 is null;

/* CCAAT-HAP3 Transcription Factor Family; CBFD_NFYB_HMF */
update atha_gene_fam_1 set gene_fam_2='CCAAT-HAP3 Transcription Factor Family' where gene_fam like
    '%CCAAT-HAP3 Transcription Factor Family%' and gene_fam_2 is null;

/*  Short Under Blue Light (SUBs); ath db */
update atha_gene_fam_1 set gene_fam_2='Short Under Blue Light (SUBs)' where lower(loc) ~ lower('AT4G12700|AT2G04280|AT4G08810') ;

/* Sulfurtransferasese / Rhodanese Family; Rhodanese */
drop table if exists atha_stran;
create table atha_stran (id varchar(50));
copy atha_stran from program 'tail -n +2 /Users/renesh/Renesh_Docs/Research/tamu/codes/psql_scripts_b/atha_stran_family_genes.txt';
update atha_gene_fam_1 set gene_fam_2='Sulfurtransferasese / Rhodanese Family' where loc in (select id from atha_stran )
    and gene_fam_2 is null;

/* Alfin-like Transcription Factor Family; Alfin,PHD */
update atha_gene_fam_1 set gene_fam_2='Alfin-like Transcription Factor Family' where gene_fam like '%Alfin%' and
    gene_fam_2 is null;

/* Antiporter Superfamily */
update atha_gene_fam_1 set gene_fam_2='Antiporter Superfamily' where gene_fam like '%Antiporter%' and gene_fam_2 is null;
delete from atha_gene_fam_1 where gene_fam='Antiporter Superfamily' and loc in (select loc from atha_gene_fam_1 where
    gene_fam='Antiporters' );
update atha_gene_fam_1 set gene_fam_2='Antiporter Superfamily' where gene_fam like '%Antiporter Superfamily%' and
    gene_fam_2 is null;

/* Calmodulin-binding Proteins */
/* refer https://www.arabidopsis.org/browse/genefamily/index.jsp */
drop table if exists atha_cbp;
create table atha_cbp (id varchar(20));
\copy atha_cbp from '/Users/renesh/Renesh_Docs/Research/tamu/codes/python_scripts/ath_cbp.txt';
update atha_gene_fam_1 set gene_fam_2='Calmodulin-binding Proteins' where loc in (select id from atha_cbp) and
    gene_fam_2 is null;
insert into atha_gene_fam_1 (loc) select id from atha_cbp where id not in (select loc from atha_gene_fam_1);
update atha_gene_fam_1 set gene_fam_2='Calmodulin-binding Proteins' where gene_fam is null and gene_fam_2 is null;
update atha_gene_fam_1 set gene_fam_2=null where gene_fam like '%Primary Pumps (ATPases) Gene%' and gene_fam_2 like
    '%Calmodulin-binding Proteins%';
update atha_gene_fam_1 set gene_fam_2=null where gene_fam like '%Miscellaneous Membrane Protein Families%' and
    gene_fam_2 like '%Calmodulin-binding Proteins%';

/* Cytochrome P450; p450 */
/* refer https://www.arabidopsis.org/browse/genefamily/index.jsp */
drop table if exists atha_cytp450;
create table atha_cytp450 (id varchar(20));
\copy atha_cytp450 from '/Users/renesh/Renesh_Docs/Research/tamu/codes/python_scripts/ath_cytp450.txt';
update atha_gene_fam_1 set gene_fam_2='Cytochrome P450' where loc in (select id from atha_cytp450) and gene_fam_2 is null;
insert into atha_gene_fam_1 (loc) select id from atha_cytp450 where id not in (select loc from atha_gene_fam_1);
update atha_gene_fam_1 set gene_fam_2='Cytochrome P450' where gene_fam is null and gene_fam_2 is null;
update atha_gene_fam_1 set gene_fam_2='Cytochrome P450' where anot similar to '%(cytochrome P450)%' and pfam_dom_auto
    similar to '%({p450})%' and gene_fam_2 not like '%Cytochrome P450%';

/* Miscellaneous Membrane Protein Family; ath db */
drop table if exists atha_misc;
create table atha_misc (id varchar(50));
copy atha_misc from program 'tail -n +2 /Users/renesh/Renesh_Docs/Research/tamu/codes/psql_scripts_b/atha_misc_family_genes.txt';
update atha_gene_fam_1 set gene_fam_2='Miscellaneous Membrane Protein Family' where loc in (select id from atha_misc )
    and gene_fam_2 is null;

/* Acyl Lipid Metabolism Gene Family; ath db some genes are shared with other families */
drop table if exists atha_alipid;
create table atha_alipid (id varchar(50));
copy atha_alipid from program 'tail -n +2 /Users/renesh/Renesh_Docs/Research/tamu/codes/psql_scripts_b/atha_alipid_family_genes.txt';
update atha_gene_fam_1 set gene_fam_2='Acyl Lipid Metabolism Gene Family' where loc in (select id from atha_alipid )
    and gene_fam_2 is null;

/* Monocot gene families */
insert into atha_gene_fam_1 (gene_fam_2) values('BTB Gene Family');


/*   %%%%%%%%%%%%%%% */


/*
########################
GENFAM TABLES
########################
*/


create or replace function public.array_unique(arr anyarray)
    returns anyarray as
    $body$
        select array( select distinct unnest($1) )
    $body$ language "sql";

create or replace function public.format_fam_table(phyt_table varchar(30), table_path text, species varchar(10),
    pfam_table_path text)
    returns void as
    $body$
    DECLARE
        table_group varchar(50);
        table_pfam varchar(50);
        table_pfam_group varchar(50);
        row_count integer;
    begin
    RAISE NOTICE 'Formating family table';
    table_group := phyt_table || '_gene_fam_group';
    table_pfam := phyt_table || '_pfam';
    table_pfam_group := table_pfam || '_group';
    EXECUTE format('drop table if exists %I', phyt_table);
    EXECUTE format('
        create table %I (phyt_id varchar(40), loc varchar(100), trn varchar(40), prot_name varchar(40),
        pfam varchar(180), panther varchar(50), kog varchar(160), kegg_ec varchar(80), ko varchar(60),
        go_id varchar(400), at_hit varchar(30), at_symb varchar(80), at_des varchar(300))', phyt_table);
    EXECUTE format('COPY %I from $$' || table_path || '$$', phyt_table);
    /* method denotes the how family is assigned */
    EXECUTE format('alter table %I add at_gene_hit varchar(30), add gene_fam_2 varchar(100), add method int',
        phyt_table);
    EXECUTE format('update %I set at_gene_hit = (regexp_split_to_array(at_hit,$$' || '\.' || '$$' || '))[1]', phyt_table);
    EXECUTE format('update %I set at_gene_hit=trim(at_gene_hit)', phyt_table);
    EXECUTE format('update %I set phyt_id=trim(phyt_id)', phyt_table);
    EXECUTE format('update %I set loc=trim(loc)', phyt_table);
    EXECUTE format('update %I set trn=trim(trn)', phyt_table);
    EXECUTE format('update %I set prot_name=trim(prot_name)', phyt_table);
    EXECUTE format('update %I set gene_fam_2=trim(gene_fam_2)', phyt_table);
    /* add annotation from tair 10 as this table is imported from phytozome */
    EXECUTE format('update %I as a set at_des=b.anot from atha_tair10_gene_anot as b where a.loc=b.loc  ', phyt_table);
    EXECUTE format('update %I as a set at_des=''NULL'' where at_des='''' ', phyt_table);

    if species ~~ '%atha%' then
        /* this line specific to Ath */
        RAISE NOTICE 'For Arabidopsis thaliana';
        /*EXECUTE format('select extra_curation()');*/
        EXECUTE format('update %I as a set gene_fam_2=b.gene_fam_2 from atha_gene_fam_1 as b where upper(a.loc)=upper(b.loc)',
            phyt_table);

    else
        RAISE NOTICE 'For other species %s', phyt_table;
        EXECUTE format('update %I as a set gene_fam_2=b.gene_fam_2, method=''1'' from atha_gene_fam_1 as b where
            upper(a.at_gene_hit)=upper(b.loc)', phyt_table);
    end if;
    EXECUTE format('drop table if exists %I', table_pfam);
    EXECUTE format('
        create table %I (dom varchar(200), acc varchar(20), query varchar(50), acc2 varchar(20), ev double precision,
            scr double precision, bias double precision, ev2 double precision, scr2 double precision,
            bias2 double precision, exp double precision, reg int, clu int, ov int, env int, dom2 int, rep int, inc int,
            des varchar(300))', table_pfam);
    EXECUTE format('COPY %I from $$' || pfam_table_path || '$$' || ' DELIMITER ' || '$$' || ',' || '$$', table_pfam);
    EXECUTE format('update %I set query=trim(query)', table_pfam);
    EXECUTE format('update %I set dom=trim(dom)', table_pfam);
    EXECUTE format('delete from %I where ev > 1e-05', table_pfam);
    EXECUTE format('drop table if exists %I', table_pfam_group);
    EXECUTE format('create table %I as select query, array_agg(dom) from %I group by query', table_pfam_group, table_pfam);
    EXECUTE format('alter table %I add dom_ct int', table_pfam_group);
    EXECUTE format('update %I set dom_ct=array_length(array_agg, 1)', table_pfam_group);
    EXECUTE format('alter table %I  drop if exists pfam_dom_auto', phyt_table);
    EXECUTE format('alter table %I add pfam_dom_auto varchar(400)', phyt_table);
    EXECUTE format('update %I set query=(regexp_split_to_array(query, ''\.p''))[1] where query ~ ''\.p'' ', table_pfam_group);
    EXECUTE format('update %I as a set pfam_dom_auto=b.array_agg from %I as b where a.trn=b.query',
            phyt_table, table_pfam_group);
    EXECUTE format('update %I as a set pfam_dom_auto=b.array_agg from %I as b where a.prot_name=b.query and
            a.pfam_dom_auto is null', phyt_table, table_pfam_group);

        /* method 2: based on the pfam domain annotation */
    /*EXECUTE format('update %I as a set gene_fam_2=b.gene_fam_2, method=''2'' from atha_gene_fam_1 as b where
            a.pfam_dom_auto=b.pfam_dom_auto and lower(a.at_des) like lower(b.anot) and a.gene_fam_2 is null', phyt_table);*/


    /* pfam curation manually */
    /* method 4 ; assign family based pfam domain and annotation after initial classification based on ath; missed
       by method 2 */

    /* C3H Transcription Factor Family */ /*
    EXECUTE format('update %I set gene_fam_2=''C3H Transcription Factor Family'' where pfam_dom_auto like
            ''{zf-CCCH,RRM_1}'' and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''C3H Transcription Factor Family'' where pfam_dom_auto like
            ''{zf-CCCH_3,zf-CCCH_2}'' and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''C3H Transcription Factor Family'' where pfam_dom_auto like
            ''{zf-CCCH,Torus}'' and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''C3H Transcription Factor Family'' where pfam_dom_auto like
            ''{zf-CCCH,KH_1,Torus,zf-CCCH_2}'' and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''C3H Transcription Factor Family'' where pfam_dom_auto like
            ''{zf-CCCH,KH_1,zf-CCCH_2}'' and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''C3H Transcription Factor Family'' where pfam_dom_auto like
            ''{zf-CCCH,DEAD,Helicase_C}'' and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''C3H Transcription Factor Family'' where pfam_dom_auto like
            ''{zf-CCCH,Torus,zf-C3HC4,zf-RING_2,zf-RING_5}'' and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''C3H Transcription Factor Family'' where pfam_dom_auto like
            ''{CwfJ_C_1,CwfJ_C_2,zf-CCCH,zf-CCCH_3}'' and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''C3H Transcription Factor Family'' where pfam_dom_auto like
            ''{KH_1,zf-CCCH}'' and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''C3H Transcription Factor Family'' where pfam_dom_auto like
            ''{zf-CCCH_3}'' and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''C3H Transcription Factor Family'' where pfam_dom_auto like
            ''{zf-CCCH,NIF,Torus}'' and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''C3H Transcription Factor Family'' where pfam_dom_auto like
            ''{SAP,zf-CCCH}'' and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''C3H Transcription Factor Family'' where pfam_dom_auto like
            ''{WD40,Nup160,Torus,zf-CCCH_3}'' and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''C3H Transcription Factor Family'' where pfam_dom_auto like
            ''{RRM_1,zf-CCCH,RRM_7}'' and gene_fam_2 is null ', phyt_table); */

    /* Miscellaneous Membrane Protein Family */
    /*
    EXECUTE format('update %I set gene_fam_2=''Miscellaneous Membrane Protein Family'' where pfam_dom_auto
            similar to ''({Actin,FtsA}|{Actin,F-box-like,F-box})'' and gene_fam_2 is null ', phyt_table);
    */


        /* from here with new annotation */
    EXECUTE format('update %I set gene_fam_2=''14-3-3 family'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''14-3-3'')
            and lower(at_des) ~ lower(''14-3-3|regulatory|GF14|G-box|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Aldehyde Dehydrogenase Gene Family'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''Aldedh'')
            and lower(at_des) ~ lower(''dehydrogenase|expressed|pyrroline|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Alfin-like Transcription Factor Family'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''Alfin'')
            and lower(at_des) ~ lower(''alfin|PHD finger|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Antiporter Superfamily'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''Na_sulph_symp|TPT|Na_H_Exchanger|HCO3_cotransp|Na_Ca_ex'')
            and lower(at_des) ~ lower(''oxoglutarate|citrate|phosphate|ATCHX|CHX28|DUF250|hydrogen exchanger|boron transporter|calcium exchanger|monovalent cation|mitochondrial carrier protein|cation|antiporter|exchanger|dicarboxylate transport|nucleotide transporter|ATP carrier|Mitochondrial substrate carrier|sugar transporter|transposable|HCO3- transporter|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Aquaporins'',method=''4'' where
                lower(pfam_dom_auto)=lower(''{MIP}'')
            and lower(at_des) ~ lower(''aquaporin|intrinsic|plasma|tonoplast|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''ARIADNE Gene Family'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''IBR'') and lower(pfam_dom_auto) !~ lower(''fibrillin|Fibrillarin|C3HC4'')
            and lower(at_des) ~ lower(''ariadne|IBR|RING|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''ARR-B Transcription Factor Family'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''Response_reg'')
            and lower(at_des) ~ lower(''response regulator|responsive regulator|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''ARF Transcription Factor Family'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''Auxin_resp'')
            and lower(at_des) ~ lower(''auxin response factor|auxin-responsive|DNA binding|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''ARID Transcription Factor Family'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''ARID'')
            and lower(at_des) ~ lower(''AT-rich|ARID|high mobility group|HMG|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''AP2-EREBP Transcription Factor Family'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''AP2'') and lower(pfam_dom_auto) !~ lower(''PAP2'')
            and lower(at_des) ~ lower(''ethylene-responsive|AP2|dehydration-responsive|NULL|cytokinin|ethylene responsive|AINTEGUMENTA|erf|DRE binding factor|DREB|redox responsive|ethylene|DORNROSCHEN|C-repeat|early activation|dehydration|DRE'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''AT-hook Motif Nuclear Localized (AHL) Family'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''DUF296'')
            and lower(at_des) ~ lower(''AT hook|AT-hook|DNA binding protein|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''AtRKD Transcription Factor Family'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''RWP-RK'')
            and lower(at_des) ~ lower(''RWP-RK|pentatricopeptide|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''BBR/BPC Transcription Factor Family'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''GAGA_bind'')
            and lower(at_des) ~ lower(''GAGA-binding|pentacysteine|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''bHLH Transcription Factor Family'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''HLH|bHLH-MYC_N'')
            and lower(at_des) ~ lower(''helix-loop-helix|bHLH|BIM2|anthocyanin|BEE|CBF|MYC7E|phytochrome|joka8|NULL|BRI1|FER-like|root hair defective|BR enhanced|transcription|beta HLH|conserved peptide|NACL|BES1-interacting Myc|LJRHL1|RHD|threonine-protein kinase WNK|BIG PETAL|BANQUO|UDP-glucosyl transferase'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''bZIP Transcription Factor Family'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''bZIP'')
            and lower(at_des) ~ lower(''HBP|E2F-related|transcription factor|CPuORF1|bZIP|NULL|basic leucine-zipper|TGACG motif-binding|G-box binding factor|leucine zipper|VIRE2|ABRE binding|abscisic acid|transposable|ABA-responsive|HY5|OCS|TGA1A|TGA3'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''BZR Transcription Factor Family'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''BES1_N'') and lower(pfam_dom_auto) !~ lower(''Glyco_hydro'')
            and lower(at_des) ~ lower(''BES1|Brassinosteroid|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Calcium Dependent Protein Kinase'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''Pkinase|EF-hand'')
            and lower(at_des) ~ lower(''calcium-dependent protein kinase|calmodulin-domain protein kinase|calmodulin-like domain protein kinase|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''C2C2-CO-like Transcription Factor Family'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''zf-B_box|CCT'')
            and lower(at_des) ~ lower(''CCT|B-box|CONSTANS|salt tolerance|light-regulated zinc finger protein|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''C2C2-Dof Transcription Factor Family'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''zf-Dof'')
            and lower(at_des) ~ lower(''zinc finger|expressed|DNA binding|NULL|TARGET OF MONOPTEROS|cycling DOF|OBF-binding|OBF binding'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''C2C2-Gata Transcription Factor Family'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''CCT|GATA'') and lower(pfam_dom_auto) !~ lower(''tify'')
            and lower(at_des) ~ lower(''GATA|ZIM|expressed|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''C2C2-YABBY Transcription Factor Family'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''YABBY'')
            and lower(at_des) ~ lower(''YABBY|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Carbohydrate Esterase Gene Family'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''PAE|Esterase|Pectinesterase|Abhydrolase_3|LpxC'') and lower(pfam_dom_auto) !~ lower(''FBA_1'')
            and lower(at_des) ~ lower(''formylglutathione|pectin|carboxyesterase|Hydrolases|Oligopeptidase|pectinacetylesterase|carboxylesterase|inactive receptor kinase|acetylglucosamine|Myb-like|ATPCME|pectinesterase|gibberellin|serine esterase|VANGUARD|NULL|beta-Hydrolases|carboxyesterase|methylesterase|self-incompatibility protein|acetylglycosamine deacetylase|Pectin lyase|root hair specific'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''CAMTA Transcription Factor Family'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''CG-1'')
            and lower(at_des) ~ lower(''calmodulin-binding|ethylene induced calmodulin binding|calmodulin binding|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''CBL-interacting serione-threonine Protein Kinases'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''Pkinase'')
            and lower(at_des) ~ lower(''CAMK_KIN1|CBL|SOS3|CBL-interacting|threonine protein kinase'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''CCAAT-DR1 Transcription Factor Family'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''CBFD_NFYB_HMF'')
            and lower(at_des) ~ lower(''subunit B13|subunit B12|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''CCAAT-HAP2 Transcription Factor Family'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''CBFB_NFYA'')
            and lower(at_des) ~ lower(''subunit A5|subunit A7|subunit A9|subunit A6|subunit A3|subunit A8|subunit A4|subunit A2|subunit A10|subunit A1|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''CCAAT-HAP3 Transcription Factor Family'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''CBFD_NFYB_HMF'')
            and lower(at_des) ~ lower(''subunit B6|subunit B10|subunit B7|subunit B8|subunit B1|subunit B5|subunit B3|subunit B2|subunit B4|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''CCAAT-HAP5 Transcription Factor Family'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''CBFD_NFYB_HMF'')
            and lower(at_des) ~ lower(''subunit C9|subunit C4|subunit C1|subunit C12|subunit C7|subunit C5|subunit C6|subunit C10|subunit C8|subunit C13|subunit C3|subunit C2|subunit C11|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Chloroplast and Mitochondria gene Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''Chloroa_b-bind|60KD_IMP|Cyt_b-c1_8|Cytochrom_C1|DsbD|Mito_carr|Pyr_redox_2|Tim17|Cytochrom_B561|FA_hydroxylase|TIC20|SecY '')
            and lower(at_des) ~ lower(''chlorophyll A-B binding|inner membrane protein|cytochrome b-c1|cytochrome c1-1|cytochrome c-type|mitochondrial carrier protein|phosphate carrier protein|NADH-ubiquinone|mitochondrial import inner membrane|cytochrome b561|beta-hydroxylase|tic20|preprotein translocase subunit secY|NULL|chlorophyll|phosphate transporter 3|light harvesting complex|SECY homolog 1|chloroplasts|cytochrome c biogenesis|NAD+ transporter|NAD\(P\)H dehydrogenase|beta-hydroxylase 1|inner membrane|NAD\+ transporter|Cytochrome C1|inner mitochondrial membrane|beta-carotene'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''COBRA Gene Family'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''COBRA'')
            and lower(at_des) ~ lower(''COBRA|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Core Cell Cycle Gene Family'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''Cyclin|Pkinase|CDI|CKS|RB_A'')
            and lower(at_des) ~ lower(''Cyclin|KIP-related|WEE1 kinase|NULL|retinoblastoma|CDK-activating kinase|CDK|cell division control'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''CPP Transcription Factor Family'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''TCR'')
            and lower(at_des) ~ lower(''CXC domain|Tesmin|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Cytochrome P450'' where
            lower(pfam_dom_auto) ~ lower(''p450'')
            and lower(at_des) ~ lower(''cytochrome P450|transposon protein|NULL|ferulic acid|hydroperoxide|cinnamate|allene oxide|ent-kaurenoic|GA requiring|brassinosteroid|ytochrome p450'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Cytochrome b5'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''FAD_binding|Cyt-b5'')
            and lower(at_des) ~ lower(''cytochrome B5|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Cytoskeleton Gene Family'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''Actin|Profilin'') and lower(pfam_dom_auto) !~ lower(''Cactin|F_actin|F-actin|PAE'')
            and lower(at_des) ~ lower(''actin|profilin|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''E2F-DP Transcription Factor Family'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''E2F_TDP'')
            and lower(at_des) ~ lower(''E2F|transcription factor Dp|NULL|winged-helix'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''EIL Transcription Factor Family'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''EIN3'')
            and lower(at_des) ~ lower(''ethylene-insensitive|NULL|Ethylene insensitive'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Expansin Gene Family'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''Pollen_allerg_1|DPBB_1'')
            and lower(at_des) ~ lower(''expansin|Barwin-like|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''EXO70 exocyst subunit family'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''Exo70'')
            and lower(at_des) ~ lower(''Exo70|exocyst|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''F-Box Gene Family'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''F-box|FBA|FBD'') and lower(pfam_dom_auto) !~ lower(''Actin|Tub'')
            and lower(at_des) ~ lower(''F-box|VIER|SKP1|EIN|FBD|RNI|Leucine-rich|phloem|LysM|kelch|DUF295|ARABIDILLO|actin|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''FH2 protein (formin) Gene Family'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''FH2'')
            and lower(at_des) ~ lower(''formin|retrotransposon|Actin|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''FtsH Gene Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''Peptidase_M41|AAA'')
            and lower(at_des) ~ lower(''FtsH protease|FTSH'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''GeBP Transcription Factor Family'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''DUF573'')
            and lower(at_des) ~ lower(''DNA-binding|transcription|storekeeper|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Glycoside Hydrolase Gene Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''Glyco_hydro|Cellulase|Alpha-amylase|Melibiase_2|Glyco_hyd_65N_2|BNR_2|NAGLU|Raffinose_syn|Trehalase|Alpha-L-AF_C|VQ|X8'')
            and lower(at_des) ~ lower(''beta-glucosidase|BNR|fucosidases|beta-mannosidase|polygalacturonase|acetylglucosaminidase|mannosylglycoprotein|xylosidase|hydrolase|Mannanase|endoglucanase|amylase|glucan|glycogen|Chitinase|hexosaminidase|galactosidase|glycosyltransferase|Trehalase|mannosidase|expressed|arabinofuranosidase|oligosaccharide|glucanotransferase|heparanase|VQ|NULL|Pectin lyase|glucosidase|glycosidases|invertase|glucuronidase|Raffinose synthase|cellulase|fructofuranosidase|disproportionating enzyme|stachyose synthase|debranching enzyme|imbibition|starch branching enzyme|Melibiase|dextrinase'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Glycosyltransferase Gene Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''UDPGT|Glyco_trans|Branch|GNT-I|LpxB|Cellulose_synt|Glyco_tran_28|Galactosyl_T|DUF604|Gb3_synth|Phosphorylase|XG_FTase|Glucan_synthase|SAM_1|Mannosyl_trans|Cupin_8|Glycos_transf|Spc97_Spc98'')
            and lower(at_des) ~ lower(''glucosyl|fucosyltransferase|glycosyltransferase|xylosyltransferase|glucosaminyltransferase|glucosaminyltransferase|disaccharide synthase|glycosyl|cellulose|trehalose|mannosyltransferase|sucrose|digalactosyldiacylglycerol|starch synthase|acetylglucosamine|diacylglycerol|sialyltransferase|octulosonic|galactosyltransferase|hypro1|fringe-related|glycosyltransferase|glucan|tetratricopeptide|acetylglucosamine|exostosin|SAM|NULL|sugar transferase|galacturonosyltransferase|galactinol synthase|DUF604|Galactosyl transferase|glycogenin|KDO transferase|GAMETOPHYTE DEFECTIVE|callose synthase|trehalase 1|ZRT|Spc97|oxoglutarate'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''GRAS Transcription Factor Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''GRAS'')
            and lower(at_des) ~ lower(''DELLA|scarecrow|SHR|RGA|nodulation|GRAS|gibberellin|chitin|MONOCULM|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''GRF Transcription Factor Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''WRC'')
            and lower(at_des) ~ lower(''growth-regulating factor|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Glutathione S-transferase Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''GST_N'')
            and lower(at_des) ~ lower(''IN2-1|dehydroascorbate|glutathione|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''HSF Transcription Factor Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''HSF_DNA-bind'')
            and lower(at_des) ~ lower(''heat stress|HSF|heat shock|winged|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Histidine Kinase Gene Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''HATPase_c|GAF|CHASE|PHY'') and lower(pfam_dom_auto) !~ lower(''PTPlike_phytase'')
            and lower(at_des) ~ lower(''phytochrome|ethylene|histidine|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Histidine Phosphotransfer Proteins'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''Hpt'')
            and lower(at_des) ~ lower(''histidine|HPT|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Homeobox Transcription Factor Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''Homeobox|RRM_1'')
            and lower(at_des) ~ lower(''homeobox|homeodomain|DDT|START|NULL|FLOWERING WAGENINGEN|protodermal|KNOTTED'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Inorganic Solute Cotransporters'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''Sulfate_transp|MFS_MOT1|Na_Ala_symp|Nramp|Zip|K_trans|Na_H_Exchanger|Cation_efflux|TFIIB|Ammonium_transp|Ctr|TrkH|PHO4'') and lower(pfam_dom_auto)!~lower(''Lzipper'')
            and lower(at_des) ~ lower(''sulfate|metal|macrophage|major facilitator|inorganic|potassium|potasium|cation|transcription|ammonium|copper|Na+ transporter|NULL|K\+ uptake transporter|K\+ efflux antiporter|phosphate transporter 1|nitrate transporter 2|zinc transporter|phosphate transporter 2|molybdate transporter|iron regulated transporter|K\+ uptake permease|sulphate transporter 1|iron-regulated transporter|K\+ transporter|ZRT|slufate transporter 2|nitrate transporter2|TBP-binding protein'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Ion Channel Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''Ion_trans|ANF_receptor|Voltage_CLC'')
            and lower(at_des) ~ lower(''potassium|cyclic nucleotide-gated|glutamate|chloride|expressed|NULL|gated channel|K\+ transporter|K\+ channel|nucleotide-binding transporter|nucleotide-regulated ion channel|K\+ outward rectifier'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''IQD Protein Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''IQ|DUF4005'') and lower(pfam_dom_auto) !~ lower(''ubiq'')
            and lower(at_des) ~ lower(''IQ|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''JUMONJI Transcription Factor Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''JmjC|JmjN'')
            and lower(at_des) ~ lower(''jumonji|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Kinesins Gene Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''Kinesin|Microtub_bd'')
            and lower(at_des) ~ lower(''kinesin|motor|NULL|triphosphate hydrolases'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Lateral Organ Boundaries Gene Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''LOB'') and lower(pfam_dom_auto) !~ lower(''Globin|Nlob'')
            and lower(at_des) ~ lower(''LOB|lateral|ASYMMETRIC LEAVES|DUF260|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Leucine-rich repeat (LRR) family protein'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''LRR_8|LRR_4'') and lower(pfam_dom_auto) !~ lower(''Pkinase'')
            and lower(at_des) ~ lower(''Leucine-rich repeat|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Amino Acid/Auxin Permease AAAP Family '',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''Aa_trans'')
            and lower(at_des) ~ lower(''amino|proline|LYS|NULL'')
            and gene_fam_2 is null ', phyt_table);
    /*
    EXECUTE format('update %I set gene_fam_2=''MAP Kinase (MAPK) Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''Pkinase'')
            and lower(at_des) ~ lower(''CGMC_MAPKCMGC|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''MAP Kinase Kinase (MAPKK) Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''Pkinase'')
            and lower(at_des) ~ lower(''MAP2K|MAPKK based on amino acid sequence homology|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''MAP Kinase Kinase Kinase (MAPKKK) Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''Pkinase'')
            and lower(at_des) ~ lower(''MAP3K|with no lysine|ACT-like protein tyrosine kinase|PAS domain-containing protein|Integrin-linked protein kinase|VH1-interacting kinase|NPK1-related protein kinase|threonine protein kinase|mitogen-activated protein kinase kinase kinase|Slob_Wnk|NULL'') and gene_fam_2 is
            null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''MAP Kinase Kinase Kinase Kinase (MAPKKKK) Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''Pkinase'')
            and lower(at_des) ~ lower(''STE_PAK_Ste20_KHSh_GCKh_HPKh|STE_PAK_Ste20_STLK|STE_PAK_Ste20_MST_like|STE_PAK_Ste20_STLK|NULL'')
            and gene_fam_2 is null ', phyt_table);
    */
    EXECUTE format('update %I set gene_fam_2=''Mechanosensitive Ion Channel Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''MS_channel'')
            and lower(at_des) ~ lower(''Mechanosensitive|MSCS|NULL'')
            and gene_fam_2 is null ', phyt_table);
    /*
    EXECUTE format('update %I set gene_fam_2=''Monolignol Biosynthesis'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''Epimerase|AMP-binding|Methyltransf|ADH_N|Transferase|Lyase_aromatic'') and lower(pfam_dom_auto) !~ lower(''adh_short'')
            and lower(at_des) ~ lower(''cinnamoyl|AMP-dependent|methyltransferase|coumarate|synthetase|dehydrogenase|Rossmann|ammonia-lyase|NULL'')
            and gene_fam_2 is null ', phyt_table);
    */
    EXECUTE format('update %I set gene_fam_2=''Magnesium Transporter (MRS2) Gene Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''CorA'')
            and lower(at_des) ~ lower(''magnesium|CorA|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''MYB Transcription Factor Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''Myb_DNA-binding'') and lower(pfam_dom_auto)!~lower(''Linker_histone'')
            and lower(at_des) ~ lower(''myb|homeodomain|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Myosin'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''Myosin_head'')
            and lower(at_des) ~ lower(''Myosin|triphosphate|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''NAC Transcription Factor Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''NAM'')
            and lower(at_des) ~ lower(''NAC|ubiquitination|NAP57|NTM1|no apical meristem|TCV|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''NADPH P450 reductases'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''FAD_binding_1'')
            and lower(at_des) ~ lower(''reductase|Flavodoxin|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Nicotianamine Synthase Gene Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''NAS'') and lower(pfam_dom_auto) !~ lower(''RNase|deaminase'')
            and lower(at_des) ~ lower(''nicotianamine|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''NLP Transcription Factor Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''RWP-RK'')
            and lower(at_des) ~ lower(''NIN|RWP-RK|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Nodulin-like protein family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''EamA|Nodulin-like|VIT1|GDA1_CD39'')
            and lower(at_des) ~ lower(''nodulin|Major facilitator|EamA|iron transporter|Walls|nucleoside phosphatase|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Non-SMC Element family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''Nse4_C'')
            and lower(at_des) ~ lower(''Nse4|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Organic Solute Cotransporters'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''OPT|Sugar_tr|TPT|TLC|AA_permease|MFS_1|Mito_carr|Mem_trans|MFS|PTR2|PUNUT|Xan_ur_permease|HAD|UAA|FPN1|Nuc_sug_transp|SBF|Zip|Trp_Tyr_perm|SSF|DUF1992|Cation_efflux'')
            and lower(at_des) ~ lower(''oligopeptide|transporter|DUF250|GDP-mannose|translocator|ATP/ADP-transporter|permease|major facilitator|mitochondrial carrier protein|auxin efflux carrier|sucrose|peptide|POT|purine permease|nucleobase-ascorbate|acyltransferase|ferroportin1|UAA transporter|bile acid sodium symporter|metal cation transporter|cation|Cation efflux|tyrosine permease|urea active transporter|expressed|iron regulated|thylakoid| NULL'')
            and gene_fam_2 is null ',phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Class III peroxidase'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''peroxidase'')
            and lower(at_des) ~ lower(''peroxidase|root hair specific|retrotransposon|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''PHD Transcription Factor Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''PHD'')
            and lower(at_des) ~ lower(''PHD|metalloendopeptidases|DNA binding|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''PHD Gene Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''PHD'')
            and lower(at_des) ~ lower(''VIL2|VIN3|Fibronectin|NULL'')
            and gene_fam_2 is null ', phyt_table);
    /* Gamma-thionin */
    EXECUTE format('update %I set gene_fam_2=''Plant defensins superfamily'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''Gamma-thionin'')
            and lower(at_des) ~ lower(''cysteine-rich|defensin|Scorpion|Gamma|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Polysaccharide Lyase Gene Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''Rhamnogal_lyase|Pec_lyase'')
            and lower(at_des) ~ lower(''Pectate|Rhamnogalacturonate|root hair specific|Pectin|lyases|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Pollen Coat Proteome'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''Oleosin|Lipase_GDSL|Stress-antifung|Caleosin'')
            and lower(at_des) ~ lower(''glycine|extracellular lipase|Caleosin|DUF26|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''PP2C-type phosphatases'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''PP2C|MORN'')
            and lower(at_des) ~ lower(''ABI2|phosphatase|poltergeist|PP2C|GTP|pol-like|ABA1|HOPW1|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Primary Pumps (ATPases) Gene Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''E1-E2_ATPase|ATP-synt|V_ATPase_I|H_Ppase|PhoLip_ATPase_C|HMA|Cation_ATPase_C|vATP-synt_AC39|V-ATPase|OSCP|ATP_synt|ATP-synt|vATP-synt_E'')
            and lower(at_des) ~ lower(''plasma membrane ATPase|P-type|ATPase|zinc-transporting|copper-transporting ATPase|cation|ATP synthase|pyrophosphatase|phospholipid-transporting|copper-transporting ATPase|calcium-transporting ATPase|ATP synthase|expressed|phosphate|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Protein tyrosine phosphatase (PTP) family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''Myotub-related|LMWPc|Rhodanese|DSPc|Y_phosphatase|FH2|mRNA_cap_enzyme'')
            and lower(at_des) ~ lower(''inositol|actin binding|butyric|phosphatase|rhodanese|PTEN|dual specificity protein|PHS1|5-AMP-activated protein kinase|tyrosine phosphatase|retrotransposon|capping enzyme|SEX4|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''PRT Gene Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''Pribosyltran|UPRTase|GATase_7'')
            and lower(at_des) ~ lower(''transferase|kinase|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''RAD5/RAD16-like Gene Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''SNF2_N'')
            and lower(at_des) ~ lower(''SNF2|hydrolases|helicase|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''RCI2 gene Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''Pmp3'')
            and lower(at_des) ~ lower(''temperature|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Receptor kinase-like protein family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''Pkinase'') and lower(pfam_dom_auto) ~ lower(''WAK|LRR|Lectin_legB|GDPD|LysM|Usp|Stress-antifung|Malectin|RCC1_2|Thaumatin|B_lectin|Lectin_C|Pkinase_Tyr|B_lectin'') and lower(pfam_dom_auto) !~ lower(''U-box'')
            and lower(at_des) ~ lower(''receptor-like protein kinase|npr1-1|Leucine-rich repeat protein kinase|transmembrane kinase|wall associated kinase|root hair specific|CRINKLY4|PEP1|ERECTA|FLG22-induced receptor-like kinase|phosphodiesterase|protein kinase family protein|SHV3|receptor like protein|leucine-rich repeat transmembrane protein kinase|STRUBBELIG-receptor|chitin elicitor|wall-associated kinase|hercules receptor kinase|EF-TU receptor|Protein kinase protein|receptor-like kinase|Leucine-rich repeat receptor|BRI1|PR5-like receptor kinase|lectin receptor kinase|lectin protein kinase|NSP-interacting kinase|HAESA-like|protein kinases|Protein kinase superfamily protein|lectin-receptor kinase|receptor lectin kinase|receptor serine|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''SBP Transcription Factor Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''SBP'') and lower(pfam_dom_auto) !~ lower(''SBP_bac'')
            and lower(at_des) ~ lower(''squamosa|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Single Myb Histone (SMH) Gene Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''Myb_DNA-bind_6|Linker_histone'')
            and lower(at_des) ~ lower(''single myb histone|Homeodomain|telomere|DNA-binding|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''SNARE and Associated Proteins'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''Syntaxin|SNARE|Longin|V-SNARE_C|SNAP|Synaptobrevin|Sec1|Sec20|CDC48'') and lower(pfam_dom_auto) !~ lower(''SNARE_assoc|Sec15'')
            and lower(at_des) ~ lower(''syntaxin|ATPase|membrin|snare|vesicle|ethylmaleimide|synaptobrevin|NSF|Sec1|secretory|vacuolar|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Structural Maintenance of Chromosomes family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''SMC_N'') and lower(pfam_dom_auto) !~ lower(''ABC_membrane|ABC_tran'')
            and lower(at_des) ~ lower(''structural|hydrolases|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Subtilisin-like Serine Gene Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''Peptidase_S8|ComA'')
            and lower(at_des) ~ lower(''Subtilisin|subtilase|Aldolase|peptidase|protease|phosphosulfolactate|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Sulfurtransferasese / Rhodanese Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''Rhodanese|ThiF|Rotamase_3'')
            and lower(at_des) ~ lower(''sulfurtransferase|Rhodanese|dehydrogenase|reductase|phosphatase|calcium|thylakoid|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''TCP transcription factor family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''TCP'') and lower(pfam_dom_auto) !~ lower(''Cpn60_TCP1'')
            and lower(at_des) ~ lower(''TCP|TEOSINTE|PCF|plastid|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Tify Gene Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''tify'')
            and lower(at_des) ~ lower(''ZIM|jasmonate|TIFY|CCT|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Trihelix Transcription Factor Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''Myb_DNA-bind_4'')
            and lower(at_des) ~ lower(''interacting|homeodomain|GT-2|DNA binding|DUF1421|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''TRM (TON1 Recruiting Motif) Superfamily'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''DUF4378|DUF3741|VARLMGL'')
            and lower(at_des) ~ lower(''ALC|DUF3741|GTP|longifolia|Phosphatidylinositol|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''TUB Transcription Factor Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''Tub'') and lower(pfam_dom_auto) !~ lower(''Tubulin'')
            and lower(at_des) ~ lower(''tubby|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''U-box Gene Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''U-box|Ufd2P_core|Prp19'')
            and lower(at_des) ~ lower(''U-box|ARM|CYS|MOS4|senescence|Armadillo|carboxyl|HSC70|RING|NULL'')
            and gene_fam_2 is null ', phyt_table);
    /* signature domain: Whirly */
    EXECUTE format('update %I set gene_fam_2=''Whirly Transcription Factor Family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''Whirly'')
            and lower(at_des) ~ lower(''WHIRLY|ssDNA|transcriptional|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''WRKY Transcription Factor Superfamily'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''WRKY'')
            and lower(at_des) ~ lower(''WRKY|zinc|Disease|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''zinc finger-homeobox gene family'',method=''4'' where
            lower(pfam_dom_auto) ~ lower(''ZF-HD_dimer'')
            and lower(at_des) ~ lower(''homeobox|mini zinc|zinc|NULL'')
            and gene_fam_2 is null ', phyt_table);
    EXECUTE format('update %I set gene_fam_2=''Cytoplasmic Ribosomal Protein Gene Family'',method=''4'' where
                lower(pfam_dom_auto) ~ lower(''Ribosomal_S|Ribosomal_L|Ribosomal_60|S10_plectin|Ribosom_S'')
            and lower(at_des) ~ lower(''ribosomal|ubiquitin|Nucleic acid|R-protein|RNA binding|SH3|breast basic|pre-tRNA|senescence|NULL'')
            and gene_fam_2 is null ', phyt_table);

        /* method 3: based on the tfdb */
        /*EXECUTE format('update %I as a set gene_fam_2=b.gene_fam_2, method=''3'' from all_tf_list as b where
            a.trn=b.phyt_id and a.gene_fam_2 is null', phyt_table);*/

    EXECUTE format('drop table if exists temp');
    EXECUTE format('create table temp as select distinct loc from %I', phyt_table);
    EXECUTE format('alter table temp add gene_fam_2 varchar(100)');
    EXECUTE format('update temp as a set gene_fam_2=b.gene_fam_2 from %I as b where a.loc = b.loc', phyt_table);
    EXECUTE format('drop table if exists %I', table_group);
    EXECUTE format('create table %I as select gene_fam_2, array_agg(loc) from temp group by gene_fam_2', table_group);
    EXECUTE format('update %I set array_agg=array_unique(array_agg)', table_group);
    EXECUTE format('alter table %I add loc_len int, add trn_array varchar[], add trn_len int,
        add phyt_id_array varchar[], add phyt_id_len int', table_group);
    EXECUTE format('drop table if exists temp');
    EXECUTE format('create table temp as select distinct trn from %I', phyt_table);
    EXECUTE format('alter table temp add gene_fam_2 varchar(100)');
    EXECUTE format('update temp as a set gene_fam_2 = b.gene_fam_2 from %I as b where a.trn = b.trn', phyt_table);
    EXECUTE format('drop table if exists temp2');
    EXECUTE format('create table temp2 as select gene_fam_2, array_agg(trn) from temp group by gene_fam_2');
    EXECUTE format('update %I as a set trn_array = b.array_agg from temp2 as b where a.gene_fam_2=b.gene_fam_2',
        table_group);
    EXECUTE format('update %I set trn_array=array_unique(trn_array)', table_group);
    EXECUTE format('drop table if exists temp');
    EXECUTE format('create table temp as select distinct phyt_id from %I', phyt_table);
    EXECUTE format('alter table temp add gene_fam_2 varchar(100)');
    EXECUTE format('update temp as a set gene_fam_2 = b.gene_fam_2 from %I as b where a.phyt_id = b.phyt_id',
        phyt_table);
    EXECUTE format('drop table if exists temp2');
    EXECUTE format('create table temp2 as select gene_fam_2, array_agg(phyt_id) from temp group by gene_fam_2');
    EXECUTE format('update %I as a set phyt_id_array = b.array_agg from temp2 as b where a.gene_fam_2=b.gene_fam_2',
        table_group);
    EXECUTE format('update %I set phyt_id_array=array_unique(phyt_id_array)', table_group);
    EXECUTE format('drop table if exists temp2; drop table if exists temp;');
    EXECUTE format('delete from %I where gene_fam_2 is null', table_group);
    EXECUTE format('update %I set loc_len=array_length(array_agg, 1)', table_group);
    EXECUTE format('update %I set trn_len=array_length(trn_array, 1)', table_group);
    EXECUTE format('update %I set phyt_id_len=array_length(phyt_id_array, 1)', table_group);



    end
    $body$ language "plpgsql";



/* path set to variable */
\set phytdir $$'/Users/renesh/Renesh_Docs/Research/tamu/db/'$$
\set pfamdir $$'/Users/renesh/Renesh_Docs/Research/tamu/db/pfam_anot/'$$

/* %%%%%%%%%%%%%%%% */

select format_fam_table('atha_phyt12',
                        :phytdir || 'Athaliana_167_TAIR10.annotation_info.txt',
                        'atha',
                        :pfamdir || 'atha_pfam_tab.csv');
select format_fam_table('ahyp_phyt12',
                        :phytdir || 'Ahypochondriacus_459_v2.1.annotation_info.txt',
                        'ahyp',
                        :pfamdir || 'ahyp_pfam_tab.csv');
select format_fam_table('atri_phyt12',
                        :phytdir || 'Atrichopoda_291_v1.0.annotation_info.txt',
                        'atri',
                        :pfamdir || 'atri_pfam_tab.csv');
select format_fam_table('aocc_phyt12',
                        :phytdir || 'Aoccidentale_449_v0.9.annotation_info.txt',
                        'aocc',
                        :pfamdir || 'aocc_pfam_tab.csv');
/* no pfam data for acom */
select format_fam_table('acom_phyt12',
                        :phytdir || 'Acomosus_321_v3.annotation_info.txt',
                        'acom',
                        :pfamdir || 'acom_pfam_tab.csv');
select format_fam_table('acoe_phyt12',
                        :phytdir || 'Acoerulea_322_v3.1.annotation_info.txt',
                        'acoe',
                        :pfamdir || 'acoe_pfam_tab.csv');
select format_fam_table('ahal_phyt12',
                        :phytdir || 'Ahalleri_264_v1.1.annotation_info.txt',
                        'ahal',
                        :pfamdir || 'ahal_pfam_tab.csv');
select format_fam_table('alyr_phyt12',
                        :phytdir || 'Alyrata_384_v2.1.annotation_info.txt',
                        'alyr',
                        :pfamdir || 'alyr_pfam_tab.csv');

select format_fam_table('aoff_phyt12',
                        :phytdir || 'Aofficinalis_498_V1.1.annotation_info_13c.txt',
                        'aoff',
                        :pfamdir || 'aoff_pfam_tab.csv');
select format_fam_table('bstr_phyt12',
                        :phytdir || 'Bstricta_278_v1.2.annotation_info.txt',
                        'bstr',
                        :pfamdir || 'bstr_pfam_tab.csv');
select format_fam_table('bdis_phyt12',
                        :phytdir || 'Bdistachyon_314_v3.1.annotation_info_13c.txt',
                        'bdis',
                        :pfamdir || 'bdis_pfam_tab.csv');
select format_fam_table('bhyb_phyt12',
                        :phytdir || 'Bhybridum_463_v1.1.annotation_info_13c.txt',
                        'bhyb',
                        :pfamdir || 'bhyb_pfam_tab.csv');
select format_fam_table('bsta_phyt12',
                        :phytdir || 'Bstacei_316_v1.1.annotation_info.txt',
                        'bsta',
                        :pfamdir || 'bsta_pfam_tab.csv');
select format_fam_table('bsyl_phyt12',
                        :phytdir || 'Bsylvaticum_490_v1.1.annotation_info_13c.txt',
                        'bsyl',
                        :pfamdir || 'bsyl_pfam_tab.csv');
select format_fam_table('bole_phyt12',
                        :phytdir || 'Boleraceacapitata_446_v1.0.annotation_info.txt',
                        'bole',
                        :pfamdir || 'bole_pfam_tab.csv');

select format_fam_table('brap_phyt12',
                        :phytdir || 'BrapaFPsc_277_v1.3.annotation_info.txt',
                        'brap',
                        :pfamdir || 'brap_pfam_tab.csv');
select format_fam_table('cgra_phyt12',
                        :phytdir || 'Cgrandiflora_266_v1.1.annotation_info.txt',
                        'cgra',
                        :pfamdir || 'cgra_pfam_tab.csv');
select format_fam_table('crub_phyt12',
                        :phytdir || 'Crubella_183_v1.0.annotation_info.txt',
                        'crub',
                        :pfamdir || 'crub_pfam_tab.csv');
select format_fam_table('cpap_phyt12',
                        :phytdir || 'Cpapaya_113_ASGPBv0.4.annotation_info.txt',
                        'cpap',
                        :pfamdir || 'cpap_pfam_tab.csv');
select format_fam_table('cqui_phyt12',
                        :phytdir || 'Cquinoa_392_v1.0.annotation_info.txt',
                        'cqui',
                        :pfamdir || 'cqui_pfam_tab.csv');
select format_fam_table('crei_phyt12',
                        :phytdir || 'Creinhardtii_281_v5.5.annotation_info.txt',
                        'crei',
                        :pfamdir || 'crei_pfam_tab.csv');
select format_fam_table('czof_phyt12',
                        :phytdir || 'Czofingiensis_461_v5.2.3.2.annotation_info.txt',
                        'czof',
                        :pfamdir || 'czof_pfam_tab.csv');


select format_fam_table('cari_phyt12',
                        :phytdir || 'Carietinum_492_v1.0.annotation_info.txt',
                        'cari',
                        :pfamdir || 'cari_pfam_tab.csv');
select format_fam_table('ccle_phyt12',
                        :phytdir || 'Cclementina_182_v1.0.annotation_info.txt',
                        'ccle',
                        :pfamdir || 'ccle_pfam_tab.csv');
select format_fam_table('csin_phyt12',
                        :phytdir || 'Csinensis_154_v1.1.annotation_info.txt',
                        'csin',
                        :pfamdir || 'csin_pfam_tab.csv');
select format_fam_table('csub_phyt12',
                        :phytdir || 'CsubellipsoideaC169_227_v2.0.annotation_info.txt',
                        'csub',
                        :pfamdir || 'csub_pfam_tab.csv');

/* No pca id and trnscript information available */
select format_fam_table('csat_phyt12',
                        :phytdir || 'Csativus_122_annotation_info_edited.txt',
                        'csat',
                        :pfamdir || 'csat_pfam_tab.csv');

/* %%%%%%%%%%%%%%%% */
select format_fam_table('dcar_phyt12',
                        :phytdir || 'Dcarota_388_v2.0.annotation_info.txt',
                        'dcar',
                        :pfamdir || 'dcar_pfam_tab.csv');
select format_fam_table('dsal_phyt12',
                        :phytdir || 'Dsalina_325_v1.0.annotation_info.txt',
                        'dsal',
                        :pfamdir || 'dsal_pfam_tab.csv');


select format_fam_table('egra_phyt12',
                        :phytdir || 'Egrandis_297_v2.0.annotation_info.txt',
                        'egra',
                        :pfamdir || 'egra_pfam_tab.csv');
select format_fam_table('esal_phyt12',
                        :phytdir || 'Esalsugineum_173_v1.0.annotation_info.txt',
                        'esal',
                        :pfamdir || 'esal_pfam_tab.csv');
select format_fam_table('fves_phyt12',
                        :phytdir || 'Fvesca_226_v1.1.annotation_info.txt',
                        'fves',
                        :pfamdir || 'fves_pfam_tab.csv');
select format_fam_table('gmax_phyt12',
                        :phytdir || 'Gmax_275_Wm82.a2.v1.annotation_info.txt',
                        'gmax',
                        :pfamdir || 'gmax_pfam_tab.csv');
select format_fam_table('ghir_phyt12',
                        :phytdir || 'Ghirsutum_458_v1.1.annotation_info.txt',
                        'ghir',
                        :pfamdir || 'ghir_pfam_tab.csv');
select format_fam_table('grai_phyt12',
                        :phytdir || 'Graimondii_221_v2.1.annotation_info.txt',
                        'grai',
                        :pfamdir || 'grai_pfam_tab.csv');
select format_fam_table('hvul_phyt12',
                        :phytdir || 'Hvulgare_462_r1.annotation_info.txt',
                        'hvul',
                        :pfamdir || 'hvul_pfam_tab.csv');

select format_fam_table('kfed_phyt12',
                        :phytdir || 'Kfedtschenkoi_382_v1.1.annotation_info.txt',
                        'kfed',
                        :pfamdir || 'kfed_pfam_tab.csv');
select format_fam_table('klax_phyt12',
                        :phytdir || 'Klaxiflora_309_v1.1.annotation_info.txt',
                        'klax',
                        :pfamdir || 'klax_pfam_tab.csv');
select format_fam_table('lsat_phyt12',
                        :phytdir || 'Lsativa_467_v5.annotation_info.txt',
                        'lsat',
                        :pfamdir || 'lsat_pfam_tab.csv');
select format_fam_table('lusi_phyt12',
                        :phytdir || 'Lusitatissimum_200_v1.0.annotation_info.txt',
                        'lusi',
                        :pfamdir || 'lusi_pfam_tab.csv');
select format_fam_table('mdom_phyt12',
                        :phytdir || 'Mdomestica_196_v1.0.annotation_info.txt',
                        'mdom',
                        :pfamdir || 'mdom_pfam_tab.csv');
select format_fam_table('mesc_phyt12',
                        :phytdir || 'Mesculenta_305_v6.1.annotation_info.txt',
                        'mesc',
                        :pfamdir || 'mesc_pfam_tab.csv');

select format_fam_table('mpol_phyt12',
                        :phytdir || 'Mpolymorpha_320_v3.1.annotation_info.txt',
                        'mpol',
                        :pfamdir || 'mpol_pfam_tab.csv');
select format_fam_table('mtru_phyt12',
                        :phytdir || 'Mtruncatula_285_Mt4.0v1.annotation_info.txt',
                        'mtru',
                        :pfamdir || 'mtru_pfam_tab.csv');
select format_fam_table('mpus_phyt12',
                        :phytdir || 'MpusillaCCMP1545_228_v3.0.annotation_info.txt',
                        'mpus',
                        :pfamdir || 'mpus_pfam_tab.csv');
select format_fam_table('mspr_phyt12',
                        :phytdir || 'MpusillaRCC299_229_v3.0.annotation_info.txt',
                        'mspr',
                        :pfamdir || 'mspr_pfam_tab.csv');
select format_fam_table('mgut_phyt12',
                        :phytdir || 'Mguttatus_256_v2.0.annotation_info.txt',
                        'mgut',
                        :pfamdir || 'mgut_pfam_tab.csv');
select format_fam_table('msin_phyt12',
                        :phytdir || 'Msinensis_497_v7.1.annotation_13c.txt',
                        'msin',
                        :pfamdir || 'msin_pfam_tab.csv');
select format_fam_table('macu_phyt12',
                        :phytdir || 'Macuminata_304_v1.annotation_info.txt',
                        'macu',
                        :pfamdir || 'macu_pfam_tab.csv');

select format_fam_table('oeur_phyt12',
                        :phytdir || 'Oeuropaea_451_v1.0.annotation_info.txt',
                        'oeur',
                        :pfamdir || 'oeur_pfam_tab.csv');
select format_fam_table('otho_phyt12',
                        :phytdir || 'Othomaeum_386_v1.0.annotation_info.txt',
                        'otho',
                        :pfamdir || 'otho_pfam_tab.csv');
select format_fam_table('osat_phyt12',
                        :phytdir || 'Osativa_204_v7.0.annotation_info.txt',
                        'osat',
                        :pfamdir || 'osat_pfam_tab.csv');
select format_fam_table('oluc_phyt12',
                        :phytdir || 'Olucimarinus_231_v2.0.annotation_info.txt',
                        'oluc',
                        :pfamdir || 'oluc_pfam_tab.csv');
select format_fam_table('phal_phyt12',
                        :phytdir || 'Phallii_495_v3.1.annotation_13c.txt',
                        'phal',
                        :pfamdir || 'phal_pfam_tab.csv');
select format_fam_table('pvir_phyt12',
                        :phytdir || 'Pvirgatum_450_v4.1.annotation_info.txt',
                        'pvir',
                        :pfamdir || 'pvir_pfam_tab.csv');
select format_fam_table('pvul_phyt12',
                        :phytdir || 'Pvulgaris_442_v2.1.annotation_info.txt',
                        'pvul',
                        :pfamdir || 'pvul_pfam_tab.csv');

select format_fam_table('ppat_phyt12',
                        :phytdir || 'Ppatens_318_v3.3.annotation_info.txt',
                        'ppat',
                        :pfamdir || 'ppat_pfam_tab.csv');
select format_fam_table('pdel_phyt12',
                        :phytdir || 'PdeltoidesWV94_445_v2.1.annotation_info.txt',
                        'pdel',
                        :pfamdir || 'pdel_pfam_tab.csv');
select format_fam_table('ptri_phyt12',
                        :phytdir || 'Ptrichocarpa_444_v3.1.annotation_info.txt',
                        'ptri',
                        :pfamdir || 'ptri_pfam_tab.csv');
select format_fam_table('pumb_phyt12',
                        :phytdir || 'Pumbilicalis_456_v1.5.annotation_info.txt',
                        'pumb',
                        :pfamdir || 'pumb_pfam_tab.csv');
select format_fam_table('pper_phyt12',
                        :phytdir || 'Ppersica_298_v2.1.annotation_info.txt',
                        'pper',
                        :pfamdir || 'pper_pfam_tab.csv');
select format_fam_table('rcom_phyt12',
                        :phytdir || 'Rcommunis_119_v0.1.annotation_info.txt',
                        'rcom',
                        :pfamdir || 'rcom_pfam_tab.csv');
select format_fam_table('spur_phyt12',
                        :phytdir || 'Spurpurea_289_v1.0.annotation_info.txt',
                        'spur',
                        :pfamdir || 'spur_pfam_tab.csv');


select format_fam_table('smoe_phyt12',
                        :phytdir || 'Smoellendorffii_91_v1.0.annotation_info.txt',
                        'smoe',
                        :pfamdir || 'smoe_pfam_tab.csv');
select format_fam_table('sita_phyt12',
                        :phytdir || 'Sitalica_312_v2.2.annotation_info.txt',
                        'sita',
                        :pfamdir || 'sita_pfam_tab.csv');
select format_fam_table('svir_phyt12',
                        :phytdir || 'Sviridis_311_v1.1.annotation_info.txt',
                        'svir',
                        :pfamdir || 'svir_pfam_tab.csv');
select format_fam_table('slyc_phyt12',
                        :phytdir || 'Slycopersicum_annotation_info.txt',
                        'slyc',
                        :pfamdir || 'slyc_pfam_tab.csv');
select format_fam_table('stub_phyt12',
                        :phytdir || 'Stuberosum_448_v4.03.annotation_info.txt',
                        'stub',
                        :pfamdir || 'stub_pfam_tab.csv');
select format_fam_table('sbio_phyt12',
                        :phytdir || 'Sbicolor_454_v3.1.1.annotation_info_13c.txt',
                        'sbio',
                        :pfamdir || 'sbio_pfam_tab.csv');
select format_fam_table('sfal_phyt12',
                        :phytdir || 'Sfallax_310_v0.5.annotation_info.txt',
                        'sfal',
                        :pfamdir || 'sfal_pfam_tab.csv');


select format_fam_table('spol_phyt12',
                        :phytdir || 'Spolyrhiza_290_v2.annotation_info.txt',
                        'spol',
                        :pfamdir || 'spol_pfam_tab.csv');
select format_fam_table('tcac_phyt12',
                        :phytdir || 'Tcacao_233_v1.1.annotation_info.txt',
                        'tcac',
                        :pfamdir || 'tcac_pfam_tab.csv');
select format_fam_table('tpra_phyt12',
                        :phytdir || 'Tpratense_385_v2.annotation_info.txt',
                        'tpra',
                        :pfamdir || 'tpra_pfam_tab.csv');
select format_fam_table('taes_phyt12',
                        :phytdir || 'Taestivum_296_v2.2.annotation_info.txt',
                        'taes',
                        :pfamdir || 'taes_pfam_tab.csv');
select format_fam_table('vung_phyt12',
                        :phytdir || 'Vunguiculata_469_v1.1.annotation_info.txt',
                        'vung',
                        :pfamdir || 'vung_pfam_tab.csv');
select format_fam_table('vvin_phyt12',
                        :phytdir || 'Vvinifera_145_Genoscope.12X.annotation_info.txt',
                        'vvin',
                        :pfamdir || 'vvin_pfam_tab.csv');
select format_fam_table('vcar_phyt12',
                        :phytdir || 'Vcarteri_317_v2.1.annotation_info.txt',
                        'vcar',
                        :pfamdir || 'vcar_pfam_tab.csv');
select format_fam_table('zmay_phyt12',
                        :phytdir || 'Zmays_284_Ensembl-18_2010-01-MaizeSequence.annotation_info.txt',
                        'zmay',
                        :pfamdir || 'zmay_pfam_tab.csv');
select format_fam_table('zmar_phyt12',
                        :phytdir || 'Zmarina_324_v2.2.annotation_info_13c.txt',
                        'zmar',
                        :pfamdir || 'zmar_pfam_tab.csv');


/* %%%%%%%%%%%%%%%% */


















/*
########################
Correct typos
########################
*/

/* %%%%%%%%%%%%%%%%

do
$$
declare
    i record;
begin
    for i in select * from pg_catalog.pg_tables where schemaname like 'public' and tablename ~ 'gene_fam_group'
    loop
        raise notice 'Running: %', i.tablename;
        execute format('update %I set gene_fam_2=''Plant Defensins Superfamily'' where
            gene_fam_2=''Plant defensins superfamily'' ', i.tablename);
        execute format('update %I set gene_fam_2=''PP2C-type Phosphatases'' where
            gene_fam_2=''PP2C-type phosphatases'' ', i.tablename);
        execute format('update %I set gene_fam_2=''Structural Maintenance of Chromosomes family'' where
            gene_fam_2=''Structural Maintenance of Chromosomes Family'' ', i.tablename);
        execute format('update %I set gene_fam_2=''Protein tyrosine phosphatase (PTP) family'' where
            gene_fam_2=''Protein Tyrosine Phosphatase (PTP) Family'' ', i.tablename);
        execute format('update %I set gene_fam_2=''Receptor Kinase-like Protein Family'' where
            gene_fam_2=''Receptor kinase-like protein family'' ', i.tablename);
        execute format('update %I set gene_fam_2=''Class III Peroxidase'' where
            gene_fam_2=''Class III peroxidase'' ', i.tablename);
        execute format('update %I set gene_fam_2=''Zinc Finger-Homeobox Gene Family'' where
            gene_fam_2=''zinc finger-homeobox gene family'' ', i.tablename);
        execute format('update %I set gene_fam_2=''Non-SMC Element Family'' where
            gene_fam_2=''Non-SMC Element family'' ', i.tablename);
        execute format('update %I set gene_fam_2=''Chloroplast and Mitochondria Gene Family'' where
            gene_fam_2=''Chloroplast and Mitochondria gene Family'' ', i.tablename);
        execute format('update %I set gene_fam_2=''EXO70 Exocyst Subunit Family'' where
            gene_fam_2=''EXO70 exocyst subunit family'' ', i.tablename);
        execute format('update %I set gene_fam_2=''Glutamine Dumper (GDU) Family'' where
            gene_fam_2=''Glutamine dumper (GDU) family'' ', i.tablename);
        execute format('update %I set gene_fam_2=''Pirin-like proteins'' where
            gene_fam_2=''Pirin-like Proteins'' ', i.tablename);
        execute format('update %I set gene_fam_2=''14-3-3 Family'' where
            gene_fam_2=''14-3-3 family'' ', i.tablename);
        execute format('update %I set gene_fam_2=''NADPH P450 Reductases'' where
            gene_fam_2=''NADPH P450 reductases'' ', i.tablename);
        execute format('update %I set gene_fam_2=''CBL-interacting Serine-Threonine Protein Kinases'' where
            gene_fam_2=''CBL-interacting serione-threonine Protein Kinases'' ', i.tablename);
        execute format('update %I set gene_fam_2=''Core DNA Replication Machinery Family'' where
            gene_fam_2=''Core DNA replication machinery Family'' ', i.tablename);
        execute format('update %I set gene_fam_2=''TCP Transcription Factor Family'' where
            gene_fam_2=''TCP transcription factor family'' ', i.tablename);
        execute format('update %I set gene_fam_2=''Nodulin-like Protein Family'' where
            gene_fam_2=''Nodulin-like protein family'' ', i.tablename);
        execute format('update %I set gene_fam_2=''Leucine-rich repeat (LRR) Family Protein'' where
            gene_fam_2=''Leucine-rich repeat (LRR) family protein'' ', i.tablename);
        execute format('update %I set gene_fam_2=''FH2 Protein (formin) Gene Family'' where
            gene_fam_2=''FH2 protein (formin) Gene Family'' ', i.tablename);
        execute format('update %I set gene_fam_2=''FH2 Protein (formin) Gene Family'' where
            gene_fam_2=''FH2 protein (formin) Gene Family'' ', i.tablename);
    end loop;
end;
$$

%%%%%%%%%%%%%%%% */

