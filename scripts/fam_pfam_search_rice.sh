## job name
#BSUB -J pfam_search

## send stderr and stdout to the same file
#BSUB -o pfam_search.%J
## login shell to avoid copying env from login session

## also helps the module function work in batch jobs
#BSUB -L /bin/bash

## 30 minutes of walltime ([hh:]mm)
#BSUB -W 30:30

## numprocs
#BSUB -n 20
#BSUB -R "span[ptile=20]"
#BSUB -R "select[mem256gb]"
#BSUB -R "rusage[mem=10000]"

## job queue
##BSUB -q sn_short

##project id for allocation
#BSUB -P 082796598983

# source /home/ren_net/software/virtulaenv/venv/bin/activate
# source ~/.bashrc
module load Westmere/1
module load HMMER/3.1b2-foss-2016a
module load BLAST+/2.4.0-intel-2015B-Python-2.7.10

# Format an HMM database into a binary format for hmmscan
cd /scratch/user/ren_net/project/family_enrich/rice_dom
hmmpress rice_fam.hmm

# run hmmscan to find protein families
FILEP="/scratch/user/ren_net/project/seqdb/phytozome12/prot/"
hmmscan -o ahyp_pfam_rice.txt --tblout ahyp_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Ahypochondriacus_459_v2.1.protein.fa
hmmscan -o atri_pfam_rice.txt --tblout atri_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Atrichopoda_291_v1.0.protein.fa
hmmscan -o aocc_pfam_rice.txt --tblout aocc_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Aoccidentale_449_v0.9.protein.fa
hmmscan -o acom_pfam_rice.txt --tblout acom_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Acomosus_321_v3.protein.fa
hmmscan -o acoe_pfam_rice.txt --tblout acoe_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Acoerulea_322_v3.1.protein.fa
hmmscan -o ahal_pfam_rice.txt --tblout ahal_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Ahalleri_264_v1.1.protein.fa
hmmscan -o alyr_pfam_rice.txt --tblout alyr_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Alyrata_384_v2.1.protein.fa
hmmscan -o atha_pfam_rice.txt --tblout atha_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Athaliana_167_TAIR10.protein.fa
hmmscan -o aoff_pfam_rice.txt --tblout aoff_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Aofficinalis_498_V1.1.protein.fa
hmmscan -o bstr_pfam_rice.txt --tblout bstr_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Bstricta_278_v1.2.protein.fa
hmmscan -o bdis_pfam_rice.txt --tblout bdis_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Bdistachyon_314_v3.1.protein.fa
hmmscan -o bhyb_pfam_rice.txt --tblout bhyb_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Bhybridum_463_v1.1.protein.fa
hmmscan -o bsta_pfam_rice.txt --tblout bsta_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Bstacei_316_v1.1.protein.fa
hmmscan -o bsyl_pfam_rice.txt --tblout bsyl_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Bsylvaticum_490_v1.1.protein.fa
hmmscan -o bole_pfam_rice.txt --tblout bole_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Boleraceacapitata_446_v1.0.protein.fa
hmmscan -o brap_pfam_rice.txt --tblout brap_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/BrapaFPsc_277_v1.3.protein.fa
hmmscan -o cgra_pfam_rice.txt --tblout cgra_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Cgrandiflora_266_v1.1.protein.fa
hmmscan -o crub_pfam_rice.txt --tblout crub_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Crubella_183_v1.0.protein.fa
hmmscan -o cpap_pfam_rice.txt --tblout cpap_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Cpapaya_113_ASGPBv0.4.protein.fa
hmmscan -o cqui_pfam_rice.txt --tblout cqui_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Cquinoa_392_v1.0.protein.fa
hmmscan -o crei_pfam_rice.txt --tblout crei_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Creinhardtii_281_v5.5.protein.fa
hmmscan -o czof_pfam_rice.txt --tblout czof_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Czofingiensis_461_v5.2.3.2.protein.fa
hmmscan -o cari_pfam_rice.txt --tblout cari_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Carietinum_492_v1.0.protein.fa
hmmscan -o ccle_pfam_rice.txt --tblout ccle_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Cclementina_182_v1.0.protein.fa
hmmscan -o csin_pfam_rice.txt --tblout csin_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Csinensis_154_v1.1.protein.fa
hmmscan -o csub_pfam_rice.txt --tblout csub_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/CsubellipsoideaC_169_227_v2.0.protein.fa
hmmscan -o csat_pfam_rice.txt --tblout csat_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Csativus_122_v1.0.protein.fa
hmmscan -o dcar_pfam_rice.txt --tblout dcar_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Dcarota_388_v2.0.protein.fa
hmmscan -o dsal_pfam_rice.txt --tblout dsal_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Dsalina_325_v1.0.protein.fa
hmmscan -o egra_pfam_rice.txt --tblout egra_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Egrandis_297_v2.0.protein.fa
hmmscan -o esal_pfam_rice.txt --tblout esal_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Esalsugineum_173_v1.0.protein.fa
hmmscan -o fves_pfam_rice.txt --tblout fves_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Fvesca_226_v1.1.protein.fa
hmmscan -o gmax_pfam_rice.txt --tblout gmax_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Gmax_275_Wm82.a2.v1.protein.fa
hmmscan -o ghir_pfam_rice.txt --tblout ghir_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Ghirsutum_458_v1.1.protein.fa
hmmscan -o grai_pfam_rice.txt --tblout grai_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Graimondii_221_v2.1.protein.fa
hmmscan -o hvul_pfam_rice.txt --tblout hvul_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Hvulgare_462_r1.protein.fa
hmmscan -o kfed_pfam_rice.txt --tblout kfed_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Kfedtschenkoi_382_v1.1.protein.fa
hmmscan -o klax_pfam_rice.txt --tblout klax_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Klaxiflora_309_v1.1.protein.fa
hmmscan -o lsat_pfam_rice.txt --tblout lsat_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Lsativa_467_v5.protein.fa
hmmscan -o lusi_pfam_rice.txt --tblout lusi_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Lusitatissimum_200_v1.0.protein.fa
hmmscan -o mdom_pfam_rice.txt --tblout mdom_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Mdomestica_196_v1.0.protein.fa
hmmscan -o mesc_pfam_rice.txt --tblout mesc_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Mesculenta_305_v6.1.protein.fa
hmmscan -o mpol_pfam_rice.txt --tblout mpol_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Mpolymorpha_320_v3.1.protein.fa
hmmscan -o mtru_pfam_rice.txt --tblout mtru_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Mtruncatula_285_Mt4.0v1.protein.fa
hmmscan -o mpus_pfam_rice.txt --tblout mpus_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/MpusillaCCMP1545_228_v3.0.protein.fa
hmmscan -o mspr_pfam_rice.txt --tblout mspr_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/MspRCC299_229_v3.0.protein.fa
hmmscan -o mgut_pfam_rice.txt --tblout mgut_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Mguttatus_256_v2.0.protein.fa
hmmscan -o msin_pfam_rice.txt --tblout msin_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Msinensis_497_v7.1.protein.fa
hmmscan -o macu_pfam_rice.txt --tblout macu_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Macuminata_304_v1.protein.fa
hmmscan -o oeur_pfam_rice.txt --tblout oeur_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Oeuropaea_451_v1.0.protein.fa
hmmscan -o otho_pfam_rice.txt --tblout otho_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Othomaeum_386_v1.0.protein.fa
hmmscan -o osat_pfam_rice.txt --tblout osat_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Osativa_323_v7.0.protein.fa
hmmscan -o oluc_pfam_rice.txt --tblout oluc_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Olucimarinus_231_v2.0.protein.fa
hmmscan -o phal_pfam_rice.txt --tblout phal_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Phallii_495_v3.1.protein.fa
hmmscan -o pvir_pfam_rice.txt --tblout pvir_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Pvirgatum_450_v4.1.protein.fa
hmmscan -o pvul_pfam_rice.txt --tblout pvul_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Pvulgaris_442_v2.1.protein.fa
hmmscan -o ppat_pfam_rice.txt --tblout ppat_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Ppatens_318_v3.3.protein.fa
hmmscan -o pdel_pfam_rice.txt --tblout pdel_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/PdeltoidesWV94_445_v2.1.protein.fa
hmmscan -o ptri_pfam_rice.txt --tblout ptri_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Ptrichocarpa_444_v3.1.protein.fa
hmmscan -o pumb_pfam_rice.txt --tblout pumb_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Pumbilicalis_456_v1.5.protein.fa
hmmscan -o pper_pfam_rice.txt --tblout pper_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Ppersica_298_v2.1.protein.fa
hmmscan -o rcom_pfam_rice.txt --tblout rcom_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Rcommunis_119_v0.1.protein.fa
hmmscan -o spur_pfam_rice.txt --tblout spur_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Spurpurea_289_v1.0.protein.fa
hmmscan -o smoe_pfam_rice.txt --tblout smoe_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Smoellendorffii_91_v1.0.protein.fa
hmmscan -o sita_pfam_rice.txt --tblout sita_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Sitalica_312_v2.2.protein.fa
hmmscan -o svir_pfam_rice.txt --tblout svir_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Sviridis_500_v2.1.protein.fa
hmmscan -o slyc_pfam_rice.txt --tblout slyc_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Slycopersicum_390_ITAG2.4.protein.fa
hmmscan -o stub_pfam_rice.txt --tblout stub_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Stuberosum_448_v4.03.protein.fa
hmmscan -o sbio_pfam_rice.txt --tblout sbio_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Sbicolor_454_v3.1.1.protein.fa
hmmscan -o sfal_pfam_rice.txt --tblout sfal_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Sfallax_310_v0.5.protein.fa
hmmscan -o spol_pfam_rice.txt --tblout spol_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Spolyrhiza_290_v2.protein.fa
hmmscan -o tcac_pfam_rice.txt --tblout tcac_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Tcacao_233_v1.1.protein.fa
hmmscan -o tpra_pfam_rice.txt --tblout tpra_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Tpratense_385_v2.protein.fa
hmmscan -o taes_pfam_rice.txt --tblout taes_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Taestivum_296_v2.2.protein.fa
hmmscan -o vung_pfam_rice.txt --tblout vung_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Vunguiculata_469_v1.1.protein.fa
hmmscan -o vvin_pfam_rice.txt --tblout vvin_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Vvinifera_145_Genoscope.12X.protein.fa
hmmscan -o vcar_pfam_rice.txt --tblout vcar_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Vcarteri_317_v2.1.protein.fa
hmmscan -o zmay_pfam_rice.txt --tblout zmay_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Zmays_284_Ensembl-18_2010-01-MaizeSequence.protein.fa
hmmscan -o zmay443_pfam_rice.txt --tblout zmay443_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/ZmaysPH207_443_v1.1.protein.fa
hmmscan -o zmar_pfam_rice.txt --tblout zmar_pfam_rice_tab.txt --noali -E 0.5 --cpu 15 rice_fam.hmm $FILEP/Zmarina_324_v2.2.protein.fa



#END
