-- Gene family enrichment analysis

/*
module load PostgreSQL/9.6.1-GCCcore-6.3.0-Python-2.7.12-bare
pg_ctl -o "-p 9999" -D /scratch/user/ren_net/Test/postresql/test.db -l /scratch/user/ren_net/Test/postresql/test.log start
psql -p 9999 -f genfam_db_maker_32019.sql family_db_32019
psql -p 9999 family_db_32019
*/

drop function if exists array_unique(arr anyarray);
create or replace function public.array_unique(arr anyarray)
    returns anyarray as
    $body$
        select array( select distinct unnest($1) )
    $body$ language "sql";


drop function if exists genfam_db_maker(phyt_table varchar(30), table_path text, species varchar(10),
    pfam_table_path text);
create or replace function genfam_db_maker(phyt_table varchar(30), table_path text, species varchar(10),
    pfam_table_path text)
    returns void as
    $body$
    DECLARE
        table_group varchar(50);
        table_pfam varchar(50);
        table_pfam_group varchar(50);
        row_count integer;
    begin
		table_group := phyt_table || '_gene_fam_group';
        table_pfam := phyt_table || '_pfam';
        table_pfam_group := table_pfam || '_group';
        EXECUTE format('drop table if exists %I', phyt_table);
        EXECUTE format('create table %I (phyt_id varchar(40), loc varchar(100), trn varchar(40), prot_name varchar(40),
            pfam varchar(180), panther varchar(50), kog varchar(160), kegg_ec varchar(80), ko varchar(60),
            go_id varchar(400), at_hit varchar(30), at_symb varchar(80), at_des varchar(300))', phyt_table);
        EXECUTE format('COPY %I from $$' || table_path || '$$', phyt_table);

        EXECUTE format('alter table %I add at_gene_hit varchar(30), add gene_fam varchar(100), add method int',
            phyt_table);
        -- EXECUTE format('update %I set at_gene_hit = (regexp_split_to_array(at_hit,$$' || '\.' || '$$' || '))[1]', phyt_table);
        -- EXECUTE format('update %I set at_gene_hit=trim(at_gene_hit)', phyt_table);
        EXECUTE format('update %I set phyt_id=trim(phyt_id)', phyt_table);
        EXECUTE format('update %I set loc=trim(loc)', phyt_table);
        EXECUTE format('update %I set trn=trim(trn)', phyt_table);
        EXECUTE format('update %I set prot_name=trim(prot_name)', phyt_table);
        EXECUTE format('update %I set gene_fam=trim(gene_fam)', phyt_table);
        /* add annotation from tair 10 as this table is imported from phytozome */
        -- EXECUTE format('update %I as a set at_des=b.anot from atha_tair10_gene_anot as b where a.loc=b.loc', phyt_table);
        -- EXECUTE format('update %I as a set at_des=''NULL'' where at_des='''' ', phyt_table);
        EXECUTE format('drop table if exists %I', table_pfam);
        EXECUTE format('create table %I (dom varchar(200), acc varchar(20), query varchar(50), acc2 varchar(20), ev double precision,
            scr double precision, bias double precision, ev2 double precision, scr2 double precision,
            bias2 double precision, exp double precision, reg int, clu int, ov int, env int, dom2 int, rep int, inc int,
            des varchar(300))', table_pfam);
        EXECUTE format('COPY %I from $$' || pfam_table_path || '$$' || ' DELIMITER ' || '$$' || ',' || '$$', table_pfam);
        EXECUTE format('update %I set query=trim(query)', table_pfam);
        -- delete non-significant hits
        EXECUTE format('delete from %I where ev > 0.05', table_pfam);
        EXECUTE format('drop table if exists %I', table_pfam_group);
        EXECUTE format('create table %I as select query, array_agg(dom) from %I group by query', table_pfam_group, table_pfam);
        EXECUTE format('alter table %I add dom_ct int', table_pfam_group);
        EXECUTE format('update %I set dom_ct=array_length(array_agg, 1)', table_pfam_group);
        EXECUTE format('alter table %I  drop if exists pfam_dom_auto', phyt_table);
        EXECUTE format('alter table %I add pfam_dom_auto varchar(1200)', phyt_table);
        EXECUTE format('update %I set query=(regexp_split_to_array(query, ''\.p''))[1] where query ~ ''\.p'' ', table_pfam_group);
        EXECUTE format('update %I as a set pfam_dom_auto=b.array_agg from %I as b where a.trn=b.query',
            phyt_table, table_pfam_group);
        EXECUTE format('update %I as a set pfam_dom_auto=b.array_agg from %I as b where a.prot_name=b.query and
            a.pfam_dom_auto is null', phyt_table, table_pfam_group);

         -- add gene family based on domain rules
         -- Cytochrome_CBB3|B3_4|Gb3_synth|POB3_N|Rab3-GTPase|VirB3 removed for string matching
        --EXECUTE format('update %I set gene_fam=''ABI3VP1 Gene Family'' where lower(pfam_dom_auto)~lower(''B3'')
        --    and lower(pfam_dom_auto)!~lower(''AP2|Auxin_resp|Cytochrome_CBB3|B3_4|Gb3_synth|POB3_N|Rab3-GTPase_cat|CDC24_OB3|VirB3|FBA_3|azE_antitoxin|OB3|NB-ARC'')
        --    and gene_fam is null ', phyt_table);

        EXECUTE format('update %I set gene_fam=''14-3-3 gene family'' where lower(pfam_dom_auto)~lower(''\{14-3-3'')
            and  gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''2OG-Fe(II) oxygenase gene family'' where lower(pfam_dom_auto)~lower(''\{2OG-FeII_Oxy|{DIOX_N'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''A20/AN1 zinc finger gene family'' where lower(pfam_dom_auto)~lower(''{zf-AN1|{zf-A20'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Aconitase (ACO) gene family'' where lower(pfam_dom_auto)~lower(''{Aconitase'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''ACT gene family'' where lower(pfam_dom_auto)~lower(''{ACT,|{ACT}'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Actin gene family'' where lower(pfam_dom_auto)~lower(''{Actin'')
            and  gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Adaptin gene family'' where lower(pfam_dom_auto)~lower(''{Adaptin_N'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Adaptor complexes (AP) medium subunit gene family'' where lower(pfam_dom_auto)~lower(''{Adap_comp_sub'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Adenylate kinase (AMK) gene family'' where lower(pfam_dom_auto)~lower(''{ADK'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''ADF gene family'' where lower(pfam_dom_auto)~lower(''{Cofilin_ADF'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Afadin/alpha-actinin-binding gene family'' where lower(pfam_dom_auto)~lower(''{ADIP'')
             and gene_fam is null ', phyt_table);
        -- DUF2967 removed for string matching
        EXECUTE format('update %I set gene_fam=''AHL gene family'' where lower(pfam_dom_auto)~lower(''{DUF296'')
            and lower(pfam_dom_auto)!~lower(''DUF2967|DUF2968'') and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Alba gene family'' where lower(pfam_dom_auto)~lower(''{Alba'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Alcohol dehydrogenase gene family'' where lower(pfam_dom_auto)~lower(''{ADH_N'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Aldehyde dehydrogenase gene family'' where lower(pfam_dom_auto)~lower(''{Aldedh'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Aldehyde oxidase (AO) gene family'' where lower(pfam_dom_auto)~lower(''{Ald_Xan_dh_C2'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Aldo/Keto reductase (AKR) gene family'' where lower(pfam_dom_auto)~lower(''{Aldo_ket_red'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Alfin-like gene family'' where lower(pfam_dom_auto)~lower(''{Alfin'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Alpha-galactosidase (AGAL) gene family'' where lower(pfam_dom_auto)~lower(''{Melibiase_2'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Alternative oxidase (AOX) gene family'' where lower(pfam_dom_auto)~lower(''{AOX'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Aluminium activated malate transporter gene family'' where lower(pfam_dom_auto)~lower(''{ALMT'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Amino acid transporters (AAT) gene family'' where lower(pfam_dom_auto)~lower(''{Aa_trans|{AA_permease'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''aminoacyl-tRNA synthetase gene family'' where lower(pfam_dom_auto)~lower(''{tRNA-synt'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Aminophospholipid ATPase (ALA) gene family'' where lower(pfam_dom_auto)~lower(''{PhoLip_ATPase_C|{PhoLip_ATPase_N'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Aminotransferase Gene Family'' where lower(pfam_dom_auto)~lower(''{Aminotran'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Annexin gene family'' where lower(pfam_dom_auto)~lower(''{Annexin'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''AP2/EREBP gene family'' where lower(pfam_dom_auto)~lower(''{AP2'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''APS kinase gene family'' where lower(pfam_dom_auto)~lower(''{APS_kinase'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Apyrase gene family'' where lower(pfam_dom_auto)~lower(''{GDA1_CD39'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Aquaporins (AQP) gene family'' where lower(pfam_dom_auto)~lower(''{MIP'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''ARF gene family'' where lower(pfam_dom_auto)~lower(''{arf,|{arf}'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''ARF-GAP gene family'' where lower(pfam_dom_auto)~lower(''{ArfGap'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Argonaute gene family'' where lower(pfam_dom_auto)~lower(''{Piwi|{PAZ'') and
            lower(pfam_dom_auto)!~lower(''peroxidase|DEAD'') and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''ARIADNE (ARI) gene family'' where lower(pfam_dom_auto)~lower(''{IBR'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''ARID gene family'' where lower(pfam_dom_auto)~lower(''{ARID'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Armadillo (ARM) repeat gene family'' where lower(pfam_dom_auto)~lower(''{Arm|{HEAT|{HEAT_2'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''ARR-B gene family'' where lower(pfam_dom_auto)~lower(''{Response_reg'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Asparagine synthase (ASNS) gene family'' where lower(pfam_dom_auto)~lower(''{Asn_synthase'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Aspartyl protease (AP) gene family'' where lower(pfam_dom_auto)~lower(''{ASP,|{ASP}|ASP,'')
             and lower(pfam_dom_auto)!~lower(''grasp|casp'') and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Autophagy-related (Atg) gene family'' where lower(pfam_dom_auto)~lower(''Atg8'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Aux/IAA gene family'' where lower(pfam_dom_auto)~lower(''{AUX_IAA'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Auxin transport gene family'' where lower(pfam_dom_auto)~lower(''{Mem_trans'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''B3 DNA binding gene superfamily'' where lower(pfam_dom_auto)~lower(''{B3,|,B3,|{B3}'')
             and gene_fam is null', phyt_table);
        EXECUTE format('update %I set gene_fam=''Band-7 gene family'' where lower(pfam_dom_auto)~lower(''{Band_7'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''BAX inhibitor-1 gene family'' where lower(pfam_dom_auto)~lower(''{Bax1-I'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''BBR/BPC gene family'' where lower(pfam_dom_auto)~lower(''{GAGA_bind'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Bcl-2-associated athanogene (BAG) gene family'' where lower(pfam_dom_auto)~lower(''{BAG'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''BET gene family'' where lower(pfam_dom_auto)~lower(''{Bromodomain|{BET'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Bet v I gene family'' where lower(pfam_dom_auto)~lower(''{Bet_v_1'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Beta galactosidases (BGAL) gene family'' where lower(pfam_dom_auto)~lower(''{Glyco_hydro_35|{Glyco_hydro_42'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Beta-fructofuranosidase (BFLUCT) gene family'' where lower(pfam_dom_auto)~lower(''{Glyco_hydro_32N'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''bHLH gene family'' where lower(pfam_dom_auto)~lower(''HLH'')
            and lower(pfam_dom_auto)!~lower(''PPR|RWP-RK|Dev_Cell_Death'') and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''BON/CPN Gene Family'' where lower(pfam_dom_auto)~lower(''{Copine'')
              and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''BRCA2-like gene family'' where lower(pfam_dom_auto)~lower(''{BRCA2|{BRCA-2'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Brix domain gene family'' where lower(pfam_dom_auto)~lower(''{Brix'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''BSD gene family'' where lower(pfam_dom_auto)~lower(''{BSD'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''BURP domain gene family'' where lower(pfam_dom_auto)~lower(''{BURP'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam= ''bZIP gene family'' where lower(pfam_dom_auto)~lower(''{bZIP_1|{bZIP_2|{bZIP_Maf|{DOG1,bZIP'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''BZR gene family'' where lower(pfam_dom_auto)~lower(''{BES1_N'')
            and  gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''C2C2-YABBY gene family'' where lower(pfam_dom_auto)~lower(''{YABBY'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''C2H2 gene family'' where lower(pfam_dom_auto)~lower(''{zf-C2H2'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''CAF1 gene family'' where lower(pfam_dom_auto)~lower(''{CAF1'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Calcium-dependent lipid-binding (CaLB) gene family'' where lower(pfam_dom_auto)~lower(''{C2,|{C2}'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Calreticulin gene family'' where lower(pfam_dom_auto)~lower(''{Calreticulin'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''CAMTA gene family'' where lower(pfam_dom_auto)~lower(''{CG-1'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Carbohydrate esterase gene family'' where lower(pfam_dom_auto)~lower(''{Pectinesterase|{Esterase|{Abhydrolase_3|{LpxC|{PAE|{Coesterase'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Carbonic anhydrase gene family'' where lower(pfam_dom_auto)~lower(''{Pro_CA|{Carb_anhydrase'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Casein kinase II subunit beta gene family'' where lower(pfam_dom_auto)~lower(''{CK_II_beta'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Catalase gene family'' where lower(pfam_dom_auto)~lower(''{Catalase'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Cation–proton antiporters (CPA) gene family'' where lower(pfam_dom_auto)~lower(''Na_H_Exchanger'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''CCCH zinc finger gene family'' where lower(pfam_dom_auto)~lower(''{zf-CCCH'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''CCoAOMT Gene Family'' where lower(pfam_dom_auto)~lower(''{ethyltransf_3,|{Methyltransf_3,|{Methyltransf_3}'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''CCT motif gene family'' where lower(pfam_dom_auto)~lower(''{CCT'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Cellulose synthase gene family'' where lower(pfam_dom_auto)~lower(''{Cellulose_synt|{Glycos_transf_2|{Glyco_trans_2_3'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Ceramidase gene family'' where lower(pfam_dom_auto)~lower(''{Ceramidase'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Chalcone-flavanone isomerase Gene Family'' where lower(pfam_dom_auto)~lower(''{Chalcone'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Chalcone and stilbene synthase gene family'' where lower(pfam_dom_auto)~lower(''{Chal_sti_synt'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Chlorophyll a/b-binding (LHC) gene family'' where lower(pfam_dom_auto)~lower(''{Chloroa_b-bind'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Choline transporter gene family'' where lower(pfam_dom_auto)~lower(''{Choline_transpo'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''cis–prenyltransferases (CPT) gene family'' where lower(pfam_dom_auto)~lower(''{Prenyltransf'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Clp protease gene family'' where lower(pfam_dom_auto)~lower(''clp_n|ClpB_D2-small|{CLP_protease'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Coactivator p15 gene family'' where lower(pfam_dom_auto)~lower(''{PC4'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''COBRA-like (COBL) gene family'' where lower(pfam_dom_auto)~lower(''{COBRA'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''COMT gene family'' where lower(pfam_dom_auto)~lower(''{Methyltransf_2,|{Methyltransf_2}|{ethyltransf_2,|{ethyltransf_2}'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Copper amine oxidase (CUAO) gene family'' where lower(pfam_dom_auto)~lower(''{Cu_amine_oxid'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Copper chaperone (CCH) gene family'' where lower(pfam_dom_auto)~lower(''{HMA'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Core cell cycle gene family'' where lower(pfam_dom_auto)~lower(''{Cyclin|{CDI'')
            and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Core DNA replication machinery gene family'' where lower(pfam_dom_auto)~lower(''{DNA_pol_B|{DNA_pol_E_B|{DNA_primase_S|{CDC27|{DNA_pol_delta_4|{DUF1744|{PCNA|{RFC1|{DNA_pol3_delta2|{Rep_fac_C|{cdt1|{MCM|{ORC|{CDC45|{PTCB-BRCT|{Sld5|{Rep_fac-A_C|{RPA_C|{XPG_I|{Dna2|{Rep_fac-A_3|{RNase_HII|{AAA,BAH|{DNA_ligase_A_M'')
        --      and lower(pfam_dom_auto)!~lower(''Y_phosphatase'') and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Core histone gene family'' where lower(pfam_dom_auto)~lower(''{Histone'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Cornichon gene family'' where lower(pfam_dom_auto)~lower(''{Cornichon'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''CPP gene family'' where lower(pfam_dom_auto)~lower(''{TCR'')
            and  gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''CRAL_TRIO domain gene family'' where lower(pfam_dom_auto)~lower(''{CRAL_TRIO'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''CRS1/YhbY domain gene family'' where lower(pfam_dom_auto)~lower(''{CRS1_YhbY'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''C-terminal processing peptidase gene family'' where lower(pfam_dom_auto)~lower(''{Peptidase_S41'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''CTP synthase gene family'' where lower(pfam_dom_auto)~lower(''{CTP_synth_N'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Cullin gene family'' where lower(pfam_dom_auto)~lower(''{Cullin,|{Cullin}'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Cupin gene family'' where lower(pfam_dom_auto)~lower(''{Cupin'')
              and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Cycloartenol synthase (CAS) gene family'' where lower(pfam_dom_auto)~lower(''{SQHop_cyclase_C'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Cyclophilin gene family'' where lower(pfam_dom_auto)~lower(''{Pro_isomerase'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Cystathionine beta-synthase (CBS) gene family'' where lower(pfam_dom_auto)~lower(''{CBS'')
             and lower(pfam_dom_auto)!~lower(''Voltage_CLC'') and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Cysteine-rich receptor-like kinases (CRK) gene family'' where lower(pfam_dom_auto)~lower(''Stress-antifung'')
             and lower(pfam_dom_auto)~lower(''Pkinase|Pkinase_Tyr'') and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Cysteine-rich secretory proteins, Antigen 5, and Pathogenesis-related 1 protein (CAP) gene family'' where lower(pfam_dom_auto)~lower(''{CAP'')
             and lower(pfam_dom_auto)!~lower(''CAP_N|Caps_synth|CAP_C'') and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''CYSTM gene family'' where lower(pfam_dom_auto)~lower(''{CYSTM'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Cytochrome b-561 gene family'' where lower(pfam_dom_auto)~lower(''{Cytochrom_B561'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Cytochrome C oxidase (COX) gene family'' where lower(pfam_dom_auto)~lower(''{COX'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Cytochrome P450 gene family'' where lower(pfam_dom_auto)~lower(''p450'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Cytokinin oxidase (CKX) gene family'' where lower(pfam_dom_auto)~lower(''{Cytokin-bind'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''DCD gene family'' where lower(pfam_dom_auto)~lower(''{Dev_Cell_Death'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''DDE transposases gene family'' where lower(pfam_dom_auto)~lower(''{DDE_Tnp_4'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Deg protease gene family'' where lower(pfam_dom_auto)~lower(''{PDZ_3|{Trypsin_2'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Di19 gene Family'' where lower(pfam_dom_auto)~lower(''{zf-Di19|{Di19'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Diacylglycerol kinase (DGK) gene family'' where lower(pfam_dom_auto)~lower(''{DAGK_acc'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Dirigent gene family'' where lower(pfam_dom_auto)~lower(''{Dirigent'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''DNA methyltransferase gene family'' where lower(pfam_dom_auto)~lower(''{DNA_methylase'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''DnaJ domain gene family'' where lower(pfam_dom_auto)~lower(''DnaJ'')
             and lower(pfam_dom_auto)!~lower(''Dehydrin|yb_DNA-binding|myb_DNA-binding'') and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Dof gene family'' where lower(pfam_dom_auto)~lower(''{zf-Dof'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''DUF1618 gene family'' where lower(pfam_dom_auto)~lower(''{DUF1618'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''DVL gene family'' where lower(pfam_dom_auto)~lower(''{DVL'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Dynamin-related gene family'' where lower(pfam_dom_auto)~lower(''{Dynamin_N|{Dynamin_M'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''E2F-DP gene family'' where lower(pfam_dom_auto)~lower(''E2F_TDP'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''EIN3/EIL gene family'' where lower(pfam_dom_auto)~lower(''{EIN3'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''ELMO/CED-12 gene family'' where lower(pfam_dom_auto)~lower(''{ELMO_CED12'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''EMP70 gene family'' where lower(pfam_dom_auto)~lower(''{EMP70'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Enolase gene family'' where lower(pfam_dom_auto)~lower(''{Enolase_C|{Enolase_N'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''ENTH/ANTH/VHS gene superamily'' where lower(pfam_dom_auto)~lower(''{ENTH|{ANTH|{VHS|{GAT'')
              and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Equilibrative nucleoside transporter (ENT) gene family'' where lower(pfam_dom_auto)~lower(''{Nucleoside_tran'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Exo70 gene family'' where lower(pfam_dom_auto)~lower(''{Exo70'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Expansin gene family'' where lower(pfam_dom_auto)~lower(''{Pollen_allerg_1|{DPBB_1'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''FAD-binding Berberine gene family'' where lower(pfam_dom_auto)~lower(''{FAD_binding_4,BBE'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''FAR1 gene family'' where lower(pfam_dom_auto)~lower(''FAR1'')
            and lower(pfam_dom_auto)!~lower(''WRKY'') and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Fatty acid hydroxylase (FAH) gene family'' where lower(pfam_dom_auto)~lower(''{FA_hydroxylase'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''FGGY carbohydrate kinase gene family'' where lower(pfam_dom_auto)~lower(''{FGGY_N|{FGGY_C'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''FHA gene family'' where lower(pfam_dom_auto)~lower(''FHA,|FHA}'')
            and lower(pfam_dom_auto)!~lower(''PP2C|Pkinase'') and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''FKBP PPIases gene family'' where lower(pfam_dom_auto)~lower(''{FKBP_C|{FKBP_N'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''FLA gene family'' where lower(pfam_dom_auto)~lower(''{Fasciclin'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Flavin monooxygenase (FMO) gene family'' where lower(pfam_dom_auto)~lower(''{FMO-like'')
              and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Formin gene family'' where lower(pfam_dom_auto)~lower(''{FH2'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Fructose-1, 6-bisphosphate aldolase (FBA) gene family'' where lower(pfam_dom_auto)~lower(''{Glycolytic'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Fructose-1,6-bisphosphatase (FBPase) gene family'' where lower(pfam_dom_auto)~lower(''{FBPase'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''FtsH protease gene family'' where lower(pfam_dom_auto)~lower(''{Peptidase_M41'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GAPDH gene family'' where lower(pfam_dom_auto)~lower(''{Gp_dh_C|{Gp_dh_N'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GASA gene family'' where lower(pfam_dom_auto)~lower(''{GASA'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Gata gene family'' where lower(pfam_dom_auto)~lower(''{GATA'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GDP dissociation inhibitor (GDI) gene family'' where lower(pfam_dom_auto)~lower(''{GDI'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GDSL-like lipase (GELP) gene family'' where lower(pfam_dom_auto)~lower(''{Lipase_GDSL'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GeBP gene family'' where lower(pfam_dom_auto)~lower(''{DUF573'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GH3 gene family'' where lower(pfam_dom_auto)~lower(''{GH3'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GILT gene family'' where lower(pfam_dom_auto)~lower(''{GILT'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Glucan synthase-like (GSL) gene family'' where lower(pfam_dom_auto)~lower(''{Glucan_synthase|{FKS1_dom1'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Glucose-6-phosphate dehydrogenase (G6PDH) gene family'' where lower(pfam_dom_auto)~lower(''G6PD_C'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Glutamate dehydrogenase (GDH) gene family'' where lower(pfam_dom_auto)~lower(''{ELFV_dehydrog|{ELFV_dehydrog_N'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Glutamate receptor gene family'' where lower(pfam_dom_auto)~lower(''{ANF_receptor'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Glutamine synthetase (GS) gene family'' where lower(pfam_dom_auto)~lower(''{Gln-synt_C|{Gln-synt_N'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Glutaredoxins gene family'' where lower(pfam_dom_auto)~lower(''{Glutaredoxin'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Glutathione peroxidase (GPX) gene family'' where lower(pfam_dom_auto)~lower(''{GSHPx'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Glutathione S-transferase (GST) gene family'' where lower(pfam_dom_auto)~lower(''{GST'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Glycerol-3-phosphate dehydrogenase (GPDH) gene family'' where lower(pfam_dom_auto)~lower(''{NAD_Gly3P_dh_C|{NAD_Gly3P_dh_N'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Glycerophosphodiester phosphodiesterase (GDPD) gene family'' where lower(pfam_dom_auto)~lower(''{GDPD'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Glycolipid transfer protein (GLTP) gene family'' where lower(pfam_dom_auto)~lower(''{GLTP'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Glyoxalase-like gene family'' where lower(pfam_dom_auto)~lower(''{Glyoxalase|{Lactamase_B|{DJ-1_PfpI'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GNAT Gene Family'' where lower(pfam_dom_auto)~lower(''{Acetyltransf'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''gp25L/emp24/p24 gene family'' where lower(pfam_dom_auto)~lower(''{EMP24_GP25L'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GRAS gene family'' where lower(pfam_dom_auto)~lower(''{GRAS'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GRF gene family'' where lower(pfam_dom_auto)~lower(''{QLQ|{WRC'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GT10 gene family'' where lower(pfam_dom_auto)~lower(''{Glyco_transf_10'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GT13 gene family'' where lower(pfam_dom_auto)~lower(''{GNT-I'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GT14 gene family'' where lower(pfam_dom_auto)~lower(''{Branch'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GT16 gene family'' where lower(pfam_dom_auto)~lower(''{MGAT2'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GT17 gene family'' where lower(pfam_dom_auto)~lower(''{Glyco_transf_17'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GT19 gene family'' where lower(pfam_dom_auto)~lower(''{LpxB'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GT20 gene family'' where lower(pfam_dom_auto)~lower(''{Glyco_transf_20'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GT21 gene family'' where lower(pfam_dom_auto)~lower(''{Glycos_transf_2'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GT22 gene family'' where lower(pfam_dom_auto)~lower(''{Glyco_transf_22'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GT24 gene family'' where lower(pfam_dom_auto)~lower(''{UDP-g_GGTase'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GT28 gene family'' where lower(pfam_dom_auto)~lower(''{Glyco_transf_28'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GT29 gene family'' where lower(pfam_dom_auto)~lower(''{Glyco_transf_29'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GT30 gene family'' where lower(pfam_dom_auto)~lower(''{Glycos_transf_N'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GT31 gene family'' where lower(pfam_dom_auto)~lower(''{Galactosyl_T'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GT32 gene family'' where lower(pfam_dom_auto)~lower(''{Gly_transf_sug'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GT34 gene family'' where lower(pfam_dom_auto)~lower(''{Glyco_transf_34'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GT35 gene family'' where lower(pfam_dom_auto)~lower(''{Phosphorylase'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GT37 gene family'' where lower(pfam_dom_auto)~lower(''{XG_FTase'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GT4 gene family'' where lower(pfam_dom_auto)~lower(''{Glycos_transf_1'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GT43 gene family'' where lower(pfam_dom_auto)~lower(''{Glyco_transf_43'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GT47 gene family'' where lower(pfam_dom_auto)~lower(''{Exostosin'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GT5 gene family'' where lower(pfam_dom_auto)~lower(''{Glyco_transf_5'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GT50 gene family'' where lower(pfam_dom_auto)~lower(''{Mannosyl_trans'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GT57 gene family'' where lower(pfam_dom_auto)~lower(''{Alg6_Alg8'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GT57 gene family'' where lower(pfam_dom_auto)~lower(''{Alg6_Alg8'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GT58 gene family'' where lower(pfam_dom_auto)~lower(''{ALG3'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GT59 gene family'' where lower(pfam_dom_auto)~lower(''{DIE2_ALG10'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GT61 gene family'' where lower(pfam_dom_auto)~lower(''{DUF563'')
            and lower(pfam_dom_auto)!~lower(''{DUF5633'') and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GT64 gene family'' where lower(pfam_dom_auto)~lower(''{Glyco_transf_64'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GT66 gene family'' where lower(pfam_dom_auto)~lower(''{STT3'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GT75 gene family'' where lower(pfam_dom_auto)~lower(''{RGP'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GT76 gene family'' where lower(pfam_dom_auto)~lower(''{Mannosyl_trans2'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''GT8 gene family'' where lower(pfam_dom_auto)~lower(''{Glyco_transf_8'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Guanylylate cyclase (GC) gene family'' where lower(pfam_dom_auto)~lower(''{Guanylate_cyc_2'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Haloacid dehalogenase (HAD) gene superfamily'' where lower(pfam_dom_auto)~lower(''{Acid_phosphat_B|{Acid_PPase|{HAD_2'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''hAT transposon gene family'' where lower(pfam_dom_auto)~lower(''Dimer_Tnp_hAT'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Heat shock factor (HSF) gene family'' where lower(pfam_dom_auto)~lower(''{HSF_DNA-bind'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''HECT ubiquitin‐protein ligase (UPL) gene family '' where lower(pfam_dom_auto)~lower(''{HECT'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Helitrons transposons gene family'' where lower(pfam_dom_auto)~lower(''{pif1|{Helitron_like_N'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Hexokinase (HXK) gene family'' where lower(pfam_dom_auto)~lower(''Hexokinase_2'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''High mobility group (HMG) gene family'' where lower(pfam_dom_auto)~lower(''{HMG_box'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Histidine phosphotransfer proteins (HPT) gene family'' where lower(pfam_dom_auto)~lower(''{Hpt'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Histone chaperones gene family'' where lower(pfam_dom_auto)~lower(''{Nucleoplasmin|{NAP|{CAF1A|{CAF1C_H4-bd|{SPT6_acidic|{YqgF|{ASF1_hist_chap|{Hira|{SHNi-TPR|{SPT16|{Ssrecog'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Histone deacetylase (HDAC) gene family'' where lower(pfam_dom_auto)~lower(''{Hist_deacetyl'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Homeobox gene family'' where lower(pfam_dom_auto)~lower(''{Homeobox|{Homeodomain|{START|{KNOX2|{KNOX1|{POX|{HALZ|{HD-ZIP_N|{ZF-HD_dimer'')
            and  gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''HSP20 gene family'' where lower(pfam_dom_auto)~lower(''{HSP20'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''HSP70 gene family'' where lower(pfam_dom_auto)~lower(''{HSP70'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''HSP90 gene family'' where lower(pfam_dom_auto)~lower(''{HSP90'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''HVA22 gene family'' where lower(pfam_dom_auto)~lower(''{TB2_DP1_HVA22'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Inositol monophosphatase-like (IMPL) gene family'' where lower(pfam_dom_auto)~lower(''{Inositol_P'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''IQD gene family'' where lower(pfam_dom_auto)~lower(''{DUF4005|{IQ'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''IWS1 gene family'' where lower(pfam_dom_auto)~lower(''{ed26|{Med26'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Jacalin-like lectin (JRL) gene family'' where lower(pfam_dom_auto)~lower(''{Jacalin'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''JUMONJI gene family'' where lower(pfam_dom_auto)~lower(''{JmjC'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''K Homology (KH) gene family'' where lower(pfam_dom_auto)~lower(''KH_1'')
              and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Kinesins gene family'' where lower(pfam_dom_auto)~lower(''{Kinesin'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''KIP1 gene family'' where lower(pfam_dom_auto)~lower(''{KIP1'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Kunitz trypsin inhibitor gene family'' where lower(pfam_dom_auto)~lower(''{Kunitz_legume'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Late embryogenesis abundant (LEA) gene family'' where lower(pfam_dom_auto)~lower(''{{LEA_1|{LEA_2|{LEA_3|{LEA_4|{LEA_5|{LEA_6|{LEA_7|{SMP'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Lateral organ boundaries domain (LBD) gene family'' where lower(pfam_dom_auto)~lower(''{LOB'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Lectin-like receptor kinase gene family'' where lower(pfam_dom_auto)~lower(''{Lectin_legB|{B_lectin'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''LIM gene family'' where lower(pfam_dom_auto)~lower(''{LIM,|{LIM}'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Lipocalins gene family'' where lower(pfam_dom_auto)~lower(''{Lipocalin_2'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Lipoxygenase (LOX) gene family'' where lower(pfam_dom_auto)~lower(''{Lipoxygenase'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Lon protease gene family'' where lower(pfam_dom_auto)~lower(''LON_substr_bdg|Lon_C'')
             and lower(pfam_dom_auto)!~lower(''Aldedh'') and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Lonely guy (LOG) gene family'' where lower(pfam_dom_auto)~lower(''{Lysine_decarbox'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''LSM gene family'' where lower(pfam_dom_auto)~lower(''{LSM,|LSM,|{LSM}'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Lysosomal Pro-x carboxypeptidase gene family'' where lower(pfam_dom_auto)~lower(''{Peptidase_S28'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''MADS-box gene family'' where lower(pfam_dom_auto)~lower(''{k-box|{SRF-TF|{MEF2_binding-TF'')
            and lower(pfam_dom_auto)!~lower(''SpoU_methylase'') and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Malate dehydrogenases (MDH) gene family'' where lower(pfam_dom_auto)~lower(''{Ldh_1_C|{Ldh_1_N'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Malectin/malectin-like domain gene family'' where lower(pfam_dom_auto)~lower(''Malectin|Malectin_like'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''MBF1 gene family'' where lower(pfam_dom_auto)~lower(''{MBF1'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Mechanosensitive (MS) ion channel gene family'' where lower(pfam_dom_auto)~lower(''{MS_channel'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Metacaspase gene family'' where lower(pfam_dom_auto)~lower(''{Peptidase_C14'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Microtubule-associated protein (MAP65) gene family'' where lower(pfam_dom_auto)~lower(''{MAP65_ASE1'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Mitochondrial carrier (MCF) gene family'' where lower(pfam_dom_auto)~lower(''{Mito_carr'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''MLO Gene Family'' where lower(pfam_dom_auto)~lower(''{lo,|{lo}|{mlo,|{mlo'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Monogalactosyldiacylglycerol synthase (MGDG) gene family'' where lower(pfam_dom_auto)~lower(''MGDG_synth'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Monosaccharide transporter (MST) gene family'' where lower(pfam_dom_auto)~lower(''{Sugar_tr'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''MORC gene family'' where lower(pfam_dom_auto)~lower(''{Morc6_S5'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''MRS2 gene family'' where lower(pfam_dom_auto)~lower(''{CorA'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''mTERF gene family'' where lower(pfam_dom_auto)~lower(''{mTERF'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''MtN3/Saliva/SWEET gene family'' where lower(pfam_dom_auto)~lower(''{tN3_slv|{MtN3_slv'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Multicopper oxidase gene family'' where lower(pfam_dom_auto)~lower(''{Cu-oxidase'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Multidrug and toxic compound extrusion (MATE) gene family'' where lower(pfam_dom_auto)~lower(''{atE|{MatE'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''MutS gene family'' where lower(pfam_dom_auto)~lower(''{MutS_III|{MutS_V|{MutS_I|{MutS_II'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''MYB gene family'' where lower(pfam_dom_auto)~lower(''{Myb_DNA-binding'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''MYB-CC gene family'' where lower(pfam_dom_auto)~lower(''{Myb_CC_LHEQLE'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Myosins gene family'' where lower(pfam_dom_auto)~lower(''{Myosin_head|{yosin_head'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''NAC gene family'' where lower(pfam_dom_auto)~lower(''{NAM|{NAM-associated'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''NADPH quinone oxidoreductase (NQO) gene family'' where lower(pfam_dom_auto)~lower(''Flavodoxin_2'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''NADK gene family'' where lower(pfam_dom_auto)~lower(''{NAD_kinase'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''NADPH oxidase (NOX) gene family'' where lower(pfam_dom_auto)~lower(''Ferric_reduct|NADPH_Ox'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Nicotianamine synthase (NAS) gene family'' where lower(pfam_dom_auto)~lower(''{NAS,|{NAS}'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Nitrilase gene family'' where lower(pfam_dom_auto)~lower(''{CN_hydrolase'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Nodulin-like gene family'' where lower(pfam_dom_auto)~lower(''{EamA|{VIT1|{Nodulin-like'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''NPH3 gene family'' where lower(pfam_dom_auto)~lower(''{NPH3'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Nuclear transport factor 2 (NTF2) gene family'' where lower(pfam_dom_auto)~lower(''{NTF2'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Nucleobase ascorbate transporter (NAT) gene family'' where lower(pfam_dom_auto)~lower(''{Xan_ur_permease'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Nucleoporin autopeptidase gene family'' where lower(pfam_dom_auto)~lower(''{Nucleoporin2|{Nup96,Nucleoporin2'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Nudix hydrolase gene family'' where lower(pfam_dom_auto)~lower(''{NUDIX|{NUDIX_2|{Nudix_hydro|{DCP2,NUDIX|{DUF4743,NUDIX'')
              and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''OFP gene family'' where lower(pfam_dom_auto)~lower(''{Ovate'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''O-fucosyl transferase (OFT) gene family'' where lower(pfam_dom_auto)~lower(''{O-FucT'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Oligopeptide transporter (OPT) gene family'' where lower(pfam_dom_auto)~lower(''{OPT'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''ORMDL gene family'' where lower(pfam_dom_auto)~lower(''{ORMDL'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''OSCA gene family'' where lower(pfam_dom_auto)~lower(''{RSN1_7TM|{PHM7_cyt,RSN1_7TM'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Oxysterol-binding protein (OSBP) gene family'' where lower(pfam_dom_auto)~lower(''{Oxysterol_BP'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Oxysterol-binding protein (OSBP) gene family'' where lower(pfam_dom_auto)~lower(''{PH'')
             and lower(pfam_dom_auto)~lower(''Oxysterol_BP'') and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''PAP fibrillin gene family'' where lower(pfam_dom_auto)~lower(''PAP_fibrillin|{Pkinase,PAP_fibrillin'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Papain-like cysteine proteases (PLCP) gene family'' where lower(pfam_dom_auto)~lower(''{Peptidase_C1,|{Peptidase_C1}'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''P-ATPase gene family'' where lower(pfam_dom_auto)~lower(''{E1-E2_ATPase|{Hydrolase,|{Hydrolase}|{Cation_ATPase_N'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Pectin methylesterase inhibitor (PMEI) gene family'' where lower(pfam_dom_auto)~lower(''{PMEI'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''PEX11 gene family '' where lower(pfam_dom_auto)~lower(''{PEX11'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''PHD gene family'' where lower(pfam_dom_auto)~lower(''{PHD'')
             and  gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Phenylalanine ammonia lyase (PAL) gene family'' where lower(pfam_dom_auto)~lower(''{Lyase_aromatic'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Phosphatidylethanolamine binding proteins (PEBP) gene family'' where lower(pfam_dom_auto)~lower(''{PBP,|{PBP}'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Phosphofructokinase (PFK) gene family'' where lower(pfam_dom_auto)~lower(''{PFK,|{PFK}'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Phosphoglycerate kinase (PGK) gene family'' where lower(pfam_dom_auto)~lower(''{PGK'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Phospholipase D gene family'' where lower(pfam_dom_auto)~lower(''{PLDc|{PLD_C'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Phosphoprotein phosphatases (PPP) gene family'' where lower(pfam_dom_auto)~lower(''{Metallophos|{Pur_ac_phosph_N,Metallophos|{STPPase_N,Metallophos|{fn3_PAP,Metallophos'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Phosphoribosyl transferase (PRT) gene family'' where lower(pfam_dom_auto)~lower(''{GATase_7|{Pribosyltran|{UPRTase'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Phytocyanin gene family'' where lower(pfam_dom_auto)~lower(''{Cu_bind_like'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''PkfB gene family'' where lower(pfam_dom_auto)~lower(''{PfkB'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''PLAC8 gene family'' where lower(pfam_dom_auto)~lower(''{PLAC8'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Plant prolamin gene family'' where lower(pfam_dom_auto)~lower(''{Tryp_alpha_amyl'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''PLATZ gene family'' where lower(pfam_dom_auto)~lower(''{PLATZ'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Pollen Ole e I allergen and extensin (POE) gene family'' where lower(pfam_dom_auto)~lower(''{Pollen_Ole_e_I'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Polyprenyl synthetase gene family'' where lower(pfam_dom_auto)~lower(''{polyprenyl_synt'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Polysaccharide lyase gene family'' where lower(pfam_dom_auto)~lower(''{Pec_lyase|{Rhamnogal_lyase'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''PP2A B regulatory subunit gene family'' where lower(pfam_dom_auto)~lower(''{B56|{EF-hand_13'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''PP2C gene family'' where lower(pfam_dom_auto)~lower(''{PP2C'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Prenylated RAB acceptor (PRA1) gene family'' where lower(pfam_dom_auto)~lower(''{PRA1'')
             and lower(pfam_dom_auto)!~lower(''Exo70'') and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Prolyl oligopeptidases (POP) gene family'' where lower(pfam_dom_auto)~lower(''{Peptidase_S9'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Protease IV gene family'' where lower(pfam_dom_auto)~lower(''{Peptidase_S49'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Protein S-acyl transferase (PAT) gene family'' where lower(pfam_dom_auto)~lower(''{DHHC'')
              and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Pseudouridine synthase gene family'' where lower(pfam_dom_auto)~lower(''{PseudoU_synth'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Pumilio gene family'' where lower(pfam_dom_auto)~lower(''{PUF|{NABP,PUF'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Purine permease gene family'' where lower(pfam_dom_auto)~lower(''{PUNUT'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Rab gene family'' where lower(pfam_dom_auto)~lower(''{ras'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Raffinose and stachyose synthase gene family'' where lower(pfam_dom_auto)~lower(''{Raffinose_syn'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Rapid alkalinization factor (RALF) gene family'' where lower(pfam_dom_auto)~lower(''{RALF'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''RCI2 gene family'' where lower(pfam_dom_auto)~lower(''{Pmp3'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Regulator of chromosome condensation (RCC1)gene family'' where lower(pfam_dom_auto)~lower(''{RCC1'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Remorin gene family'' where lower(pfam_dom_auto)~lower(''{Remorin'')
             and lower(pfam_dom_auto)!~lower(''Exo70'') and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Reticulon gene family'' where lower(pfam_dom_auto)~lower(''{Reticulon'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Reverse transcriptase-like gene family'' where lower(pfam_dom_auto)~lower(''{RVT'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Rho guanine nucleotide exchange factors (GEF) gene family'' where lower(pfam_dom_auto)~lower(''{PRONE'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Rhomboid protease gene family'' where lower(pfam_dom_auto)~lower(''{Rhomboid'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''RNA-recognition motif (RRM) gene family'' where lower(pfam_dom_auto)~lower(''{RRM_1'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''RPM1-interacting protein 4 (RIN4) Gene Family'' where lower(pfam_dom_auto)~lower(''{AvrRpt-cleavage'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''RWP-RK gene family'' where lower(pfam_dom_auto)~lower(''{RWP-RK|{PB1|{bHLH-MYC_N|{GAF_2'')
            and lower(pfam_dom_auto)~lower(''RWP-RK'') and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''S1FA-like gene family'' where lower(pfam_dom_auto)~lower(''{S1FA'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''SAC domain gene family'' where lower(pfam_dom_auto)~lower(''{Syja_N'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''S-adenosyl-L-homocysteine hydrolase gene family'' where lower(pfam_dom_auto)~lower(''{AdoHcyase'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''SAUR gene family'' where lower(pfam_dom_auto)~lower(''{Auxin_inducible'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Serine acetyltransferase (SAT) gene family'' where lower(pfam_dom_auto)~lower(''{SATase_N'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Serine Beta-lactamase gene family'' where lower(pfam_dom_auto)~lower(''{Beta-lactamase'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Serine carboxypeptidase gene family'' where lower(pfam_dom_auto)~lower(''{Peptidase_S10'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Serine hydroxymethyltransferase (SHMT) gene family'' where lower(pfam_dom_auto)~lower(''{SHMT'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Serpin gene family'' where lower(pfam_dom_auto)~lower(''{Serpin'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''SET gene family'' where lower(pfam_dom_auto)~lower(''{SET'')
              and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Short-chain dehydrogenases/reductases (SDR) gene superfamily'' where
            lower(pfam_dom_auto)~lower(''\{3Beta_HSD|{adh_short|{Epimerase|{adh_short_C2'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Sigma70-like gene family'' where lower(pfam_dom_auto)~lower(''{Sigma70_r3|{Sigma70_r4|{Sigma70_r2'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Signal peptidase gene family'' where lower(pfam_dom_auto)~lower(''{Peptidase_S24'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Signal peptide peptidases-like (SPPL) gene family '' where lower(pfam_dom_auto)~lower(''{Peptidase_A22B'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Sir2 gene family'' where lower(pfam_dom_auto)~lower(''{SIR2'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''SKP1 gene family'' where lower(pfam_dom_auto)~lower(''{skp1'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''SMC gene family'' where lower(pfam_dom_auto)~lower(''{SMC_N'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Sodium/Calcium exchanger (NCX) gene family'' where lower(pfam_dom_auto)~lower(''{Na_Ca_ex|EF-hand_7'')
            and lower(pfam_dom_auto)~lower(''Na_Ca_ex'') and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Spermidine synthase (SPDS) gene family'' where lower(pfam_dom_auto)~lower(''{Spermine_synth'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Squamosa promoter binding protein-like (SPL) gene family'' where lower(pfam_dom_auto)~lower(''{SBP,|{SBP}'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''SRS gene family'' where lower(pfam_dom_auto)~lower(''{DUF702'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Strictosidine synthase-like (SSL) gene family'' where lower(pfam_dom_auto)~lower(''{Str_synth'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Subtilases (SBT) gene family'' where lower(pfam_dom_auto)~lower(''{Peptidase_S8'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Sucrose synthase (SUS) gene family'' where lower(pfam_dom_auto)~lower(''{Sucrose_synth'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Sulfotransferase (SOT) gene family'' where lower(pfam_dom_auto)~lower(''{Sulfotransfer_1|{Sulfotransfer_2|{Sulfotransfer_3'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Sulfurtransferasese/Rhodanese (ST) gene family '' where lower(pfam_dom_auto)~lower(''{Rhodanese|{UPF0176_N|{ThiF'')
            and lower(pfam_dom_auto)~lower(''Rhodanese'') and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Superoxide dismutase (SOD) gene family'' where lower(pfam_dom_auto)~lower(''{Sod_Fe_C|{Sod_Cu|{Sod_Fe_N'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Syntaxins gene family'' where lower(pfam_dom_auto)~lower(''{Syntaxin|{SNARE|{V-SNARE|{Synaptobrevin'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''TAZ gene family'' where lower(pfam_dom_auto)~lower(''{zf-TAZ|{BTB|{HAT_KAT11'')
             and lower(pfam_dom_auto)~lower(''zf-TAZ'') and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''TBC gene family'' where lower(pfam_dom_auto)~lower(''{RabGAP-TBC'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''TCP gene family'' where lower(pfam_dom_auto)~lower(''{TCP,|{TCP}'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Terpene synthase (TPS) gene family'' where lower(pfam_dom_auto)~lower(''{Terpene_synth_C|{Terpene_synth'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Tetraspanin (TET) gene family'' where lower(pfam_dom_auto)~lower(''{Tetraspanin'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Tetratricopeptide repeat (TPR) gene family'' where lower(pfam_dom_auto)~lower(''{TPR'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Thaumatin gene family'' where lower(pfam_dom_auto)~lower(''{Thaumatin'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Thioredoxin gene family'' where lower(pfam_dom_auto)~lower(''{Thioredoxin'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Threonine aldolase (TA) gene family'' where lower(pfam_dom_auto)~lower(''{Beta_elim_lyase'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Tify gene family'' where lower(pfam_dom_auto)~lower(''{tify'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Trehalose-6-phosphate phosphatase (TPP) gene family'' where lower(pfam_dom_auto)~lower(''{Trehalose_PPase'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Trichome birefringence like (TBL) gene family'' where lower(pfam_dom_auto)~lower(''{PC-Esterase'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''TUBBBY-like (TLP) gene family'' where lower(pfam_dom_auto)~lower(''{Tub,|{Tub}'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Tubulin gene family'' where lower(pfam_dom_auto)~lower(''{Tubulin'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Ubiquitin gene family'' where lower(pfam_dom_auto)~lower(''{ubiquitin'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Ubiquitin-conjugating enzyme (UBC) gene family'' where lower(pfam_dom_auto)~lower(''{UQ_con'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''UDP-glucuronate decarboxylase (UXS) gene family'' where lower(pfam_dom_auto)~lower(''{GDP_Man_Dehyd'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''UDP-glycosyltransferases (UGT) gene family'' where lower(pfam_dom_auto)~lower(''{UDPGT'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''ULT gene family'' where lower(pfam_dom_auto)~lower(''{SAND'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Universal stress protein (USP) gene family'' where lower(pfam_dom_auto)~lower(''{Usp,|{Usp}'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Ureide Permease (UPS) gene family'' where lower(pfam_dom_auto)~lower(''{Ureide_permease'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Villin gene family'' where lower(pfam_dom_auto)~lower(''{Gelsolin'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''VQ gene family'' where lower(pfam_dom_auto)~lower(''{VQ'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Whirly gene family'' where lower(pfam_dom_auto)~lower(''{Whirly'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Xyloglucan fucosyltransferase (XFT) gene family'' where lower(pfam_dom_auto)~lower(''{XG_Ftase'')
             and gene_fam is null ', phyt_table);




        EXECUTE format('update %I set gene_fam=''NB-LRR Gene Family'' where lower(pfam_dom_auto)~lower(''{NB-ARC'') and
              gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''NB-LRR Gene Family'' where lower(pfam_dom_auto)~lower(''{TIR'') and
             lower(pfam_dom_auto)~lower(''NB-ARC'') and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Leucine-rich repeat (LRR) gene family'' where lower(pfam_dom_auto)~lower(''{{LRR|{LRRNT|{LRRCT'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''kelch repeat gene family'' where lower(pfam_dom_auto)~lower(''{Kelch'')
             and lower(pfam_dom_auto)~lower(''F-box'') and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Ion channel gene family'' where lower(pfam_dom_auto)~lower(''Ion_trans|Voltage_CL|Ion_trans_2'')
            and lower(pfam_dom_auto)!~lower(''Ion_trans_N'') and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''F-box gene family'' where lower(pfam_dom_auto)~lower(''F-box|F-box-like|F-box-like_2|FBA|FBA_1|FBA_2|FBA_3|FBD'')
            and lower(pfam_dom_auto)!~lower(''zf-C2H2_2|Tub|HLH|JmjC|Actin|O-FucT|SNF2_N|PPR_2|Dimer_Tnp_hAT|Jacalin'') and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Ankyrin repeat (ANK) C3HC4-type RING finger (RF) gene family'' where lower(pfam_dom_auto)~lower(''ank_2'')
            and lower(pfam_dom_auto)~lower(''zf-C3HC4_3'') and lower(pfam_dom_auto)!~lower(''Pkinase'') and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Ankyrin repeat gene family'' where lower(pfam_dom_auto)~lower(''{Ank|{PGG,Ank'')
             and lower(pfam_dom_auto)!~lower(''zf-CCCH'') and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Acyl lipid metabolism gene family'' where lower(pfam_dom_auto)~lower(''{ACC_central|{Acyl-ACP_TE|{E1_dh|{Transket_pyr|\{2-oxoacid_dh|{FeThRed_A|{ACCA|{BPL_LplA_LipB|{CPSase_L_D2|{Acyl_transf_1|{Ketoacyl-synt|{ACP_syn_III|{FabA|{FA_desaturase_2|{PP-binding|{ACPS|{PAP2|{FA_desaturase|Acyltransferase|{CDP-OH_P_transf|{Choline_kinase|{CTP_transf_like|{PS_Dcarbxylase|{PSS|{LCAT|{ketoacyl-synt|{Acyl-CoA_dh_1|{Carboxyl_trans|{ECH_2|{LIAS_N|{MBOAT|{Oleosin|{Caleosin|{Palm_thioest|{GMC_oxred_N|{AMP-binding|{Hydrolase_4|{ACOX|{ECH_1|\{3HCDH_N|{Thiolase_N|{APH|{Phospholip_A2_1|{Patatin|{Abhydrolase_2|{Lipase_3|{Allene_ox_cyc|{Oxidored_FMN|{PI3Ka|{Amidase|{An_peroxidase|{VID27|{Exo_endo_phos|{PI3_PI4_kinase|{PIP5K|{PI-PLC-X|{Phosphoesterase|{DDHD|{PUL|{FAE1_CUT1_RppA|{Steroid_dh|{NAD_binding_4|{MBOAT_2|{Wax2_C|{Transferase|{ELO,|{ELO}|{DUF1298|{Abhydrolase_1|{LTP_2|{Hydrophob_seed|{Citrate_bind|{Citrate_synt|{MCD|{ACBP|{CMAS|{GDPD|{Abhydro_lipase'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Glycoside hydrolase (GH) gene family'' where lower(pfam_dom_auto)~lower(''{Glyco_hydro_1,|{Glyco_hydro_1}|{Glyco_hydro_2,|{Glyco_hydro_2}|{Glyco_hydro_3,|{Glyco_hydro_3}|{Cellulase|{Glyco_hydro_9|{Glyco_hydro_10,|{Glyco_hydro_10}|{Alpha-amylase|{Glyco_hydro_14|{Glyco_hydro_16|{Glyco_hydro_17|{Glyco_hydro_18|{Glyco_hydro_19|{Glyco_hydro_20|{Glyco_hydro_28|{Alpha_L_fucos|{Glyco_hydro_31|{Glyco_hydro_32N|{Glyco_hydro_35|{Trehalase|{Glyco_hydro_38|{Alpha-mann_mid|{Glyco_hydro_43|{Glyco_hydro_47|{Alpha-L-AF_C|{Glyco_hydro_63|{Glyco_hydro_77|{Glyco_hydro_79n|{Glyco_hydro_81|{Glyco_hydro_85|{NAGLU|{Glyco_hydro_100'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Inorganic solute cotransporter gene family'' where lower(pfam_dom_auto)~lower(''{Sulfate_transp|{MFS_MOT1|{Nramp|{Zip|{K_trans|{Cation_efflux|{Ammonium_transp|{TrkH|{PHO4'')
             and lower(pfam_dom_auto)!~lower(''Na_H_Exchanger|Sugar_tr'') and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Pentatricopeptide repeat gene family'' where lower(pfam_dom_auto)~lower(''{PPR_2|{PPR|{DYW_deaminase'')
             and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''ABC transporter gene family'' where lower(pfam_dom_auto)~lower(''ABC_tran'')
             and gene_fam is null', phyt_table);
        EXECUTE format('update %I set gene_fam=''Protein kinase gene superfamily'' where lower(pfam_dom_auto)~lower(''{ABC1|{AceK|{Act-Frag_cataly|{Alpha_kinase|{APH,|{APH}|{APH_6_hur|{Choline_kinase|{CotH|{DUF1679|{DUF2252|{DUF4135|{EcKinase|{Fam20C|{Fructosamin_kin|{FTA2|{Haspin_kinase|{HipA_C|{Ins_P5_2-kin|{IPK|{IucA_IucC|{Kdo|{Kinase-like|{Kinase-PolyVal|{KIND|{Pan3_PK|{PI3_PI4_kinase|{PIP49_C|{PIP5K|{Pkinase|{Pkinase_fungal|{Pkinase_Tyr|{Pox_ser-thr_kin|{RIO1|{Seadorna_VP7|{UL97|{WaaY|{YrbL-PhoP_reg|{YukC|{Ins134_P3_kin'')
            and lower(pfam_dom_auto)~lower(''Pkinase'')  and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''Ring finger domain gene family'' where lower(pfam_dom_auto)~lower(''zf-C3HC4|zf-RING_2|RINGv|zf-RING_4'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''RNA helicase gene family'' where lower(pfam_dom_auto)~lower(''DEAD|HA2|SNF2_N'') and
            lower(pfam_dom_auto)~lower(''Helicase_C'') and  gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''UCH/UBP gene family'' where lower(pfam_dom_auto)~lower(''UCH'')
            and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''WD40 gene family'' where lower(pfam_dom_auto)~lower(''{WD40'')
             and lower(pfam_dom_auto)!~lower(''F-box|zf-CCCH|Bromodomain'') and gene_fam is null ', phyt_table);
        EXECUTE format('update %I set gene_fam=''WRKY gene family'' where lower(pfam_dom_auto)~lower(''WRKY'')
            and gene_fam is null ', phyt_table);



        -- PAP2|HAP2|RAP2|RAB3GAP2_N removed for string matching
        --EXECUTE format('update %I set gene_fam=''AP2/EREBP gene family'' where lower(pfam_dom_auto)~lower(''{AP2'')
        --    and lower(pfam_dom_auto)!~lower(''PAP2|HAP2|RAP2|RAB3GAP2_N|AKAP2_C|Bap29|FAP206'')
        --    and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Auxin Response Factor Gene Family'' where lower(pfam_dom_auto)~lower(''Auxin_resp'')
        --    and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''bHLH Transcription Factor Family'' where lower(pfam_dom_auto)~lower(''bHLH-MYC_N|HLH'')
        --    and lower(pfam_dom_auto)!~lower(''PPR|RWP-RK|Dev_Cell_Death'') and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''C2C2-CO-like Transcription Factor Family'' where lower(pfam_dom_auto)~lower(''zf-B_box'')
        --    and lower(pfam_dom_auto)~lower(''zf-B_box|CCT'') and lower(pfam_dom_auto)!~lower(''tify|GATA|Response_reg|{ArfGap'')
        --    and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''C2C2-Dof Transcription Factor Family'' where lower(pfam_dom_auto)~lower(''zf-Dof'')
        --    and gene_fam is null ', phyt_table);
        -- GATase removed for string matching
        --EXECUTE format('update %I set gene_fam=''CCAAT Transcription Factor Family'' where lower(pfam_dom_auto)~lower(''CBFD_NFYB_HMF|CBFB_NFYA'')
        --    and gene_fam is null ', phyt_table);
        -- RtcR removed for string matching
        --EXECUTE format('update %I set gene_fam=''Homeobox gene family'' where lower(pfam_dom_auto)~lower(''{Homeobox{Homeodomain|{START|{KNOX2|{KNOX1|{POX|{HALZ|{HD-ZIP_N|{{ZF-HD_dimer'')
        --    and lower(pfam_dom_auto)~lower(''Homeobox'') and lower(pfam_dom_auto)!~lower(''DDT'') and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''JUMONJI gene family'' where lower(pfam_dom_auto)~lower(''JmjC'')
        --    and lower(pfam_dom_auto)~lower(''JmjC|JmjN'') and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''MADS Transcription Factor Family'' where lower(pfam_dom_auto)~lower(''SRF-TF'')
        --    and lower(pfam_dom_auto)!~lower(''SpoU_methylase'') and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''MED6 Gene Family'' where lower(pfam_dom_auto)~lower(''ed6'')
        --    and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''MED7 Gene Family'' where lower(pfam_dom_auto)~lower(''ed7'')
        --    and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''MYB gene family'' where lower(pfam_dom_auto)~lower(''yb_DNA-bind_6|Myb_DNA-bind_6|yb_DNA-binding|Myb_DNA-binding'')
        --    and lower(pfam_dom_auto)!~lower(''Response_reg|Linker_histone|SWIRM|SNF2_N|HSA|GST_N'') and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''PHD gene family'' where lower(pfam_dom_auto)~lower(''PHD'')
        --     and lower(pfam_dom_auto)!~lower(''Alfin|Homeobox|zf-HC5HC2H_2|AAA|BD|PHD_Oberon|VWA|zf-RING|JmjC|C1_2|SF1-HH|Evr1_Alr|HAT_KAT11|RAG2_PHD|zf-RanBP|RNIP|zf-Dof|ARID|BAH|zf-UBR|Abhydrolase|Nipped|PPR|PHD20L1'') and gene_fam is null ', phyt_table);
        -- Carb_bind removed for string matching
        --EXECUTE format('update %I set gene_fam=''RB Gene Family'' where lower(pfam_dom_auto)~lower(''RB_B'')
        --     and lower(pfam_dom_auto)!~lower(''Cyclin|PAH|Carb_bind'') and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Rcd1-like Gene Family'' where lower(pfam_dom_auto)~lower(''Rcd1'')
        --     and lower(pfam_dom_auto)!~lower(''U-box|Arm'') and gene_fam is null ', phyt_table);
        -- SET_assoc|C2-set|N-SET removed for string matching
        --EXECUTE format('update %I set gene_fam=''SQUAMOSA PROMOTER BINDING PROTEIN-LIKE (SPL) gene family'' where lower(pfam_dom_auto)~lower(''SBP'')
        --    and lower(pfam_dom_auto)!~lower(''Jiv90|SBP_bac|PsbP|SBP56|HSBP1'') and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''SET gene family'' where lower(pfam_dom_auto)~lower(''SET'')
        --     and lower(pfam_dom_auto)!~lower(''PHD|TPR|SET_assoc|C2-set|N-SET'') and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''SNF2 Gene Family'' where lower(pfam_dom_auto)~lower(''SNF2_N'')
        --     and lower(pfam_dom_auto)!~lower(''DEAD|NAC|PMD'') and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Sigma70-like Transcription Factor Family'' where lower(pfam_dom_auto)~lower(''Sigma70'')
        --    and lower(pfam_dom_auto)!~lower(''CS|zf-RING|CLTH|PDDEXK|yb_DNA-binding|myb_DNA-binding|IBN|eIF|CDC45|DDE|Skp1|CRAL_TRIO|C2,|DEK|HLH'') and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''SNF2 Gene Family'' where lower(pfam_dom_auto)~lower(''SNF2_N'')
        --     and lower(pfam_dom_auto)!~lower(''DEAD|NAC|PMD'') and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''SWI/SNF-BAF60b Gene Family'' where lower(pfam_dom_auto)~lower(''SWIB'')
        --     and lower(pfam_dom_auto)!~lower(''Terpene|zf-CCCH'') and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''SWI/SNF-SWI3 Gene Family'' where lower(pfam_dom_auto)~lower(''SWIRM'')
        --    and lower(pfam_dom_auto)!~lower(''yb_DNA-bind_6|myb_DNA-bind_6RINGv'') and gene_fam is null ', phyt_table);
        -- TCP1|Tcp11|TcpS removed for string matching
        -- CbtB removed for string matching
        --EXECUTE format('update %I set gene_fam=''TRAF Gene Family'' where lower(pfam_dom_auto)~lower(''BTB'')
        --     and lower(pfam_dom_auto)!~lower(''NPH3|TPR|CbtB|zf-TAZ'') and gene_fam is null ', phyt_table);
        -- icrotub|Tubulin|tube|yotub|Tub_N removed for string matching
        --EXECUTE format('update %I set gene_fam=''ZF-HD Transcription Factor Family'' where lower(pfam_dom_auto)~lower(''ZF-HD_dimer'')
        --    and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''ARIADNE Gene Family'' where lower(pfam_dom_auto)~lower(''IBR'')
        --    and lower(pfam_dom_auto)!~lower(''{Ssl1|{RWD|{RVT|{Eapp|{zf-C3HC4|{Helicase|{AAA|fibrillin|Fibrillarin|NIBRIN|Fibrinogen'')
        --    and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Antiporter Gene Family'' where lower(pfam_dom_auto)~lower(''{TPT|{Na_Ca_ex|{Na_H_Exchanger|{HCO3_cotransp|{CitMHS|{Na_sulph_symp|{ito_carr'')
        --    and gene_fam is null ', phyt_table);
        -- all removed for string matching
        --EXECUTE format('update %I set gene_fam=''Aquaporins (AQP) gene family'' where lower(pfam_dom_auto)~lower(''{MIP'')
        --    and lower(pfam_dom_auto)!~lower(''{SIP1|zip|Lipase|KIP1|TIP|Phospholip|ipZ|bZIP|lip|HIP|rip|PIP|IP-T|IPP|Yip|Sip|NIP|DIP|JIP|AIP|_IP|VIP|OIP|Fip|CIP|5ip|KIP|eip|1IP|iPG|Ipi|IPK'')
        --    and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Cytochrome b5 Gene Family'' where lower(pfam_dom_auto)~lower(''{Cyt-b5'')
        --    and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Cytoskeleton Gene Family'' where lower(pfam_dom_auto)~lower(''{Actin|{Profilin'')
        --    and lower(pfam_dom_auto)!~lower(''zf-C2H2'') and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Ion Channel Gene Family'' where lower(pfam_dom_auto)~lower(''{Ion_trans|Voltage_CLC'')
        --    and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Peroxidase Gene Family'' where lower(pfam_dom_auto)~lower(''peroxidase'')
        --    and lower(pfam_dom_auto)!~lower(''{An_peroxidase'') and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''NADPH P450 Reductases Gene Family'' where lower(pfam_dom_auto)~lower(''{FAD_binding_1'')
        --    and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Plant defensins gene family'' where lower(pfam_dom_auto)~lower(''Gamma-thionin|Defensin_like'')
        --    and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''PP2A Gene Family'' where lower(pfam_dom_auto)~lower(''etallophos'')
        --    and lower(pfam_dom_auto)!~lower(''etallophos_2|etallophosN'') and gene_fam is null ', phyt_table);
        -- globin|Nlob removed for string matching
        -- globin|Nlob removed for string matching
        --EXECUTE format('update %I set gene_fam=''Protein tyrosine phosphatase (PTP) gene family'' where lower(pfam_dom_auto)~lower(''Y_phosphatase'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Peptide Transporter (PTR) Gene Family'' where lower(pfam_dom_auto)~lower(''{PTR2'')
        --    and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Primary Pumps (ATPases) Gene Family'' where
        --    lower(pfam_dom_auto)~lower(''{E1-E2_ATPase|{V_ATPase|{H_Ppase|{PhoLip_ATPase|{Cation_ATPase|{ATP-synt_C|{ATP-synt_ab|{vATP-synt|{ATP-synt_Eps|{V-ATPase_C|{ATP-synt_A|{ATP-synt}|{ATP-synt,|{t_ATP_synt|{t_ATP-synt_B}|{ATP-synt_DE_N|{ATP-synt_B|{ATP-synt_G|{DUF1082|{OSCP|{ATP-synt_D|{V-ATPase_H_N|{ATP-synt_F'')
        --    and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Subtilisin-like Serine Proteases Gene Family'' where lower(pfam_dom_auto)~lower(''{Peptidase_S8'')
        --    and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Trehalose Biosynthesis Gene Family'' where lower(pfam_dom_auto)~lower(''{Glyco_transf_20|{Trehalase'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''U-box gene family'' where lower(pfam_dom_auto)~lower(''{U-box'')
        --     and lower(pfam_dom_auto)!~lower(''{Pkinase|{Prp|{zf-C3HC4|{WD40|{zf-Nse|{CLTH|{DWNN|{zf-CCHC|{Pro_isomerase'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Glutamine Amidotransferase (GATase) Gene Family'' where lower(pfam_dom_auto)~lower(''{GATase'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''CSD gene family'' where lower(pfam_dom_auto)~lower(''CSD'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''ZPR1 Gene Family'' where lower(pfam_dom_auto)~lower(''{zf-ZPR1'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''DUF632 Gene Family'' where lower(pfam_dom_auto)~lower(''{DUF632'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''DUF833 Gene Family'' where lower(pfam_dom_auto)~lower(''TANGO2'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Sin3 gene family'' where lower(pfam_dom_auto)~lower(''{Sin3a_C|{PAH,Sin3a_C'')
        --     and lower(pfam_dom_auto)!~lower(''DEAD|Pyr_redox_2|Pkinase|WRKY|ATH|PEARLI-4'') and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''SOH1 Gene Family'' where lower(pfam_dom_auto)~lower(''ed31|Med31'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''TFb2 Gene Family'' where lower(pfam_dom_auto)~lower(''Tfb2'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Trypsin Gene Family'' where lower(pfam_dom_auto)~lower(''{Trypsin_2'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''ATP-dependent Metalloprotease Gene Gamily'' where lower(pfam_dom_auto)~lower(''{Peptidase_M41'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Prolamin precursor Gene Family'' where lower(pfam_dom_auto)~lower(''{Gliadin'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''NB-LRR Gene Family'' where lower(pfam_dom_auto)~lower(''{NB-ARC|{TIR|{AAA_22|{AAA_24'') and
        --     lower(pfam_dom_auto)~lower(''NB-ARC'') and lower(pfam_dom_auto)!~lower(''WRKY'') and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Cysteine/Histidine-rich C1 domain Gene Family '' where lower(pfam_dom_auto)~lower(''{C1_2'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Pyruvate Kinase (PK) Gene Family'' where lower(pfam_dom_auto)~lower(''{pk,|{pk}'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''X8 Gene Family'' where lower(pfam_dom_auto)~lower(''{x8'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''IPP Transferase Gene Family'' where lower(pfam_dom_auto)~lower(''IPPT'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Exostosin Gene Family'' where lower(pfam_dom_auto)~lower(''{Exostosin'')
        --    and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''NLI interacting factor-like phosphatase Gene Family'' where lower(pfam_dom_auto)~lower(''{NIF'')
        --     and lower(pfam_dom_auto)!~lower(''NifU|NiFe|NifH|NIFK'') and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''CTP Gene Family'' where lower(pfam_dom_auto)~lower(''{Neprosin'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Cysteine-rich Repeat Secretory Protein Gene Family'' where lower(pfam_dom_auto)~lower(''{Stress-antifung'')
       --     and lower(pfam_dom_auto)!~lower(''Pkinase'') and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Dehydrin Gene Family'' where lower(pfam_dom_auto)~lower(''{Dehydrin'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''DJ-1 Gene Family'' where lower(pfam_dom_auto)~lower(''{DJ-1_PfpI'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Dehydrodolichyl Diphosphate Synthase (DPS) Gene Family'' where lower(pfam_dom_auto)~lower(''Prenyltransf'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''ECAGL Gene Family'' where lower(pfam_dom_auto)~lower(''{Prolamin_like'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Peptidase T1 Gene family'' where lower(pfam_dom_auto)~lower(''{Proteasome'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Methyladenine Glycosylase (MAG) Gene Family'' where lower(pfam_dom_auto)~lower(''{Adenine_glyco'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Phosphoglycerate Mutase (PGM) Gene Family'' where lower(pfam_dom_auto)~lower(''{His_Phos_1'')
        --     and lower(pfam_dom_auto)!~lower(''PPR_2|6PF2K'') and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Glutamate Decarboxylase (GAD) Gene Family'' where lower(pfam_dom_auto)~lower(''{Pyridoxal_deC'')
        --     and gene_fam is null ', phyt_table);
       --EXECUTE format('update %I set gene_fam=''NAD dependent epimerase/dehydratase Gene Family'' where lower(pfam_dom_auto)~lower(''{GDP_Man_Dehyd'')
        --     and gene_fam is null ', phyt_table);
       --EXECUTE format('update %I set gene_fam=''Thioesterase Gene Family'' where lower(pfam_dom_auto)~lower(''4HBT'')
        --     and gene_fam is null ', phyt_table);
             -- collide with acyl lipid
        --EXECUTE format('update %I set gene_fam=''3-beta-HSD Gene Family'' where lower(pfam_dom_auto)~lower(''\{3Beta_HSD'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Aldose 1-epimerase Gene Family'' where lower(pfam_dom_auto)~lower(''Aldose_epim'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Plant-specific domain TIGR01589 Gene Family'' where lower(pfam_dom_auto)~lower(''A_thal_3526'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Core-2/I-branching beta-1,6-N-acetylglucosaminyltransferase Gene Family'' where lower(pfam_dom_auto)~lower(''Branch'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''O-acetyltransferase Gene Family'' where lower(pfam_dom_auto)~lower(''Cas1_AcylT'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''CCT motif gene family'' where lower(pfam_dom_auto)~lower(''{CCT}'')
        --     gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Clathrin adaptor complex small chain gene family'' where lower(pfam_dom_auto)~lower(''{Clat_adaptor_s'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''CobW gene family'' where lower(pfam_dom_auto)~lower(''{cobW'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''TCP-1/cpn60 chaperonin gene family'' where lower(pfam_dom_auto)~lower(''{Cpn60_TCP1'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''FAD-dependent oxidoreductase gene family'' where lower(pfam_dom_auto)~lower(''{DAO'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''DHHC-type finger gene family'' where lower(pfam_dom_auto)~lower(''{DHHC'')
        --      and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Sulfite exporter TauE/SafE gene family'' where lower(pfam_dom_auto)~lower(''{TauE'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Zinc-binding dehydrogenase gene family'' where lower(pfam_dom_auto)~lower(''{ADH_zinc_N'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Histidine kinase gene family'' where lower(pfam_dom_auto)~lower(''CHASE|HisKA'')
        --     and lower(pfam_dom_auto)~lower(''Response_reg'') and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Phosphatidate cytidylyltransferase gene family'' where lower(pfam_dom_auto)~lower(''{CTP_transf_1'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Cytidine/deoxycytidylate deaminase gene family'' where lower(pfam_dom_auto)~lower(''{dCMP_cyt_deam'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Helicase gene family'' where lower(pfam_dom_auto)~lower(''{HA2|{DEAD|{{Helicase_C|{Helicase_C_2|{Helicase_C_4'')
        --     and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''SPX domain gene family'' where lower(pfam_dom_auto)~lower(''{SPX|{EXS,SPX|{MFS_1,SPX'')
        --    and lower(pfam_dom_auto)~lower(''Pkinase'') and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''SPX domain gene family'' where lower(pfam_dom_auto)~lower(''SPX'')
        --    and lower(pfam_dom_auto)~lower(''Zf'') and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''GT33 gene family'' where lower(pfam_dom_auto)~lower(''{Glycos_transf_1'')
        --    and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Lon protease gene family'' where lower(pfam_dom_auto)~lower(''LON_substr_bdg|Lon_C'')
        --    and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''CCT motif gene family'' where lower(pfam_dom_auto)~lower(''{CCT'')
        --     and lower(pfam_dom_auto)~lower(''zf-B_box'') and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''MUSTANG (MUG) gene family'' where lower(pfam_dom_auto)~lower(''DBD_Tnp_Mut'')
        --    and lower(pfam_dom_auto)~lower(''MULE'') and lower(pfam_dom_auto)~lower(''SWIM'') and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Acyl Lipid Metabolism Gene Family'' where lower(pfam_dom_auto)~lower(''{ACC_central|{Acyl-ACP_TE|{E1_dh|2-oxoacid_dh|{FAE1_CUT1_RppA|{Lipase_3|{Tryp_alpha_amyl|{LTP_2|{Hydrophob_seed|{FA_desaturase|{Citrate_bind|{Citrate_synt|{Oleosin|{ACBP|{CMAS|{Palm_thioest|{GDPD|{AMP-binding|{DUF1298|{ELO,|{ELO}|{Wax2|{BOAT_2|{NAD_binding_4|{Steroid_dh|{PI-PLC-X|{PIP5K|{PI3_PI4_kinase|{PAP2|{Patatin|{Syja_N|{Exo_endo_phos|{VID27|{An_peroxidase|{Amidase|{Oxidored_FMN|{Allene_ox_cyc|{Lipoxygenase|{Abhydrolase_6|{Abhydrolase_1|{Abhydrolase_2|{Hydrolase_4|{Phospholip_A2_1|{DAGK_acc|Acyl-CoA_dh_1|{ECH_2|{Thiolase_N|{ECH_1|{ACOX|{Abhydro_lipase|{LCAT|{Caleosin|{BOAT|{Acyltransferase|{PP-binding|{CDP-OH_P_transf|{Choline_kinase|{CTP_transf_like|{PSS|{DLH|{ACP_syn_III_C'')
        --     and lower(pfam_dom_auto)!~lower(''p450|PLDc|PhoLip_ATPase_C|Y_phosphatase|4HBT'') and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Acyl Lipid Metabolism Gene Family'' where lower(pfam_dom_auto)~lower(''{ACC_central|{Acyl-ACP_TE|{E1_dh|2-oxoacid_dh|{FAE1_CUT1_RppA|{Lipase_3|{Tryp_alpha_amyl|{LTP_2|{Hydrophob_seed|{FA_desaturase|{Citrate_bind|{Citrate_synt|{Oleosin|{ACBP|{CMAS|{Palm_thioest|{AMP-binding|{DUF1298|{ELO,|{ELO}|{Wax2|{BOAT_2|{NAD_binding_4|{Steroid_dh|{PI-PLC-X|{PAP2|{Patatin|{Syja_N|{Exo_endo_phos|{VID27|{An_peroxidase|{Amidase|{Oxidored_FMN|{Allene_ox_cyc|{Abhydrolase_6|{Abhydrolase_1|{Abhydrolase_2|{Hydrolase_4|{Phospholip_A2_1|{DAGK_acc|Acyl-CoA_dh_1|{ECH_2|{Thiolase_N|{ECH_1|{ACOX|{Abhydro_lipase|{LCAT|{Caleosin|{BOAT|{Acyltransferase|{PP-binding|{CDP-OH_P_transf|{CTP_transf_like|{PSS|{DLH|{ACP_syn_III_C'')
        --     and lower(pfam_dom_auto)!~lower(''p450|PLDc|PhoLip_ATPase_C|Y_phosphatase|4HBT'') and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''ABC Transporter Gene Family'' where lower(pfam_dom_auto)~lower(''{ABC2_membrane_3|{ABC_tran|{ABC_membrane|{APS_kinase,ABC_membrane|{ABC_membrane_2|{ABC2_membrane|{CbiQ|{laD|{AAA_22,ABC2_membrane|{AAA_15,ABC_tran|{AAA_14,ABC_tran|{AAA_14,ABC_tran|UPF0014'')
        --     and gene_fam is null', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Glycosyltransferase Gene Family'' where lower(pfam_dom_auto)~lower(''{Sucrose_synth|{Glyco_transf_4|{GDG_synth|{Glyco_transf_28|{DUF604|{Gb3_synth|{Glyco_transf_41|{annosyl_trans|{Spc97_Spc98|{Glyco_trans_2_3|{Glyco_trans_4_5|{Glyco_tranf_2_3|{Glyco_transf_90|{Glyco_tranf_2_4|{Glyco_transf_92|{Glyco_tranf_2_4|{Glyco_transf_92|{Glycos_trans_3N|{Glyco_trans_1_4|{Gal-bind_lectin'')
        --     and lower(pfam_dom_auto)!~lower(''Trehalose_Ppase|XG_Ftase|Glucan_synthase'') and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Eukaryotic Initiation Factors Gene Family'' where lower(pfam_dom_auto)~lower(''{SUI1|{eIF-1a|{EIF_2_alpha|{eIF-5_eIF|{eIF2|IF-2B|{W2|{eIF2A|{eIF-3|{eIF-3_zeta|{eIF3g|{CSN8_PSD8_EIF3K|{eIF-4B|{IF4E|{IF4G|{eIF-5_eIF-2B|{eIF-5a|{GTP_EFTU|{EF1_GNE|{eRF1_2|{eIF3_N'')
        --     and lower(pfam_dom_auto)!~lower(''SWIB|UORF|{WD40'') and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Inorganic Solute Cotransporter Gene Family'' where lower(pfam_dom_auto)~lower(''{Zip|{FS_1|{Sulfate_transp|{FS_MOT1|{STAS|{Nramp|{K_trans|{Cation_efflux|{Ammonium_transp|{Ctr|{TrkH|{PHO4'')
        --     and lower(pfam_dom_auto)!~lower(''Na_H_Exchanger|Sugar_tr'') and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Glycoside Hydrolase Gene Family'' where lower(pfam_dom_auto)~lower(''{Glyco_hydro_1,|{Glyco_hydro_1}|{Glyco_hydro_2,|{Glyco_hydro_2}|{Glyco_hydro_3,|{Glyco_hydro_3_C,|{Glyco_hydro_3}|{Glyco_hydro_3_C}|{Cellulase|{Glyco_hydro_9|{Glyco_hydro_10,|{Glyco_hydro_10}|{Alpha-amyl|{Glyco_hydro_14|{Glyco_hydro_16|{Glyco_hydro_17|{Glyco_hydro_18|{Glyco_hydro_19|{Glyco_hydro_20|{elibiase_2|{Glyco_hydro_28|{Glyco_hydro_31|{Glyco_hydro_32|{Raffinose_syn|{Glyco_hydro_38|{Glyco_hydro_47|{Alpha-L-AF|{Glyco_hydro_63|{Glyco_hydro_77|{Glyco_hydro_79|{Glyco_hydro_81|{Glyco_hydro_85|{Glyco_hydro_127|{Glyco_hydro_100|{Glyco_hydro_2_C'')
        --     and lower(pfam_dom_auto)!~lower(''Trehalase'') and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Glycoside Hydrolase Gene Family'' where lower(pfam_dom_auto)~lower(''{Glyco_hydro_1,|{Glyco_hydro_1}|{Glyco_hydro_2,|{Glyco_hydro_2}|{Glyco_hydro_3,|{Glyco_hydro_3_C,|{Glyco_hydro_3}|{Glyco_hydro_3_C}|{Cellulase|{Glyco_hydro_9|{Glyco_hydro_10,|{Glyco_hydro_10}|{Alpha-amyl|{Glyco_hydro_14|{Glyco_hydro_16|{Glyco_hydro_17|{Glyco_hydro_18|{Glyco_hydro_19|{Glyco_hydro_20|{elibiase_2|{Glyco_hydro_28|{Glyco_hydro_31|{Glyco_hydro_38|{Glyco_hydro_47|{Alpha-L-AF|{Glyco_hydro_63|{Glyco_hydro_77|{Glyco_hydro_79|{Glyco_hydro_81|{Glyco_hydro_85|{Glyco_hydro_127|{Glyco_hydro_100|{Glyco_hydro_2_C'')
        --     and lower(pfam_dom_auto)!~lower(''Trehalase'') and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''CRP Gene Family'' where lower(pfam_dom_auto)~lower(''Ribosomal'')
         --   and lower(at_des)~lower(''Ribosomal'') and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Protein Kinase Gene Superfamily'' where lower(pfam_dom_auto)~lower(''{Pkinase|{NAF|{alectin_like|{Malectin_like|{LRR|{Kinase-like|{RIO1|{ABC1|{B_lectin|{BT1|{Beach|{APH|{DUF1084|Pkinase'')
        --    and lower(pfam_dom_auto)~lower(''Pkinase'')  and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Protein Kinase Gene Superfamily'' where lower(pfam_dom_auto)~lower(''{AMPK1_CBM|{GUB_WAK_bind'')
        --    and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''UORF Gene Family'' where lower(pfam_dom_auto)~lower(''UORF_c'')
        --    and lower(pfam_dom_auto)!~lower(''bZIP_1|Trehalose_Ppase|Pkinase|bHLH-MYC_N|HLH|PPR_2|Homeobox|HSF_DNA-bind|NB-ARC'') and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Calmodulin and calmodulin-related calcium sensor proteins Gene Family'' where lower(pfam_dom_auto)~lower(''{EF-hand_1|{EF-hand_5|{EF-hand_6|{EF-hand_7|{EF-hand_8|Calmodulin_bind'')
        --    and lower(pfam_dom_auto)!~lower(''Dimer_Tnp_hAT'') and gene_fam is null ', phyt_table);
        --EXECUTE format('update %I set gene_fam=''Methyl transferases gene family'' where lower(pfam_dom_auto)~lower(''{Methyltransf'')
        --    and gene_fam is null ', phyt_table);

        --create domain
        -- EXECUTE format('update %I set gene_fam=''G2-like Transcription Factor Family'' where lower(pfam_dom_auto)~lower(''yb_DNA-binding|yb_CC_LHEQLE'')
        --    and lower(pfam_dom_auto)!~lower(''Response_reg|yb_DNA-bind_6|SNF2_N|DIRP|Bromodomain|Lactamase|DNA_gyraseB|yb_DNA-bind_7'') and gene_fam is null ', phyt_table);
        -- { for first domain priority
        -- EXECUTE format('update %I set gene_fam=''HRT Transcription Factor Family'' where lower(pfam_dom_auto)~lower(''HRT'')
        --    and gene_fam is null ', phyt_table);

        EXECUTE format('drop table if exists temp');
        EXECUTE format('create table temp as select distinct loc from %I', phyt_table);
        EXECUTE format('alter table temp add gene_fam varchar(100)');
        EXECUTE format('update temp as a set gene_fam=b.gene_fam from %I as b where a.loc = b.loc', phyt_table);
        EXECUTE format('drop table if exists %I', table_group);
        EXECUTE format('create table %I as select gene_fam, array_agg(loc) from temp group by gene_fam', table_group);
        EXECUTE format('update %I set array_agg=array_unique(array_agg)', table_group);
        EXECUTE format('alter table %I add loc_len int, add trn_array varchar[], add trn_len int,
            add phyt_id_array varchar[], add phyt_id_len int', table_group);
        EXECUTE format('drop table if exists temp');
        EXECUTE format('create table temp as select distinct trn from %I', phyt_table);
        EXECUTE format('alter table temp add gene_fam varchar(100)');
        EXECUTE format('update temp as a set gene_fam = b.gene_fam from %I as b where a.trn = b.trn', phyt_table);
        EXECUTE format('drop table if exists temp2');
        EXECUTE format('create table temp2 as select gene_fam, array_agg(trn) from temp group by gene_fam');
        EXECUTE format('update %I as a set trn_array = b.array_agg from temp2 as b where a.gene_fam=b.gene_fam',
            table_group);
        EXECUTE format('update %I set trn_array=array_unique(trn_array)', table_group);
        EXECUTE format('drop table if exists temp');
        EXECUTE format('create table temp as select distinct phyt_id from %I', phyt_table);
        EXECUTE format('alter table temp add gene_fam varchar(100)');
        EXECUTE format('update temp as a set gene_fam = b.gene_fam from %I as b where a.phyt_id = b.phyt_id',
            phyt_table);
        EXECUTE format('drop table if exists temp2');
        EXECUTE format('create table temp2 as select gene_fam, array_agg(phyt_id) from temp group by gene_fam');
        EXECUTE format('update %I as a set phyt_id_array = b.array_agg from temp2 as b where a.gene_fam=b.gene_fam',
            table_group);
        EXECUTE format('update %I set phyt_id_array=array_unique(phyt_id_array)', table_group);
        EXECUTE format('drop table if exists temp2; drop table if exists temp;');
        EXECUTE format('delete from %I where gene_fam is null', table_group);
        EXECUTE format('update %I set loc_len=array_length(array_agg, 1)', table_group);
        EXECUTE format('update %I set trn_len=array_length(trn_array, 1)', table_group);
        EXECUTE format('update %I set phyt_id_len=array_length(phyt_id_array, 1)', table_group);

	end
    $body$ language "plpgsql";


-- path set to variable
\set phytdir $$'/scratch/user/ren_net/project/family_enrich/phyt12_data/'$$
--\set pfamdir $$'/scratch/user/ren_net/project/family_enrich/pfam_data/'$$
\set pfamdir $$'/scratch/user/ren_net/project/family_enrich/pfam_data/pfam_map_52019/'$$


select genfam_db_maker('atha_phyt12',
                        :phytdir || 'Athaliana_167_TAIR10.annotation_info.txt',
                        'atha',
                        :pfamdir || 'atha_pfam_tab.csv');
select genfam_db_maker('osat_phyt12',
                        :phytdir || 'Osativa_204_v7.0.annotation_info.txt',
                        'osat',
                        :pfamdir || 'osat_pfam_tab.csv');
select genfam_db_maker('grai_phyt12',
                        :phytdir || 'Graimondii_221_v2.1.annotation_info.txt',
                        'grai',
                        :pfamdir || 'grai_pfam_tab.csv');
select genfam_db_maker('taes_phyt12',
                        :phytdir || 'Taestivum_296_v2.2.annotation_info.txt',
                        'taes',
                        :pfamdir || 'taes_pfam_tab.csv');
select genfam_db_maker('zmay_phyt12',
                        :phytdir || 'Zmays_284_Ensembl-18_2010-01-MaizeSequence.annotation_info.txt',
                        'zmay',
                        :pfamdir || 'zmay_pfam_tab.csv');
--Amaranthus hypochondriacus v2.1 (Amaranth): public 1
select genfam_db_maker('ahyp_phyt12',
                        :phytdir || 'Ahypochondriacus_459_v2.1.annotation_info.txt',
                        'ahyp',
                        :pfamdir || 'ahyp_pfam_tab.csv');
--Amborella trichopoda v1.0 (Amborella): public 2
select genfam_db_maker('atri_phyt12',
                        :phytdir || 'Atrichopoda_291_v1.0.annotation_info.txt',
                        'atri',
                        :pfamdir || 'atri_pfam_tab.csv');
-- Anacardium occidentale v0.9 (Cashew): restrict
-- Ananas comosus v3 (Pineapple): public 3
select genfam_db_maker('acom_phyt12',
                        :phytdir || 'Acomosus_321_v3.annotation_info.txt',
                        'acom',
                        :pfamdir || 'acom_pfam_tab.csv');
--Aquilegia coerulea v3.1 (Colorado blue columbine): restrict
select genfam_db_maker('acoe_phyt12',
                        :phytdir || 'Acomosus_321_v3.annotation_info.txt',
                        'acoe',
                        :pfamdir || 'acoe_pfam_tab.csv');
--Arabidopsis halleri v1.1: restrict
--Arabidopsis lyrata v2.1 (Lyre-leaved rock cress):public 4
select genfam_db_maker('alyr_phyt12',
                        :phytdir || 'Alyrata_384_v2.1.annotation_info.txt',
                        'alyr',
                        :pfamdir || 'alyr_pfam_tab.csv');
--Arabidopsis thaliana TAIR10 (Thale cress): public 5
select genfam_db_maker('atha_phyt12',
                        :phytdir || 'Athaliana_167_TAIR10.annotation_info.txt',
                        'atha',
                        :pfamdir || 'atha_pfam_tab.csv');
--Asparagus officinalis V1.1 (Asparagus) public 6
select genfam_db_maker('aoff_phyt12',
                        :phytdir || 'Aofficinalis_498_V1.1.annotation_info_13c.txt',
                        'aoff',
                        :pfamdir || 'aoff_pfam_tab.csv');
--Boechera stricta v1.2 (Drummond's rock cress):public 7
select genfam_db_maker('bstr_phyt12',
                        :phytdir || 'Bstricta_278_v1.2.annotation_info.txt',
                        'bstr',
                        :pfamdir || 'bstr_pfam_tab.csv');
--Botryococcus braunii v2.1: restrict
--Brachypodium distachyon v3.1 (Purple false brome): public 8
select genfam_db_maker('bdis_phyt12',
                        :phytdir || 'Bdistachyon_314_v3.1.annotation_info_13c.txt',
                        'bdis',
                        :pfamdir || 'bdis_pfam_tab.csv');
--Brachypodium hybridum v1.1: restrict
--Brachypodium stacei v1.1: restrict
--Brachypodium sylvaticum v1.1: restrict
--Brassica oleracea capitata v1.0 (Savoy cabbage):public 9
select genfam_db_maker('bole_phyt12',
                        :phytdir || 'Boleraceacapitata_446_v1.0.annotation_info.txt',
                        'bole',
                        :pfamdir || 'bole_pfam_tab.csv');
--Brassica rapa FPsc v1.3 (Turnip mustard - FasPlant self-compatible): restrict
--Capsella grandiflora v1.1: public 10
select genfam_db_maker('cgra_phyt12',
                        :phytdir || 'Cgrandiflora_266_v1.1.annotation_info.txt',
                        'cgra',
                        :pfamdir || 'cgra_pfam_tab.csv');
--Capsella rubella v1.0 (Pink shepherd's purse):public 11
select genfam_db_maker('crub_phyt12',
                        :phytdir || 'Crubella_183_v1.0.annotation_info.txt',
                        'crub',
                        :pfamdir || 'crub_pfam_tab.csv');
--Carica papaya ASGPBv0.4 (Papaya):public 12
select genfam_db_maker('cpap_phyt12',
                        :phytdir || 'Cpapaya_113_ASGPBv0.4.annotation_info.txt',
                        'cpap',
                        :pfamdir || 'cpap_pfam_tab.csv');
--Chenopodium quinoa v1.0 (Quinoa): public 13
select genfam_db_maker('cqui_phyt12',
                        :phytdir || 'Cquinoa_392_v1.0.annotation_info.txt',
                        'cqui',
                        :pfamdir || 'cqui_pfam_tab.csv');
--Chlamydomonas reinhardtii v5.5 (Green algae): public 14
select genfam_db_maker('crei_phyt12',
                        :phytdir || 'Creinhardtii_281_v5.5.annotation_info.txt',
                        'crei',
                        :pfamdir || 'crei_pfam_tab.csv');
--Chromochloris zofingiensis v5.2.3.2:public 15
select genfam_db_maker('czof_phyt12',
                        :phytdir || 'Czofingiensis_461_v5.2.3.2.annotation_info.txt',
                        'czof',
                        :pfamdir || 'czof_pfam_tab.csv');
--Cicer arietinum v1.0 (Chickpea): public 16
select genfam_db_maker('cari_phyt12',
                        :phytdir || 'Carietinum_492_v1.0.annotation_info.txt',
                        'cari',
                        :pfamdir || 'cari_pfam_tab.csv');
--Citrus clementina v1.0 (Clementine):public 17
select genfam_db_maker('ccle_phyt12',
                        :phytdir || 'Cclementina_182_v1.0.annotation_info.txt',
                        'ccle',
                        :pfamdir || 'ccle_pfam_tab.csv');
--Citrus sinensis v1.1 (Sweet orange):public 18
select genfam_db_maker('csin_phyt12',
                        :phytdir || 'Csinensis_154_v1.1.annotation_info.txt',
                        'csin',
                        :pfamdir || 'csin_pfam_tab.csv');
--Coccomyxa subellipsoidea C-169 v2.0: public 19
select genfam_db_maker('csub_phyt12',
                        :phytdir || 'CsubellipsoideaC169_227_v2.0.annotation_info.txt',
                        'csub',
                        :pfamdir || 'csub_pfam_tab.csv');
--Coffea arabica UCDv0.5 (Coffee bean): restrict
--Cucumis sativus v1.0 (Cucumber):public 20
select genfam_db_maker('csat_phyt12',
                        :phytdir || 'Csativus_122_annotation_info_edited.txt',
                        'csat',
                        :pfamdir || 'csat_pfam_tab.csv');
--Daucus carota v2.0 (Carrot): public 21
select genfam_db_maker('dcar_phyt12',
                        :phytdir || 'Dcarota_388_v2.0.annotation_info.txt',
                        'dcar',
                        :pfamdir || 'dcar_pfam_tab.csv');
--Dunaliella salina v1.0 (Dunaliella):public 22
select genfam_db_maker('dsal_phyt12',
                        :phytdir || 'Dsalina_325_v1.0.annotation_info.txt',
                        'dsal',
                        :pfamdir || 'dsal_pfam_tab.csv');
--Eucalyptus grandis v2.0 (Rose gum):public 23
select genfam_db_maker('egra_phyt12',
                        :phytdir || 'Egrandis_297_v2.0.annotation_info.txt',
                        'egra',
                        :pfamdir || 'egra_pfam_tab.csv');
--Eutrema salsugineum v1.0 (Salt cress):public 24
select genfam_db_maker('esal_phyt12',
                        :phytdir || 'Esalsugineum_173_v1.0.annotation_info.txt',
                        'esal',
                        :pfamdir || 'esal_pfam_tab.csv');
--Fragaria vesca v1.1 (Woodland strawberry): public 25
select genfam_db_maker('fves_phyt12',
                        :phytdir || 'Fvesca_226_v1.1.annotation_info.txt',
                        'fves',
                        :pfamdir || 'fves_pfam_tab.csv');
--Glycine max Wm82.a2.v1 (Soybean):public 26
select genfam_db_maker('gmax_phyt12',
                        :phytdir || 'Gmax_275_Wm82.a2.v1.annotation_info.txt',
                        'gmax',
                        :pfamdir || 'gmax_pfam_tab.csv');
--Gossypium hirsutum v1.1 (Upland cotton): restrict
--Gossypium raimondii v2.1 (Cotton): public 27
select genfam_db_maker('grai_phyt12',
                        :phytdir || 'Graimondii_221_v2.1.annotation_info.txt',
                        'grai',
                        :pfamdir || 'grai_pfam_tab.csv');
--Helianthus annuus r1.2 (Sunflower): public
--Hordeum vulgare r1 (Barley): public 28
select genfam_db_maker('hvul_phyt12',
                        :phytdir || 'Hvulgare_462_r1.annotation_info.txt',
                        'hvul',
                        :pfamdir || 'hvul_pfam_tab.csv');
--Kalanchoe fedtschenkoi v1.1 (diploid Kalanchoe) public 29
select genfam_db_maker('kfed_phyt12',
                        :phytdir || 'Kfedtschenkoi_382_v1.1.annotation_info.txt',
                        'kfed',
                        :pfamdir || 'kfed_pfam_tab.csv');
--Kalanchoe laxiflora v1.1 (Milky widow's thrill): restrict
--Lactuca sativa V8 (Lettuce): public 30
select genfam_db_maker('lsat_phyt12',
                        :phytdir || 'Lsativa_467_v5.annotation_info.txt',
                        'lsat',
                        :pfamdir || 'lsat_pfam_tab.csv');
--Linum usitatissimum v1.0 (Flax): public 31
select genfam_db_maker('lusi_phyt12',
                        :phytdir || 'Lusitatissimum_200_v1.0.annotation_info.txt',
                        'lusi',
                        :pfamdir || 'lusi_pfam_tab.csv');
--Malus domestica v1.0 (Apple): public 32
select genfam_db_maker('mdom_phyt12',
                        :phytdir || 'Mdomestica_196_v1.0.annotation_info.txt',
                        'mdom',
                        :pfamdir || 'mdom_pfam_tab.csv');
--Manihot esculenta v6.1 (Cassava): public 33
select genfam_db_maker('mesc_phyt12',
                        :phytdir || 'Mesculenta_305_v6.1.annotation_info.txt',
                        'mesc',
                        :pfamdir || 'mesc_pfam_tab.csv');
--Marchantia polymorpha v3.1 (Common liverwort): public 34
select genfam_db_maker('mpol_phyt12',
                        :phytdir || 'Mpolymorpha_320_v3.1.annotation_info.txt',
                        'mpol',
                        :pfamdir || 'mpol_pfam_tab.csv');
--Medicago truncatula Mt4.0v1 (Barrel medic): public 35
select genfam_db_maker('mtru_phyt12',
                        :phytdir || 'Mtruncatula_285_Mt4.0v1.annotation_info.txt',
                        'mtru',
                        :pfamdir || 'mtru_pfam_tab.csv');
--Micromonas pusilla CCMP1545 v3.0: public 36
select genfam_db_maker('mpus_phyt12',
                        :phytdir || 'MpusillaCCMP1545_228_v3.0.annotation_info.txt',
                        'mpus',
                        :pfamdir || 'mpus_pfam_tab.csv');
--Micromonas sp. RCC299 v3.0: public 37
select genfam_db_maker('mspr_phyt12',
                        :phytdir || 'MpusillaRCC299_229_v3.0.annotation_info.txt',
                        'mspr',
                        :pfamdir || 'mspr_pfam_tab.csv');
--Mimulus guttatus v2.0 (Monkey flower): public 38
select genfam_db_maker('mgut_phyt12',
                        :phytdir || 'Mguttatus_256_v2.0.annotation_info.txt',
                        'mgut',
                        :pfamdir || 'mgut_pfam_tab.csv');
--Miscanthus sinensis v7.1 (Chinese silvergrass): restrict
--Musa acuminata v1 (Banana): public 39
select genfam_db_maker('macu_phyt12',
                        :phytdir || 'Macuminata_304_v1.annotation_info.txt',
                        'macu',
                        :pfamdir || 'macu_pfam_tab.csv');
--Olea europaea var. sylvestris v1.0 (Wild olive): public 40
select genfam_db_maker('oeur_phyt12',
                        :phytdir || 'Oeuropaea_451_v1.0.annotation_info.txt',
                        'oeur',
                        :pfamdir || 'oeur_pfam_tab.csv');
--Oropetium thomaeum v1.0: public 41
select genfam_db_maker('otho_phyt12',
                        :phytdir || 'Othomaeum_386_v1.0.annotation_info.txt',
                        'otho',
                        :pfamdir || 'otho_pfam_tab.csv');
--Oryza sativa Kitaake v3.1 (Kitaake rice): restrict
--Oryza sativa v7_JGI (Rice): public 42
select genfam_db_maker('osat_phyt12',
                        :phytdir || 'Osativa_204_v7.0.annotation_info.txt',
                        'osat',
                        :pfamdir || 'osat_pfam_tab.csv');
--Ostreococcus lucimarinus v2.0: public 43
select genfam_db_maker('oluc_phyt12',
                        :phytdir || 'Olucimarinus_231_v2.0.annotation_info.txt',
                        'oluc',
                        :pfamdir || 'oluc_pfam_tab.csv');
--Panicum hallii v3.1 (Hall's panicgrass): restrict
--Panicum virgatum v4.1 (Switchgrass): restrict
--Phaseolus vulgaris v2.1 (Common bean): restrict
--Physcomitrella patens v3.3 (Moss): public 44
select genfam_db_maker('ppat_phyt12',
                        :phytdir || 'Ppatens_318_v3.3.annotation_info.txt',
                        'ppat',
                        :pfamdir || 'ppat_pfam_tab.csv');
--Populus deltoides WV94 v2.1 (Eastern cottonwood): restrict
--Populus trichocarpa v3.1 (Poplar): public 45
select genfam_db_maker('ptri_phyt12',
                        :phytdir || 'Ptrichocarpa_444_v3.1.annotation_info.txt',
                        'ptri',
                        :pfamdir || 'ptri_pfam_tab.csv');
--Porphyra umbilicalis v1.5 (Laver): public 46
select genfam_db_maker('pumb_phyt12',
                        :phytdir || 'Pumbilicalis_456_v1.5.annotation_info.txt',
                        'pumb',
                        :pfamdir || 'pumb_pfam_tab.csv');
--Prunus persica v2.1 (Peach): public 47
select genfam_db_maker('pper_phyt12',
                        :phytdir || 'Ppersica_298_v2.1.annotation_info.txt',
                        'pper',
                        :pfamdir || 'pper_pfam_tab.csv');
--Ricinus communis v0.1 (Castor bean):public 48
select genfam_db_maker('rcom_phyt12',
                        :phytdir || 'Rcommunis_119_v0.1.annotation_info.txt',
                        'rcom',
                        :pfamdir || 'rcom_pfam_tab.csv');
--Salix purpurea v1.0 (Purple osier willow): restrict
--Selaginella moellendorffii v1.0 (Spikemoss): public 49
select genfam_db_maker('smoe_phyt12',
                        :phytdir || 'Smoellendorffii_91_v1.0.annotation_info.txt',
                        'smoe',
                        :pfamdir || 'smoe_pfam_tab.csv');
--Setaria italica v2.2 (Foxtail millet): public 50
select genfam_db_maker('sita_phyt12',
                        :phytdir || 'Sitalica_312_v2.2.annotation_info.txt',
                        'sita',
                        :pfamdir || 'sita_pfam_tab.csv');
--Setaria viridis v2.1 (Green foxtail): restrict
--Solanum lycopersicum iTAG2.4 (Tomato): public 51
select genfam_db_maker('slyc_phyt12',
                        :phytdir || 'Slycopersicum_annotation_info.txt',
                        'slyc',
                        :pfamdir || 'slyc_pfam_tab.csv');
--Solanum tuberosum v4.03 (Potato):public 52
select genfam_db_maker('stub_phyt12',
                        :phytdir || 'Stuberosum_448_v4.03.annotation_info.txt',
                        'stub',
                        :pfamdir || 'stub_pfam_tab.csv');
--Sorghum bicolor v3.1.1 (Cereal grass): public 53
select genfam_db_maker('sbio_phyt12',
                        :phytdir || 'Sbicolor_454_v3.1.1.annotation_info_13c.txt',
                        'sbio',
                        :pfamdir || 'sbio_pfam_tab.csv');
--Sphagnum fallax v0.5 (Bog moss): restrict
--Spirodela polyrhiza v2 (Greater duckweed): public 54
select genfam_db_maker('spol_phyt12',
                        :phytdir || 'Spolyrhiza_290_v2.annotation_info.txt',
                        'spol',
                        :pfamdir || 'spol_pfam_tab.csv');
--Theobroma cacao v1.1 (Cacao): public 55
select genfam_db_maker('tcac_phyt12',
                        :phytdir || 'Tcacao_233_v1.1.annotation_info.txt',
                        'tcac',
                        :pfamdir || 'tcac_pfam_tab.csv');
--Trifolium pratense v2 (Red clover): public 56
select genfam_db_maker('tpra_phyt12',
                        :phytdir || 'Tpratense_385_v2.annotation_info.txt',
                        'tpra',
                        :pfamdir || 'tpra_pfam_tab.csv');
--Triticum aestivum v2.2 (Common wheat): public 57
select genfam_db_maker('taes_phyt12',
                        :phytdir || 'Taestivum_296_v2.2.annotation_info.txt',
                        'taes',
                        :pfamdir || 'taes_pfam_tab.csv');
--Vigna unguiculata v1.1 (Cowpea): restrict
--Vitis vinifera Genoscope.12X (Common grape vine): public 58
select genfam_db_maker('vvin_phyt12',
                        :phytdir || 'Vvinifera_145_Genoscope.12X.annotation_info.txt',
                        'vvin',
                        :pfamdir || 'vvin_pfam_tab.csv');
--Volvox carteri v2.1 (Volvox): public 59
select genfam_db_maker('vcar_phyt12',
                        :phytdir || 'Vcarteri_317_v2.1.annotation_info.txt',
                        'vcar',
                        :pfamdir || 'vcar_pfam_tab.csv');
--Zea mays Ensembl-18 (Maize): public 60
select genfam_db_maker('zmay_phyt12',
                        :phytdir || 'Zmays_284_Ensembl-18_2010-01-MaizeSequence.annotation_info.txt',
                        'zmay',
                        :pfamdir || 'zmay_pfam_tab.csv');
--Zostera marina v2.2 (Common eelgrass): public 61
select genfam_db_maker('zmar_phyt12',
                        :phytdir || 'Zmarina_324_v2.2.annotation_info_13c.txt',
                        'zmar',
                        :pfamdir || 'zmar_pfam_tab.csv');




-- ADD GOSLIM ANNOTATION
/*gene fam project */
drop table if exists ath_goslim, rice_goslim;
create table ath_goslim (gene_id varchar(60), tr_id varchar(40), pos_desc varchar(60), activity varchar(200), go varchar(60),
	    id int, term varchar(5), activity_2 varchar(200), ev_code varchar(10) );
create table rice_goslim (gene_id varchar(30),  go varchar(30), term varchar(5), activity varchar(200), iea varchar(5), ath_id varchar(30));

\copy ath_goslim from '/scratch/user/ren_net/project/seqdb/arabidopsis/tair10/ATH_GO_GOSLIM_csv.csv' with (FORMAT csv, header false) ;
\copy rice_goslim from '/scratch/user/ren_net/project/seqdb/rice/all.GOSlim_assignment' ;

alter table rice_goslim add gene_id_2 varchar(20);
update rice_goslim set gene_id_2=(regexp_split_to_array(gene_id, '\.'))[1];

-- to delete duplicate records based on two columns
alter table ath_goslim add gene_id_go_id varchar(40);
alter table rice_goslim add gene_id_go_id varchar(40);
update ath_goslim set gene_id_go_id=(gene_id || go);
update rice_goslim set gene_id_go_id=(gene_id_2 || go);
drop table if exists ath_goslim_uniq, rice_goslim_uniq;
create table ath_goslim_uniq as select gene_id_go_id, count(gene_id_go_id) from ath_goslim group by gene_id_go_id;
create table rice_goslim_uniq as select gene_id_go_id, count(gene_id_go_id) from rice_goslim group by gene_id_go_id;
alter table ath_goslim_uniq add gene_id varchar(30), add tr_id varchar(30), add pos_desc varchar(60),
    add activity varchar(200), add go varchar(30), add id int, add term varchar(5), add activity_2 varchar(200), add ev_code varchar(10);
alter table rice_goslim_uniq add gene_id varchar(30), add go varchar(30), add activity varchar(200), add term varchar(5);
update ath_goslim_uniq as a set gene_id=b.gene_id, tr_id=b.tr_id, pos_desc=b.pos_desc, activity=b.activity,
    go=b.go, id=b.id, term=b.term, activity_2=b.activity_2, ev_code=b.ev_code from ath_goslim as b where
    a.gene_id_go_id=b.gene_id_go_id;
update rice_goslim_uniq as a set gene_id=b.gene_id_2, go=b.go, activity=b.activity, term=b.term from rice_goslim as b where
    a.gene_id_go_id=b.gene_id_go_id;


-- add gene family annotation
alter table ath_goslim_uniq drop if exists gene_fam_2, drop if exists fam_go_term, drop if exists uniq_p, drop if exists
    uniq_f, drop if exists uniq_c, drop if exists go_activity;
alter table rice_goslim_uniq drop if exists gene_fam_2, drop if exists fam_go_term, drop if exists uniq_p, drop if exists
    uniq_f, drop if exists uniq_c, drop if exists go_activity;
alter table ath_goslim_uniq add gene_fam varchar(100), add fam_go_term varchar(190), add uniq_p text, add uniq_f text,
    add uniq_c text, add go_activity varchar(300);
alter table rice_goslim_uniq add gene_fam varchar(100), add fam_go_term varchar(190), add uniq_p text, add uniq_f text,
    add uniq_c text, add go_activity varchar(300);
update ath_goslim_uniq as a set gene_fam=b.gene_fam from atha_phyt12 as b where a.gene_id=b.loc;
update rice_goslim_uniq as a set gene_fam=b.gene_fam from osat_phyt12 as b where a.gene_id=b.loc;
update ath_goslim_uniq set fam_go_term=(gene_fam || '_' || go || '_' || term);
update rice_goslim_uniq set fam_go_term=(gene_fam || '_' || go || '_' || term);
update ath_goslim_uniq set go_activity=(activity || '(' || go || ')');
update rice_goslim_uniq set go_activity=(activity || '(' || go || ')');
drop table if exists temp, temp1 ;
create table temp as select gene_fam, string_agg(distinct go_activity,',') from ath_goslim_uniq where term='P' group by
    gene_fam;
create table temp1 as select gene_fam, string_agg(distinct go_activity,',') from rice_goslim_uniq where term='P' group by
    gene_fam;
update ath_goslim_uniq as a set uniq_p=b.string_agg from temp as b where a.gene_fam=b.gene_fam;
update rice_goslim_uniq as a set uniq_p=b.string_agg from temp1 as b where a.gene_fam=b.gene_fam;
drop table if exists temp, temp1 ;
create table temp as select gene_fam, string_agg(distinct go_activity,',') from ath_goslim_uniq where term='F' group by
    gene_fam;
create table temp1 as select gene_fam, string_agg(distinct go_activity,',') from rice_goslim_uniq where term='F' group by
    gene_fam;
update ath_goslim_uniq as a set uniq_f=b.string_agg from temp as b where a.gene_fam=b.gene_fam;
update rice_goslim_uniq as a set uniq_f=b.string_agg from temp1 as b where a.gene_fam=b.gene_fam;
drop table if exists temp, temp1 ;
create table temp as select gene_fam, string_agg(distinct go_activity,',') from ath_goslim_uniq where term='C' group by
    gene_fam;
create table temp1 as select gene_fam, string_agg(distinct go_activity,',') from rice_goslim_uniq where term='C' group by
    gene_fam;
update ath_goslim_uniq as a set uniq_c=b.string_agg from temp as b where a.gene_fam=b.gene_fam;
update rice_goslim_uniq as a set uniq_c=b.string_agg from temp1 as b where a.gene_fam=b.gene_fam;
drop table temp, temp1;


-- alter all tables at one time and add the column
do
$$
declare
    i record;
begin
    for i in select * FROM pg_catalog.pg_tables where schemaname like 'public' and tablename like '%gene_fam_group%'
    loop
        execute format('alter table %I drop if exists uniq_p, drop if exists uniq_f, drop if exists uniq_c',
            i.tablename);
        execute format('alter table %I add column uniq_p text, add column uniq_f text, add column uniq_c text',
            i.tablename);
        execute format('update %I as a set uniq_p=b.uniq_p, uniq_f=b.uniq_f, uniq_c=b.uniq_c from ath_goslim_uniq as b
            where a.gene_fam=b.gene_fam', i.tablename);
        execute format('update %I as a set uniq_p=b.uniq_p from rice_goslim_uniq as b where a.uniq_p is null and
            a.gene_fam=b.gene_fam', i.tablename);
        execute format('update %I as a set uniq_f=b.uniq_f from rice_goslim_uniq as b where a.uniq_f is null and
            a.gene_fam=b.gene_fam', i.tablename);
        execute format('update %I as a set uniq_c=b.uniq_c from rice_goslim_uniq as b where a.uniq_c is null and
            a.gene_fam=b.gene_fam', i.tablename);
    end loop;
end;
$$

