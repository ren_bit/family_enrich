import argparse
import re

parser = argparse.ArgumentParser(description="Split the fasta based on the space and for hmmscan output")
parser.add_argument('-f', action='store', dest='file', help='Input hmmscan output file')
parser.add_argument('-o', action='store', dest='out', help='Output CSV file')
result = parser.parse_args()

f_open = open(result.file, "rU")
f_write = open(result.out, "w")

for line in f_open:
    line = line.strip()
    if not line.startswith("#"):
        # data = re.search(r'(.*) (.*)\s+(.*)\s+(.*)\s+(.*)\s+(.*)\s+(.*)\s+(.*)\s+(.*)\s+(.*)\s+(.*)\s+(.*)'
        #                 r'\s+(.*)\s+(.*)\s+(.*)\s+(.*)\s+(.*)\s+(.*)\s+(.*)', line)
        # data = re.search(r'(.*)  (.*)  (.*)  (.*)  (.*)  (.*)  (.*)  (.*)  (.*)  (.*)  (.*)  (.*)  (.*)  (.*)', line)
        # print(data.group(1))
        # if data:
        #    print(data.group(1))
        data = re.split(' +', line)
        # line = line.remove(' ')
        if len(data) == 19:
            # print str.join(',', data)
            data[18] = data[18].replace(',', ' ')
            f_write.write(str.join(',', data))
            f_write.write("\n")
        elif len(data) > 19:
            ele = list(range(18, len(data)))
            data[18] = " ".join([e for i, e in enumerate(data) if i in ele])
            data[18] = data[18].replace(',', '')
            # print str.join(',', data[0:19])
            f_write.write(str.join(',', data[0:19]))
            f_write.write("\n")

f_open.close()
f_write.close()
