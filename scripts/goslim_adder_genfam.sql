/* add go slim annotation to gene fam db */

/* DO THIS STEP; THIS IS NECESSARY TO ADD REMEAINING GENEFAM2 TERMS  */
drop table if exists temp;
create table temp as select gene_fam_2, unnest(array_agg) from atha_phyt12_gene_fam_group ;
update atha_gene_fam_1 as a set gene_fam_2=b.gene_fam_2 from temp as b where a.loc=b.unnest and a.gene_fam_2 is null;

/*gene fam project */
drop table if exists ath_goslim;
create table ath_goslim 
	(
	 gene_id varchar(30),
	 tr_id varchar(30),
	 pos_desc varchar(60),
	 activity varchar(200),
	 go varchar(30),
	 id int,
	 term varchar(5),
	 activity_2 varchar(200),
	 ev_code varchar(10)
	 );
	 
\copy ath_goslim from '/Users/renesh/Renesh_Docs/Research/tamu/db/arabidopsis/ATH_GO_GOSLIM_csv.csv' with (FORMAT csv, header false) ;

/* to delete duplicate records based on two columns */
alter table ath_goslim add gene_id_go_id varchar(40);
update ath_goslim set gene_id_go_id=(gene_id || id);
drop table if exists ath_goslim_uniq;
create table ath_goslim_uniq as select gene_id_go_id, count(gene_id_go_id) from ath_goslim group by gene_id_go_id;
alter table ath_goslim_uniq add gene_id varchar(30), add tr_id varchar(30), add pos_desc varchar(60),
    add activity varchar(200), add go varchar(30), add id int, add term varchar(5), add activity_2 varchar(200), add ev_code varchar(10);
update ath_goslim_uniq as a set gene_id=b.gene_id, tr_id=b.tr_id, pos_desc=b.pos_desc, activity=b.activity,
    go=b.go, id=b.id, term=b.term, activity_2=b.activity_2, ev_code=b.ev_code from ath_goslim as b where
    a.gene_id_go_id=b.gene_id_go_id;

/* add gene family annotation */
alter table ath_goslim_uniq drop if exists gene_fam_2, drop if exists fam_go_term, drop if exists uniq_p, drop if exists
    uniq_f, drop if exists uniq_c, drop if exists go_activity;
alter table ath_goslim_uniq add gene_fam_2 varchar(100), add fam_go_term varchar(90), add uniq_p text, add uniq_f text,
    add uniq_c text, add go_activity varchar(300);
update ath_goslim_uniq as a set gene_fam_2=b.gene_fam_2 from atha_gene_fam_1 as b where a.gene_id=b.loc;
update ath_goslim_uniq set fam_go_term=(gene_fam_2 || '_' || go || '_' || term);
update ath_goslim_uniq set go_activity=(activity || '(' || go || ')');
drop table if exists temp ;
create table temp as select gene_fam_2, string_agg(distinct go_activity,',') from ath_goslim_uniq where term='P' group by
    gene_fam_2;
update ath_goslim_uniq as a set uniq_p=b.string_agg from temp as b where a.gene_fam_2=b.gene_fam_2;
drop table if exists temp ;
create table temp as select gene_fam_2, string_agg(distinct go_activity,',') from ath_goslim_uniq where term='F' group by
    gene_fam_2;
update ath_goslim_uniq as a set uniq_f=b.string_agg from temp as b where a.gene_fam_2=b.gene_fam_2;
drop table if exists temp ;
create table temp as select gene_fam_2, string_agg(distinct go_activity,',') from ath_goslim_uniq where term='C' group by
    gene_fam_2;
update ath_goslim_uniq as a set uniq_c=b.string_agg from temp as b where a.gene_fam_2=b.gene_fam_2;
drop table temp;

/* change db */
--\c family_db;

/* alter all tables at one time and add the column */
do
$$
declare
    i record;
begin
    for i in select * FROM pg_catalog.pg_tables where schemaname like 'public' and tablename like '%gene_fam_group%'
    loop
        execute format('alter table %I drop if exists uniq_p, drop if exists uniq_f, drop if exists uniq_c',
        i.tablename);
        execute format('alter table %I add column uniq_p text, add column uniq_f text, add column uniq_c text',
        i.tablename);
        execute format('update %I as a set uniq_p=b.uniq_p, uniq_f=b.uniq_f, uniq_c=b.uniq_c from ath_goslim_uniq as b where a.gene_fam_2=b.gene_fam_2',
        i.tablename);
    end loop;
end;
$$


/*end*/
